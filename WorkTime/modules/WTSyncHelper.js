/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer sync based methods
***********************************************************************/

function initSyncConfigObj() {  	
  	try {
      	printMessage("INSIDE initSyncConfigObj - ");
      	var config 												= {};
        config.batchsize 										= gblSyncParams.SYNC_BATCH_SIZE;
        config.chunksize 										= gblSyncParams.SYNC_CHUNK_SIZE;       
        config.issecure 										= gblSyncParams.IS_SECURE;
        config.networktimeout 									= gblSyncParams.SYNC_TIMEOUT;

        config.onsyncstart 										= onSyncStartCallback;
        config.onscopestart 									= onScopeStartCallback;
        config.onscopeerror 									= onScopeErrorCallback;
        config.onscopesuccess 									= onScopeSuccessCallback;
        config.onuploadstart 									= onUploadStartCallback;
        config.onuploadsuccess 									= onUploadSuccessCallback;
      	config.ondownloadstart 									= ondownloadstartCallback;
        config.ondownloadsuccess 								= ondownloadsuccessCallback;
        config.onsyncsuccess 									= onSyncSuccessCallback;
        config.onsyncerror 										= onSyncErrorCallBack;

        var removeafterupload									= {};
        removeafterupload.WTMBaaSSyncUploadData					= ["WorkTimeTrace"];
        ////config.removeafterupload								= removeafterupload;
      	////config.insertNullForFieldsMissingInPayload 			= false; /** Kony suggested solution based on support ticket 95214 [Refer JIRA - STP-2881] **/

        return config;
    } catch (e) {
      	logException(e, "initSyncConfigObj", true);
    }
}

/** To construct default session task object **/
function initSessionTaskObj() {
  	try {
      	printMessage("INSIDE initSessionTaskObj - ");
        var defSyncSessionTaskObject 							= { 
            WTSyncDownloadICabsInstances						: {doupload: false,  dodownload: false, uploaderrorpolicy: "continueonerror"},
          	WTSyncDownloadEmployee								: {doupload: false,  dodownload: false, uploaderrorpolicy: "continueonerror"},
          	WTSyncDownloadBranches								: {doupload: false,  dodownload: false, uploaderrorpolicy: "continueonerror"},
          	WTSyncDownloadConfigData							: {doupload: false,  dodownload: false, uploaderrorpolicy: "continueonerror"},
            WTMBaaSSyncUploadData								: {doupload: false,  dodownload: false, uploaderrorpolicy: "continueonerror"},
          	WTNonSyncScope										: {doupload: false,  dodownload: false, uploaderrorpolicy: "continueonerror"}
        };
  
  		return defSyncSessionTaskObject;
   	} catch (e) {
      	logException(e, "initSessionTaskObj", true);
    }
}

/** SYNC PATTERN DESCRIBED BELOW **/
/** 
SYNC_INITIAL_DOWNLOAD
	DOWNLOAD_BASE_DATA						[WTSyncDownloadICabsInstances]
	DOWNLOAD_EMPLOYEE						[WTSyncDownloadEmployee]
	DOWNLOAD_BRANCHES						[WTSyncDownloadBranches]
    DOWNLOAD_CONFIG_DATA					[WTSyncDownloadConfigData]
    
SYNC_DAILY_DOWNLOAD
	DOWNLOAD_CONFIG_DATA					[WTSyncDownloadConfigData]
	
SYNC_DATA_UPLOAD
	UPLOAD_TRACE_DATA						[WTMBaaSSyncUploadData]
**/

/** Start sync session **/
function initDataSync(syncModuleName, syncNavigation, showNoNtwrkMsg, exitOnNoNtwrk){
  	try {
      	printMessage("INSIDE initDataSync - syncModuleName - ", syncModuleName);
      	printMessage("INSIDE initDataSync - syncNavigation - ", syncNavigation);
      	printMessage("INSIDE initDataSync - showNoNtwrkMsg - ", showNoNtwrkMsg);
      	printMessage("INSIDE initDataSync - exitOnNoNtwrk - ", exitOnNoNtwrk);
      
      	if (isNetworkAvailable()){
          	if (syncModuleName === "SYNC_DATA_UPLOAD") {
                Home.lblProgress.text							= getI18nString("common.message.syncStarting");
            }

            printMessage("INSIDE initDataSync - gblSyncProgress - ", gblSyncProgress);

            if (!isEmpty(syncModuleName) && gblSyncProgress.isActive === false) {
                gblSyncProgress.isActive						= true;
                var sesTaskObj 									= initSessionTaskObj();

                if (syncModuleName == "SYNC_DATA_UPLOAD") {
                    sesTaskObj.WTMBaaSSyncUploadData			= {doupload: true, dodownload: false, uploaderrorpolicy: "continueonerror"};
                } else {
                    if (syncNavigation == "DOWNLOAD_BASE_DATA") {
                        sesTaskObj.WTSyncDownloadICabsInstances	= {doupload: false, dodownload: true, uploaderrorpolicy: "continueonerror"};
                    } else if (syncNavigation == "DOWNLOAD_EMPLOYEE") {
                        sesTaskObj.WTSyncDownloadEmployee		= {doupload: false, dodownload: true, uploaderrorpolicy: "continueonerror"};
                    } else if (syncNavigation == "DOWNLOAD_BRANCHES") {
                        sesTaskObj.WTSyncDownloadBranches		= {doupload: false, dodownload: true, uploaderrorpolicy: "continueonerror"};
                    } else {
                        sesTaskObj.WTSyncDownloadConfigData		= {doupload: false, dodownload: true, uploaderrorpolicy: "continueonerror"};
                    }
                }

                printMessage("INSIDE initDataSync - sesTaskObj - ", sesTaskObj);
                var syncConfig									= initSyncConfigObj();
                printMessage("INSIDE initDataSync - syncConfig - ", syncConfig);
                syncConfig.sessiontasks 						= sesTaskObj; 

                gblSyncProgress.syncNavigation					= syncNavigation;
                gblSyncProgress.syncModuleName					= syncModuleName;
                sync.startSession(syncConfig);
            } else {
                printMessage("INSIDE initDataSync - FAILED - gblSyncProgress - ", gblSyncProgress);
            }
        } else {
          	if (syncModuleName === "SYNC_DATA_UPLOAD") {
              	setFlxOverlayMsgOkButtonStyle(true);
            }
          
          	if (showNoNtwrkMsg === true && exitOnNoNtwrk === true) {
              	showMessagePopup("common.message.noNetwork", true, onClickMessagePopupOkButton);
                dismissLoading();
            } else if (showNoNtwrkMsg === true) {
              	showMessagePopup("common.message.noNetwork", true, dismissMessagePopup);
              	dismissLoading();
            } else {
              	/** Nothing to do here **/
            }
        }
    } catch (e) {
      	logException(e, "initDataSync", true);
    }
}

/** Stop sync session **/
function stopSyncSession(){
  	try {
      	printMessage("INSIDE stopSyncSession - ");
      	sync.stopSession(onSyncStopSessionCallback);
    } catch (e) {
      	logException(e, "stopSyncSession", true);
    }  	
}

/** Sync start callback **/
function onSyncStartCallback(outputparams){
  	printMessage("INSIDE onSyncStartCallback - outputparams - ", outputparams);
}

/** Sync scope start callback **/
function onScopeStartCallback(outputparams){
  	try {
      	printMessage("INSIDE onScopeStartCallback - outputparams - ", outputparams);
        if (gblSyncProgress.syncModuleName === "SYNC_DATA_UPLOAD") {
            Home.lblProgress.text								= getI18nString("common.message.syncInProgress");
        }
   	} catch (e) {
      	logException(e, "onScopeStartCallback", true);
    }
}

/** Sync scope error callback **/
function onScopeErrorCallback(outputparams){
	printMessage("INSIDE onScopeErrorCallback - outputparams - ", outputparams);
}

/** Sync scope success callback **/
function onScopeSuccessCallback(outputparams){
  	try {
      	printMessage("INSIDE onScopeSuccessCallback - outputparams - ", outputparams);
      	if (gblSyncProgress.syncModuleName === "SYNC_DATA_UPLOAD") {
            Home.lblProgress.text								= getI18nString("common.message.syncDone");
        }
    } catch (e) {
      	logException(e, "onScopeSuccessCallback", true);
    }        
}

/** Sync upload start callback **/
function onUploadStartCallback(outputparams){
	printMessage("INSIDE onUploadStartCallback - outputparams - ", outputparams);
}

/** Sync upload success callback **/
function onUploadSuccessCallback(response){
  	printMessage("INSIDE onUploadSuccessCallback - ");
}

function ondownloadstartCallback(outputparams) {    
  	try {
      	printMessage("INSIDE ondownloadstartCallback - outputparams - ", outputparams);
        var syncModuleName 										= gblSyncProgress.syncModuleName;
      	var syncNavigation 										= gblSyncProgress.syncNavigation;
      	printMessage("INSIDE ondownloadstartCallback - syncModuleName - ", syncModuleName);
      	printMessage("INSIDE ondownloadstartCallback - syncNavigation - ", syncNavigation);
      
      	var selENVObj											= gblENVConfig[gblBuildMode];
      	var clientcontext										= {};
      	clientcontext.countryCode 								= gblSelData.COUNTRY_CODE;
      	clientcontext.businessCode 								= gblSelData.BUSINESS_CODE;
      	clientcontext.languageCode 								= gblSelData.LANGUAGE_CODE;
      	clientcontext.email 									= gblSelData.EMAIL;
      
      	if (gblAPILayer == "ESB") {
          	clientcontext.client_id 							= selENVObj.ECI;
          	clientcontext.client_secret 						= selENVObj.ECS;
        } else if (gblAPILayer == "GCP") {
          	clientcontext.Authorization 						= gblGCPAuthorization;
          	clientcontext["x-api-key"] 							= selENVObj.GAK;
        } else {
          	clientcontext.client_id 							= selENVObj.ECI;
          	clientcontext.client_secret 						= selENVObj.ECS;
            clientcontext.Authorization 						= gblGCPAuthorization;
          	clientcontext["x-api-key"] 							= selENVObj.GAK;
        }
      
      	if (syncNavigation == "DOWNLOAD_CONFIG_DATA") {
          	clientcontext										= setConfigDataInputs(clientcontext); 
        }
      
      	var req 												= outputparams.downloadRequest;
        printMessage("INSIDE ondownloadstartCallback - req - ", req);
      	req.clientcontext										= clientcontext;
        return req;
    } catch (e) {
      	logException(e, "ondownloadstartCallback", true);
    }  
}

function ondownloadsuccessCallback(outputparams) {
    printMessage("INSIDE ondownloadsuccessCallback - outputparams - ", outputparams);
}

/** Sync success callback **/
function onSyncSuccessCallback(outputparams){
  	try {
      	printMessage("INSIDE onSyncSuccessCallback - outputparams - ", outputparams);
        gblSyncProgress.isActive								= false;

      	showLoading("common.message.loading");
      	var syncModuleName 										= gblSyncProgress.syncModuleName;
      	var syncNavigation 										= gblSyncProgress.syncNavigation;
      	if (syncModuleName == "SYNC_INITIAL_DOWNLOAD" && syncNavigation == "DOWNLOAD_BASE_DATA") {
          	getICabsInstancesFromDB();
        } else if (syncModuleName == "SYNC_INITIAL_DOWNLOAD" && syncNavigation == "DOWNLOAD_EMPLOYEE") {
          	getEmployeeDetailsFromDB();
        } else if (syncModuleName == "SYNC_INITIAL_DOWNLOAD" && syncNavigation == "DOWNLOAD_BRANCHES") {
          	getBranchesFromDB();
        } else if (syncNavigation == "DOWNLOAD_CONFIG_DATA") {
          	getNonServiceEventTypesFromDB();
          	var curDeviceDateTime								= new Date();
			var strCurDeviceDateTime 							= curDeviceDateTime.toString();
          	gblSyncDates.strLastDailySync						= strCurDeviceDateTime;
          	setDataInKonyStore("strLastDailySync", strCurDeviceDateTime);
          	if(syncModuleName == "SYNC_DAILY_DOWNLOAD"){
               dismissLoading();
            }
        } else {
      		gblSelData.LAST_SYNC_DATE							= getLocalDateTimeStr();
          	updateLoggedUserInDB(gblSelData, onSuccessUploadWorkTime, onErrorUploadWorkTime);
          	setFlxOverlayMsgOkButtonStyle(true);
          	if (Home.flxPopupOverlayContainer.isVisible === false) {
              	showMessagePopup("common.message.syncDone", true, dismissMessagePopup);
            }   
            dismissLoading();
        }
  	} catch (e) {
      	logException(e, "onSyncSuccessCallback", true);
    }  
}

/** Sync error callback **/
function onSyncErrorCallBack(error){
  	try {
      	printMessage("INSIDE onSyncErrorCallBack - error - ", error);
  		gblSyncProgress.isActive								= false;
        if (gblSyncProgress.syncModuleName === "SYNC_DATA_UPLOAD") {
          	if (Home.flxPopupOverlayContainer.isVisible === false) {
              	showMessagePopup("common.message.syncError", true, dismissMessagePopup);
            } else {
              	Home.lblProgress.text							= getI18nString("common.message.syncError");
            }  
            setFlxOverlayMsgOkButtonStyle(true);
            dismissLoading();
        } else if (gblSyncProgress.syncModuleName == "SYNC_INITIAL_DOWNLOAD" && gblSyncProgress.syncNavigation == "DOWNLOAD_CONFIG_DATA") {
          	getNonServiceEventTypesFromDB();
          	var curDeviceDateTime								= new Date();
			var strCurDeviceDateTime 							= curDeviceDateTime.toString();
          	gblSyncDates.strLastDailySync						= strCurDeviceDateTime;
          	setDataInKonyStore("strLastDailySync", strCurDeviceDateTime);
        } else {
          	showMessagePopup("common.message.syncError", true, dismissMessagePopup);
          	dismissLoading();
        }
   	} catch (e) {
      	logException(e, "onSyncErrorCallBack", true);
    } 
}

/** Sync stop session callback **/
function onSyncStopSessionCallback(outputparams){
  	try {
      	printMessage("INSIDE onSyncStopSessionCallback - outputparams - ", outputparams);
        gblSyncProgress.isActive								= false;
        dismissProgressMessage();
        setFlxOverlayMsgOkButtonStyle(true);
        dismissLoading();
 	} catch (e) {
      	logException(e, "onSyncStopSessionCallback");
    } 
}

/** Sync reset success callback **/
function onSyncResetSuccessCallback(response){
  	dismissLoading();
}

/** Sync reset failure callback **/
function onSyncResetFailureCallback(response){
  	dismissLoading();
}

function setConfigDataInputs(clientContextObj) {
  	try {
      	clientContextObj.occupationCode 			= "06";
      	clientContextObj.workOrderTypes 			= true;
      	clientContextObj.calendarEventTypes 		= false;
      	clientContextObj.occupations 				= false;
      	clientContextObj.serviceCover 				= false;

      	clientContextObj.recommendationPriorities 	= false;
      	clientContextObj.riskStandards 				= false;
      	clientContextObj.measureOfSpaces 			= false;
      	clientContextObj.treatmentTypes 			= false;
      	clientContextObj.treatmentEquipment 		= false;
      	clientContextObj.windDirection 				= false;  
      	clientContextObj.skyCondition 				= false;
      	clientContextObj.paymentMethod 				= false;
      
      	clientContextObj.contractTypes 				= false;
        clientContextObj.portfolioStatuses 			= false;
        clientContextObj.jobTitles 					= false;
        clientContextObj.customerTypes 				= false;
        clientContextObj.customerContactTypes 		= false;
        clientContextObj.serviceLines 				= false;
        clientContextObj.detectors 					= false;
        clientContextObj.detectorTypes 				= false;        
        clientContextObj.infestationLevels 			= false;
        clientContextObj.infestationLocations 		= false;
        clientContextObj.issues 					= false;
        clientContextObj.issueTypes 				= false;        
        clientContextObj.noteStandards 				= false;
        clientContextObj.noteTypes 					= false;
        clientContextObj.pests 						= false;
        clientContextObj.pestTypes 					= false;        
        clientContextObj.physicalIDTypes 			= false;        
        clientContextObj.recommendationStandards 	= false;
        clientContextObj.recommendationTypes 		= false;
        clientContextObj.recommendationStatuses 	= false;
        clientContextObj.taskStandards 				= false;
        clientContextObj.taskTypes 					= false;
        clientContextObj.taskStatuses 				= false;
        clientContextObj.actionStandards 			= false;
        clientContextObj.preparations 				= false;
              
        return clientContextObj;
    } catch (e) {
        printMessage("INSIDE setConfigDataInputs - e.message - ", e.message);
    }
}

