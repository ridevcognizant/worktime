//****************Sync Version:Sync-Dev-8.0.0_v201711101237_r14*******************
// ****************Generated On Thu Jan 17 16:18:13 UTC 2019WorkTimeAttendance*******************
// **********************************Start WorkTimeAttendance's helper methods************************
if (typeof(kony) === "undefined") {
	kony = {};
}

if (typeof(kony.sync) === "undefined") {
	kony.sync = {};
}

if (typeof(kony.sync.log) === "undefined") {
	kony.sync.log = {};
}

if (typeof(sync) === "undefined") {
	sync = {};
}

if (typeof(sync.log) === "undefined") {
	sync.log = {};
}



if(typeof(com)=== "undefined"){ com = {}; }
if(typeof(com.wt)=== "undefined"){ com.wt = {}; }

/************************************************************************************
* Creates new WorkTimeAttendance
*************************************************************************************/
com.wt.WorkTimeAttendance = function(){
	this.attendanceId = null;
	this.employeeCode = null;
	this.email = null;
	this.startDateStr = null;
	this.startTimestamp = null;
	this.startLocLatitude = null;
	this.startLocLongitude = null;
	this.endDateStr = null;
	this.endTimestamp = null;
	this.endLocLatitude = null;
	this.endLocLongitude = null;
	this.attendanceType = null;
	this.startLocComment = null;
	this.endLocComment = null;
	this.isDeleted = null;
	this.lastModifiedDate = null;
	this.startTimestampStr = null;
	this.endTimestampStr = null;
	this.branchNumber = null;
	this.businessCode = null;
	this.countryCode = null;
	this.attendanceRef = null;
	this.nonServiceEventTypeCode = null;
	this.markForUpload = true;
};

com.wt.WorkTimeAttendance.prototype = {
	get attendanceId(){
		return this._attendanceId;
	},
	set attendanceId(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)){
			sync.log.error("Invalid data type for the attribute attendanceId in WorkTimeAttendance.\nExpected:\"integer\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._attendanceId = val;
	},
	get employeeCode(){
		return this._employeeCode;
	},
	set employeeCode(val){
		this._employeeCode = val;
	},
	get email(){
		return this._email;
	},
	set email(val){
		this._email = val;
	},
	get startDateStr(){
		return this._startDateStr;
	},
	set startDateStr(val){
		this._startDateStr = val;
	},
	get startTimestamp(){
		return this._startTimestamp;
	},
	set startTimestamp(val){
		this._startTimestamp = val;
	},
	get startLocLatitude(){
		return this._startLocLatitude;
	},
	set startLocLatitude(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)){
			sync.log.error("Invalid data type for the attribute startLocLatitude in WorkTimeAttendance.\nExpected:\"double\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._startLocLatitude = val;
	},
	get startLocLongitude(){
		return this._startLocLongitude;
	},
	set startLocLongitude(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)){
			sync.log.error("Invalid data type for the attribute startLocLongitude in WorkTimeAttendance.\nExpected:\"double\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._startLocLongitude = val;
	},
	get endDateStr(){
		return this._endDateStr;
	},
	set endDateStr(val){
		this._endDateStr = val;
	},
	get endTimestamp(){
		return this._endTimestamp;
	},
	set endTimestamp(val){
		this._endTimestamp = val;
	},
	get endLocLatitude(){
		return this._endLocLatitude;
	},
	set endLocLatitude(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)){
			sync.log.error("Invalid data type for the attribute endLocLatitude in WorkTimeAttendance.\nExpected:\"double\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._endLocLatitude = val;
	},
	get endLocLongitude(){
		return this._endLocLongitude;
	},
	set endLocLongitude(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)){
			sync.log.error("Invalid data type for the attribute endLocLongitude in WorkTimeAttendance.\nExpected:\"double\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._endLocLongitude = val;
	},
	get attendanceType(){
		return this._attendanceType;
	},
	set attendanceType(val){
		this._attendanceType = val;
	},
	get startLocComment(){
		return this._startLocComment;
	},
	set startLocComment(val){
		this._startLocComment = val;
	},
	get endLocComment(){
		return this._endLocComment;
	},
	set endLocComment(val){
		this._endLocComment = val;
	},
	get isDeleted(){
		return kony.sync.getBoolean(this._isDeleted)+"";
	},
	set isDeleted(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute isDeleted in WorkTimeAttendance.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._isDeleted = val;
	},
	get lastModifiedDate(){
		return this._lastModifiedDate;
	},
	set lastModifiedDate(val){
		this._lastModifiedDate = val;
	},
	get startTimestampStr(){
		return this._startTimestampStr;
	},
	set startTimestampStr(val){
		this._startTimestampStr = val;
	},
	get endTimestampStr(){
		return this._endTimestampStr;
	},
	set endTimestampStr(val){
		this._endTimestampStr = val;
	},
	get branchNumber(){
		return this._branchNumber;
	},
	set branchNumber(val){
		this._branchNumber = val;
	},
	get businessCode(){
		return this._businessCode;
	},
	set businessCode(val){
		this._businessCode = val;
	},
	get countryCode(){
		return this._countryCode;
	},
	set countryCode(val){
		this._countryCode = val;
	},
	get attendanceRef(){
		return this._attendanceRef;
	},
	set attendanceRef(val){
		this._attendanceRef = val;
	},
	get nonServiceEventTypeCode(){
		return this._nonServiceEventTypeCode;
	},
	set nonServiceEventTypeCode(val){
		this._nonServiceEventTypeCode = val;
	},
};

/************************************************************************************
* Retrieves all instances of WorkTimeAttendance SyncObject present in local database with
* given limit and offset where limit indicates the number of records to be retrieved
* and offset indicates number of rows to be ignored before returning the records.
* e.g. var orderByMap = []
* orderByMap[0] = {};
* orderByMap[0].key = "attendanceId";
* orderByMap[0].sortType ="desc";
* orderByMap[1] = {};
* orderByMap[1].key = "employeeCode";
* orderByMap[1].sortType ="asc";
* var limit = 20;
* var offset = 5;
* com.wt.WorkTimeAttendance.getAll(successcallback,errorcallback, orderByMap, limit, offset)
*************************************************************************************/
com.wt.WorkTimeAttendance.getAll = function(successcallback, errorcallback, orderByMap, limit, offset){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.getAll->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WorkTimeAttendance.getTableName();
	orderByMap = kony.sync.formOrderByClause("WorkTimeAttendance",orderByMap);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);	
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_orderBy(query, orderByMap);
				kony.sync.qb_limitOffset(query,limit,offset);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];

	function mySuccCallback(res){
		sync.log.trace("Entering com.wt.WorkTimeAttendance.getAll->successcallback");
		successcallback(com.wt.WorkTimeAttendance.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname,sql, params,mySuccCallback,errorcallback);
};

/************************************************************************************
* Returns number of WorkTimeAttendance present in local database.
*************************************************************************************/
com.wt.WorkTimeAttendance.getAllCount = function(successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.getAllCount function");
	com.wt.WorkTimeAttendance.getCount("",successcallback,errorcallback);
};

/************************************************************************************
* Returns number of WorkTimeAttendance using where clause in the local Database
*************************************************************************************/
com.wt.WorkTimeAttendance.getCount = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.getCount->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WorkTimeAttendance.getCount" , "getCount", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WorkTimeAttendance.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select count(*) from \"" + tbname + "\" " + wcs;
	kony.sync.single_execute_sql(dbname,sql, null, mySuccCallback, errorcallback);
	function mySuccCallback(res) {
		sync.log.trace("Entering com.wt.WorkTimeAttendance.getCount->successcallback");
		if(null!==res){
			var count = null;
			count = res["count(*)"];
			kony.sync.verifyAndCallClosure(successcallback, {count:count});
		}
		else{
			sync.log.error("Some error occured while getting the count");
		}
	}
};

/************************************************************************************
* Creates a new instance of WorkTimeAttendance in the local Database. The new record will 
* be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
com.wt.WorkTimeAttendance.prototype.create = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.wt.WorkTimeAttendance.prototype.create function");
	var valuestable = this.getValuesTable(true);
	com.wt.WorkTimeAttendance.create(valuestable, successcallback,errorcallback,this.markForUpload);
};
com.wt.WorkTimeAttendance.create = function(valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering  com.wt.WorkTimeAttendance.create->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	
	if(!kony.sync.validateInput(arguments, "com.wt.WorkTimeAttendance.create" , "create", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WorkTimeAttendance.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);

	if(kony.sync.attributeValidation(valuestable,"WorkTimeAttendance",errorcallback,true)===false){
		return;
	}
	
	function executeSuccess(){
		sync.log.trace("Entering  com.wt.WorkTimeAttendance.create->success callback");
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}

	if(kony.sync.enableORMValidations){
		var relationshipMap={};  
		relationshipMap = com.wt.WorkTimeAttendance.getRelationshipMap(relationshipMap,valuestable);
		kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
	}
	else{
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}
};

/************************************************************************************
* Creates number of new instances of WorkTimeAttendance in the local Database. These new 
* records will be merged with the enterprise datasource in the next Sync. Based upon 
* kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var valuesArray = [];
*		valuesArray[0] = {};
*		valuesArray[0].employeeCode = "employeeCode_0";
*		valuesArray[0].email = "email_0";
*		valuesArray[0].startDateStr = "startDateStr_0";
*		valuesArray[0].startTimestamp = "startTimestamp_0";
*		valuesArray[0].startLocLatitude = 0;
*		valuesArray[0].startLocLongitude = 0;
*		valuesArray[0].endDateStr = "endDateStr_0";
*		valuesArray[0].endTimestamp = "endTimestamp_0";
*		valuesArray[0].endLocLatitude = 0;
*		valuesArray[0].endLocLongitude = 0;
*		valuesArray[0].attendanceType = "attendanceType_0";
*		valuesArray[0].startLocComment = "startLocComment_0";
*		valuesArray[0].endLocComment = "endLocComment_0";
*		valuesArray[0].startTimestampStr = "startTimestampStr_0";
*		valuesArray[0].endTimestampStr = "endTimestampStr_0";
*		valuesArray[0].branchNumber = "branchNumber_0";
*		valuesArray[0].businessCode = "businessCode_0";
*		valuesArray[0].countryCode = "countryCode_0";
*		valuesArray[0].attendanceRef = "attendanceRef_0";
*		valuesArray[0].nonServiceEventTypeCode = "nonServiceEventTypeCode_0";
*		valuesArray[1] = {};
*		valuesArray[1].employeeCode = "employeeCode_1";
*		valuesArray[1].email = "email_1";
*		valuesArray[1].startDateStr = "startDateStr_1";
*		valuesArray[1].startTimestamp = "startTimestamp_1";
*		valuesArray[1].startLocLatitude = 1;
*		valuesArray[1].startLocLongitude = 1;
*		valuesArray[1].endDateStr = "endDateStr_1";
*		valuesArray[1].endTimestamp = "endTimestamp_1";
*		valuesArray[1].endLocLatitude = 1;
*		valuesArray[1].endLocLongitude = 1;
*		valuesArray[1].attendanceType = "attendanceType_1";
*		valuesArray[1].startLocComment = "startLocComment_1";
*		valuesArray[1].endLocComment = "endLocComment_1";
*		valuesArray[1].startTimestampStr = "startTimestampStr_1";
*		valuesArray[1].endTimestampStr = "endTimestampStr_1";
*		valuesArray[1].branchNumber = "branchNumber_1";
*		valuesArray[1].businessCode = "businessCode_1";
*		valuesArray[1].countryCode = "countryCode_1";
*		valuesArray[1].attendanceRef = "attendanceRef_1";
*		valuesArray[1].nonServiceEventTypeCode = "nonServiceEventTypeCode_1";
*		valuesArray[2] = {};
*		valuesArray[2].employeeCode = "employeeCode_2";
*		valuesArray[2].email = "email_2";
*		valuesArray[2].startDateStr = "startDateStr_2";
*		valuesArray[2].startTimestamp = "startTimestamp_2";
*		valuesArray[2].startLocLatitude = 2;
*		valuesArray[2].startLocLongitude = 2;
*		valuesArray[2].endDateStr = "endDateStr_2";
*		valuesArray[2].endTimestamp = "endTimestamp_2";
*		valuesArray[2].endLocLatitude = 2;
*		valuesArray[2].endLocLongitude = 2;
*		valuesArray[2].attendanceType = "attendanceType_2";
*		valuesArray[2].startLocComment = "startLocComment_2";
*		valuesArray[2].endLocComment = "endLocComment_2";
*		valuesArray[2].startTimestampStr = "startTimestampStr_2";
*		valuesArray[2].endTimestampStr = "endTimestampStr_2";
*		valuesArray[2].branchNumber = "branchNumber_2";
*		valuesArray[2].businessCode = "businessCode_2";
*		valuesArray[2].countryCode = "countryCode_2";
*		valuesArray[2].attendanceRef = "attendanceRef_2";
*		valuesArray[2].nonServiceEventTypeCode = "nonServiceEventTypeCode_2";
*		com.wt.WorkTimeAttendance.createAll(valuesArray, successcallback, errorcallback, true);
*************************************************************************************/
com.wt.WorkTimeAttendance.createAll = function(valuesArray, successcallback, errorcallback, markForUpload){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.createAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WorkTimeAttendance.createAll" , "createAll", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WorkTimeAttendance.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var isProperData = true;
	var arrayLen = 0;
	var errorInfo = [];
	var arrayLength = valuesArray.length;
	var errObject = null;
	var isReferentialIntegrityFailure = false;
	var errMsg = null;
	if(kony.sync.enableORMValidations){
		var newValuesArray = [];

		//column level validations
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
			if(kony.sync.attributeValidation(valuestable,"WorkTimeAttendance",errorcallback,true)===false){
				return;
			}

			newValuesArray[i] = valuestable;
		}
		valuesArray = newValuesArray;
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);
		var isError = false;
	}
	else{
		//copying by value
		var newValuesArray = [];
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			newValuesArray[i] = kony.sync.CreateCopy(valuesArray[i]);
		}
		valuesArray = newValuesArray;
		kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
	}

	function transactionErrorCallback(){
		if(isError==true){
			//Statement error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
		}
		else{
			//Transaction error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}

	function transactionSuccessCallback(){
		sync.log.trace("Entering  com.wt.WorkTimeAttendance.createAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
		}
		else{
			if(isReferentialIntegrityFailure){
				kony.sync.verifyAndCallClosure(errorcallback, errObject);
			}
		}
	}
	
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  com.wt.WorkTimeAttendance.createAll->checkIntegrity");
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var relationshipMap={};  
			relationshipMap = com.wt.WorkTimeAttendance.getRelationshipMap(relationshipMap,valuesArray[i]);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				isReferentialIntegrityFailure = true;
				return;
			}
		}
	}
};
/************************************************************************************
* Updates WorkTimeAttendance using primary key in the local Database. The update will be
* merged with the enterprise datasource in the next Sync.
*************************************************************************************/
com.wt.WorkTimeAttendance.prototype.updateByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.wt.WorkTimeAttendance.prototype.updateByPK function");
	var pks = this.getPKTable();
	var valuestable = this.getValuesTable(false);
	com.wt.WorkTimeAttendance.updateByPK(pks,valuestable, successcallback,errorcallback,this.markForUpload);
};
com.wt.WorkTimeAttendance.updateByPK = function(pks,valuestable, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering  com.wt.WorkTimeAttendance.updateByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WorkTimeAttendance.updateByPK",  "updateByPk", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WorkTimeAttendance.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [ ];

	if(com.wt.WorkTimeAttendance.pkCheck(pks,wcs,errorcallback,"updating")===false){
		return;
	}

	if(kony.sync.attributeValidation(valuestable,"WorkTimeAttendance",errorcallback,false)===false){
		return;
	}

	var relationshipMap={};  
	relationshipMap = com.wt.WorkTimeAttendance.getRelationshipMap(relationshipMap,valuestable);

	kony.sync.updateByPK(tbname, dbname, relationshipMap, pks,valuestable, successcallback,errorcallback, markForUpload, wcs);
};

/************************************************************************************
* Updates WorkTimeAttendance(s) using where clause in the local Database. The update(s)
* will be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
com.wt.WorkTimeAttendance.update = function(wcs, valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.update function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WorkTimeAttendance.update",  "update", errorcallback)){
		return;
	}

	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WorkTimeAttendance.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);

	if(kony.sync.attributeValidation(valuestable,"WorkTimeAttendance",errorcallback,false)===false){
		return;
	}
	function executeSuccess(){
		sync.log.trace("Entering  com.wt.WorkTimeAttendance.update-> success callback of Integrity Check");
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, com.wt.WorkTimeAttendance.getPKTable());
	}

	if(kony.sync.enableORMValidations){
		var relationshipMap={};  
		relationshipMap = com.wt.WorkTimeAttendance.getRelationshipMap(relationshipMap,valuestable);
		kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
	}
	else{
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, com.wt.WorkTimeAttendance.getPKTable());
	}
};

/************************************************************************************
* Updates WorkTimeAttendance(s) satisfying one or more where clauses in the local Database. 
* The update(s) will be merged with the enterprise datasource in the next Sync.
* Based upon kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var inputArray = [];
*		inputArray[0] = {};
*		inputArray[0].changeSet = {};
*		inputArray[0].changeSet.employeeCode = "employeeCode_updated0";
*		inputArray[0].changeSet.email = "email_updated0";
*		inputArray[0].changeSet.startDateStr = "startDateStr_updated0";
*		inputArray[0].changeSet.startTimestamp = "startTimestamp_updated0";
*		inputArray[0].whereClause = "where attendanceId = 0";
*		inputArray[1] = {};
*		inputArray[1].changeSet = {};
*		inputArray[1].changeSet.employeeCode = "employeeCode_updated1";
*		inputArray[1].changeSet.email = "email_updated1";
*		inputArray[1].changeSet.startDateStr = "startDateStr_updated1";
*		inputArray[1].changeSet.startTimestamp = "startTimestamp_updated1";
*		inputArray[1].whereClause = "where attendanceId = 1";
*		inputArray[2] = {};
*		inputArray[2].changeSet = {};
*		inputArray[2].changeSet.employeeCode = "employeeCode_updated2";
*		inputArray[2].changeSet.email = "email_updated2";
*		inputArray[2].changeSet.startDateStr = "startDateStr_updated2";
*		inputArray[2].changeSet.startTimestamp = "startTimestamp_updated2";
*		inputArray[2].whereClause = "where attendanceId = 2";
*		com.wt.WorkTimeAttendance.updateAll(inputArray,successcallback,errorcallback);
*************************************************************************************/
com.wt.WorkTimeAttendance.updateAll = function(inputArray, successcallback, errorcallback, markForUpload) {
	sync.log.trace("Entering com.wt.WorkTimeAttendance.updateAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WorkTimeAttendance.updateAll",  "updateAll", errorcallback)){
		return;
	}
	var dbname = "WorkTime_EMEA_SIT_2_1";
	var tbname = "WorkTimeAttendance";
	var isError = false;
	var errObject = null;
	if(markForUpload == false || markForUpload == "false"){
		markForUpload="false"
	}
	else{
		markForUpload="true"
	}
	if((kony.sync.enableORMValidations)){

		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var v = inputArray[i];
			var valuestable = v.changeSet;
			var isEmpty = true;
			for(var key in valuestable){
				isEmpty = false;
				break;
			}
			if(isEmpty){
				errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeNullValue,kony.sync.getErrorMessage(kony.sync.errorCodeNullValue)));
				return;
			}
			var wcs = v.whereClause;
			var twcs = wcs;
			if(kony.sync.attributeValidation(valuestable,"WorkTimeAttendance",errorcallback,false)===false){
				return;
			}

			newInputArray[i] = [];
			newInputArray[i].changeSet = valuestable;
			newInputArray[i].whereClause = wcs;
		}
		inputArray = newInputArray;
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);

	}
	else{
		//copying by value
		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
		    var v = inputArray[i];
		    newInputArray[i] = kony.sync.CreateCopy(v);
		}
		inputArray = newInputArray;
		kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,errorcallback,markForUpload, com.wt.WorkTimeAttendance.getPKTable());
	}
	
		function transactionSuccessCallback(){
		sync.log.trace("Entering  com.wt.WorkTimeAttendance.updateAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,transactionErrorCallback,markForUpload, com.wt.WorkTimeAttendance.getPKTable());
		}
	}

	function transactionErrorCallback(){
		if(errObject===false){
			//Sql statement error has occcurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
			
		}
		else if(errObject!==null){
			// Referential integrity error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, errObject);
		}
		else{
			//Transaction error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  com.wt.WorkTimeAttendance.updateAll->checkIntegrity");
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var relationshipMap={}; 
			relationshipMap = com.wt.WorkTimeAttendance.getRelationshipMap(relationshipMap,inputArray[i].changeSet);
			sync.log.debug("Relationship Map for Integrity check created:", relationshipMap);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				kony.sync.rollbackTransaction(tx);
				return;
			}
		}
	}


}
/************************************************************************************
* Deletes WorkTimeAttendance using primary key from the local Database. The record will be
* deleted from the enterprise datasource in the next Sync.
*************************************************************************************/
com.wt.WorkTimeAttendance.prototype.deleteByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.prototype.deleteByPK function");
	var pks = this.getPKTable();
	com.wt.WorkTimeAttendance.deleteByPK(pks,successcallback,errorcallback,this.markForUpload);
};
com.wt.WorkTimeAttendance.deleteByPK = function(pks, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.deleteByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WorkTimeAttendance.deleteByPK",  "deleteByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WorkTimeAttendance.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var twcs = [];
	var deletedRows;
	var record = "";
	if(com.wt.WorkTimeAttendance.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}	
	twcs = kony.sync.CreateCopy(wcs);
	function WorkTimeAttendanceTransactionCallback(tx){
		sync.log.trace("Entering com.wt.WorkTimeAttendance.deleteByPK->WorkTimeAttendance_PKPresent successcallback");
		record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(record===false){
			isError = true;
			return;
		}
		if (null !== record) {
		}else{
			pkNotFound = true;
		}
		var deletedRows = kony.sync.remove(tx, tbname, wcs, false, markForUpload, null);
			if(deletedRows === false){
				isError = true;
			}
	}
	
	function WorkTimeAttendanceErrorCallback(){
		sync.log.error("Entering com.wt.WorkTimeAttendance.deleteByPK->relationship failure callback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}

	}
	function WorkTimeAttendanceSuccessCallback(){
		sync.log.trace("Entering com.wt.WorkTimeAttendance.deleteByPK->relationship success callback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering com.wt.WorkTimeAttendance.deleteByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
		
		var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(dbconnection != null){
			kony.sync.startTransaction(dbconnection, WorkTimeAttendanceTransactionCallback, WorkTimeAttendanceSuccessCallback, WorkTimeAttendanceErrorCallback, "Single Execute");
		}

};

/************************************************************************************
* Deletes WorkTimeAttendance(s) using where clause from the local Database. The record(s)
* will be deleted from the enterprise datasource in the next Sync.
* e.g. com.wt.WorkTimeAttendance.remove("where employeeCode like 'A%'", successcallback,errorcallback, true);
*************************************************************************************/
com.wt.WorkTimeAttendance.remove = function(wcs, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.remove->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WorkTimeAttendance.remove",  "remove", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WorkTimeAttendance.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;
	var record = "";

	function WorkTimeAttendance_removeTransactioncallback(tx){
			wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, false, markForUpload, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function WorkTimeAttendance_removeSuccess(){
		sync.log.trace("Entering com.wt.WorkTimeAttendance.remove->WorkTimeAttendance_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering com.wt.WorkTimeAttendance.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering com.wt.WorkTimeAttendance.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, WorkTimeAttendance_removeTransactioncallback, WorkTimeAttendance_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Deletes WorkTimeAttendance using primary key from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
com.wt.WorkTimeAttendance.prototype.removeDeviceInstanceByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.prototype.removeDeviceInstanceByPK function");
	var pks = this.getPKTable();
	com.wt.WorkTimeAttendance.removeDeviceInstanceByPK(pks,successcallback,errorcallback);
};
com.wt.WorkTimeAttendance.removeDeviceInstanceByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.removeDeviceInstanceByPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WorkTimeAttendance.removeDeviceInstanceByPK",  "removeDeviceInstanceByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WorkTimeAttendance.getTableName();
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var deletedRows;
	if(com.wt.WorkTimeAttendance.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}
	
	function WorkTimeAttendanceTransactionCallback(tx){
		sync.log.trace("Entering com.wt.WorkTimeAttendance.removeDeviceInstanceByPK -> WorkTimeAttendanceTransactionCallback");
		var record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(null !== record && false !=record) {
			deletedRows = kony.sync.remove(tx, tbname, wcs, true, null, null);
			if(deletedRows === false){
				isError = true;
			}
		}else{
			pkNotFound = true;
		}
	}
	
	function WorkTimeAttendanceErrorCallback(){
		sync.log.error("Entering com.wt.WorkTimeAttendance.removeDeviceInstanceByPK -> WorkTimeAttendanceErrorCallback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	function WorkTimeAttendanceSuccessCallback(){
		sync.log.trace("Entering com.wt.WorkTimeAttendance.removeDeviceInstanceByPK -> WorkTimeAttendanceSuccessCallback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering com.wt.WorkTimeAttendance.removeDeviceInstanceByPK -> PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
	
	var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(dbconnection != null){
		kony.sync.startTransaction(dbconnection, WorkTimeAttendanceTransactionCallback, WorkTimeAttendanceSuccessCallback, WorkTimeAttendanceErrorCallback, "Single Execute");
	}

};

/************************************************************************************
* Deletes WorkTimeAttendance(s) using where clause from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
com.wt.WorkTimeAttendance.removeDeviceInstance = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.removeDeviceInstance->main function");
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WorkTimeAttendance.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;

	function WorkTimeAttendance_removeTransactioncallback(tx){
		wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, true, null, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function WorkTimeAttendance_removeSuccess(){
		sync.log.trace("Entering com.wt.WorkTimeAttendance.remove->WorkTimeAttendance_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering com.wt.WorkTimeAttendance.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering com.wt.WorkTimeAttendance.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, WorkTimeAttendance_removeTransactioncallback, WorkTimeAttendance_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Retrieves WorkTimeAttendance using primary key from the local Database. 
*************************************************************************************/
com.wt.WorkTimeAttendance.prototype.getAllDetailsByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.prototype.getAllDetailsByPK function");
	var pks = this.getPKTable();
	com.wt.WorkTimeAttendance.getAllDetailsByPK(pks,successcallback,errorcallback);
};
com.wt.WorkTimeAttendance.getAllDetailsByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.getAllDetailsByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WorkTimeAttendance.getAllDetailsByPK",  "getAllDetailsByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WorkTimeAttendance.getTableName();
	var wcs = [];
	if(com.wt.WorkTimeAttendance.pkCheck(pks,wcs,errorcallback,"searching")===false){
		return;
	}
	twcs = kony.sync.CreateCopy(wcs);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, wcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	function mySuccCallback(res){
		sync.log.trace("Entering com.wt.WorkTimeAttendance.getAllDetailsByPK-> success callback function");
		successcallback(com.wt.WorkTimeAttendance.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};






/************************************************************************************
* Retrieves WorkTimeAttendance(s) using where clause from the local Database. 
* e.g. com.wt.WorkTimeAttendance.find("where employeeCode like 'A%'", successcallback,errorcallback);
*************************************************************************************/
com.wt.WorkTimeAttendance.find = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.find function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WorkTimeAttendance.find",  "find", errorcallback)){
		return;
	}
	//wcs will be a string formed by the user.
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WorkTimeAttendance.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select * from \"" + tbname + "\" " + wcs;
	function mySuccCallback(res){
		kony.sync.verifyAndCallClosure(successcallback, com.wt.WorkTimeAttendance.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccCallback, errorcallback);
};

/************************************************************************************
* Marks instance of WorkTimeAttendance with given primary key for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
com.wt.WorkTimeAttendance.prototype.markForUploadbyPK = function(successcallback, errorcallback){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.prototype.markForUploadbyPK function");
	var pks = this.getPKTable();
	com.wt.WorkTimeAttendance.markForUploadbyPK(pks, successcallback, errorcallback);
};
com.wt.WorkTimeAttendance.markForUploadbyPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.markForUploadbyPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WorkTimeAttendance.markForUploadbyPK",  "markForUploadbyPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WorkTimeAttendance.getTableName();
	var isError = false;
	var recordsFound = false;
	var recordsMarkedForUpload = 0;
	var wcs = [];
	if(com.wt.WorkTimeAttendance.pkCheck(pks, wcs, errorcallback, "marking for upload by PK")===false){
		return;
	}

	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);		
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = [];
		twcs = wcs;
		kony.table.insert(twcs,{key : kony.sync.historyTableChangeTypeColumn, value : record[kony.sync.historyTableChangeTypeColumn], optype : "EQ",comptype : "AND"});
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
					kony.sync.qb_where(query, twcs);
		kony.table.remove(twcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);
	}
	
	function single_transaction_callback (tx){
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_select(query, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query, tbname);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		var resultSet = kony.sync.executeSql(tx, sql, params);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		if(num_records > 0){
			recordsFound = true;
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, 0);
			var changeType = record[kony.sync.mainTableChangeTypeColumn];
			if(!kony.sync.isNullOrUndefined(changeType) && kony.string.startsWith(""+changeType,"9")){
				recordsMarkedForUpload = 1;
				if(markRecordForUpload(tx, record) === false){
					isError = true;
					return;
				}
			}
		}
					
				
		var query1 =kony.sync.qb_createQuery();
					kony.sync.qb_select(query1, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query1, tbname + kony.sync.historyTableName);
					kony.sync.qb_where(query1, wcs);
		var query1_compile = kony.sync.qb_compile(query1);
		var sql1 = query1_compile[0];
		var params1 = query1_compile[1];
		var resultSet1 = kony.sync.executeSql (tx, sql1, params1);
		if(resultSet1!==false){
			var num_records = resultSet1.rows.length;
			for(var i = 0; i <= num_records - 1; i++ ){
				var record = kony.db.sqlResultsetRowItem(tx, resultSet1, i);
				if(markRecordForUploadHistory(tx, record) === false){
					isError = true;
					return;
				}
				recordsFound = true;
			}
		}
		else{
			isError = true;
		}
	}
	function single_transaction_success_callback(){
		if(recordsFound === true){
			kony.sync.verifyAndCallClosure(successcallback , {count:recordsMarkedForUpload});
		}
		else{
			kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
		}
	}
	
	function single_transaction_error_callback(res){
		if (!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Marks instance(s) of WorkTimeAttendance matching given where clause for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
com.wt.WorkTimeAttendance.markForUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.markForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WorkTimeAttendance.markForUpload",  "markForUpload", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WorkTimeAttendance.getTableName();
	var isError = false;
	var num_records_main = 0;
	wcs = kony.sync.validateWhereClause(wcs);
	if(!kony.sync.isNull(wcs) && !kony.sync.isEmptyString(wcs)) {
		wcs = wcs + " and " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}else{	
		wcs = "where " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}
	
	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0] + " " + wcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = "";
		twcs = wcs;
		twcs = twcs + " AND " + kony.sync.historyTableChangeTypeColumn + " = " + record[kony.sync.historyTableChangeTypeColumn];
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0]  + " " + twcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function single_transaction_callback (tx){
		sync.log.trace("Entering com.wt.WorkTimeAttendance.markForUpload->single_transaction_callback");
		//updating main table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from \"" + tbname + "\" " + wcs ;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}
		
		num_records_main = resultSet.rows.length;
		for(var i = 0; i < num_records_main; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUpload(tx, record) === false){
				isError = true;
				return;
			}
		}
		
		//updating history table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from " + tbname + kony.sync.historyTableName + " " + wcs;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		for ( var i = 0; i <= num_records - 1; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUploadHistory(tx, record)=== false){
				isError = true;
				return;
			}
		}
	}
	
	function single_transaction_success_callback(){
		sync.log.trace("Entering com.wt.WorkTimeAttendance.markForUpload->single_transaction_success_callback");
		kony.sync.verifyAndCallClosure(successcallback, {count:num_records_main});
	}
	
	function single_transaction_error_callback(){
		sync.log.error("Entering com.wt.WorkTimeAttendance.markForUpload->single_transaction_error_callback");
		if(!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Retrieves instance(s) of WorkTimeAttendance pending for upload. Records are marked for
* pending upload if they have been updated or created locally and the changes have
* not been merged with enterprise datasource.
*************************************************************************************/
com.wt.WorkTimeAttendance.getPendingUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.getPendingUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WorkTimeAttendance.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var sql;
	if(typeof(wcs) === "string" && wcs != null){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname + "\" "+ wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.wt.WorkTimeAttendance.getPendingUpload->successcallback function");
		kony.sync.verifyAndCallClosure(successcallback, com.wt.WorkTimeAttendance.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of WorkTimeAttendance pending for acknowledgement. This is relevant
* when the SyncObject is part of the SyncScope whose SyncStrategy is PersistentSync.
* In persistent Sync the  records in the local database are put into a pending 
* acknowledgement state after an upload.
*************************************************************************************/
com.wt.WorkTimeAttendance.getPendingAcknowledgement = function(successcallback, errorcallback){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.getPendingAcknowledgement->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WorkTimeAttendance.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var mysql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" <> "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	kony.sync.single_select_execute(dbname, mysql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.wt.WorkTimeAttendance.getPendingAcknowledgement success callback function");
		kony.sync.verifyAndCallClosure(successcallback, com.wt.WorkTimeAttendance.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of WorkTimeAttendance deferred for upload.
*************************************************************************************/
com.wt.WorkTimeAttendance.getDeferredUpload = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.getDeferredUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WorkTimeAttendance.getTableName();
	var sql;
	if(typeof(wcs) === "string" && wcs != null ){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname +  "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'";
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'"; 
	}
	
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.wt.WorkTimeAttendance.getDeferredUpload->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, com.wt.WorkTimeAttendance.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
};

/************************************************************************************
* Rollbacks all changes to WorkTimeAttendance in local database to last synced state
*************************************************************************************/
com.wt.WorkTimeAttendance.rollbackPendingLocalChanges = function(successcallback, errorcallback){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.rollbackPendingLocalChanges->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WorkTimeAttendance.getTableName();
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.wt.WorkTimeAttendance.rollbackPendingLocalChanges->main function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}		
};

/************************************************************************************
* Rollbacks changes to WorkTimeAttendance's record with given primary key in local 
* database to last synced state
*************************************************************************************/
com.wt.WorkTimeAttendance.prototype.rollbackPendingLocalChangesByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.prototype.rollbackPendingLocalChangesByPK function");
	var pks = this.getPKTable();
	com.wt.WorkTimeAttendance.rollbackPendingLocalChangesByPK(pks,successcallback,errorcallback);
};
com.wt.WorkTimeAttendance.rollbackPendingLocalChangesByPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.rollbackPendingLocalChangesByPK->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WorkTimeAttendance.rollbackPendingLocalChangesByPK",  "rollbackPendingLocalChangesByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WorkTimeAttendance.getTableName();
	var wcs = [];
	if(com.wt.WorkTimeAttendance.pkCheck(pks,wcs,errorcallback,"rollbacking")===false){
		return;
	}	
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, wcs, mySuccesscallback, pkNotFoundErrCallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.wt.WorkTimeAttendance.rollbackPendingLocalChangesByPK->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering com.wt.WorkTimeAttendance.rollbackPendingLocalChangesByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
};

/************************************************************************************
* isRecordDeferredForUpload returns true or false depending on whether WorkTimeAttendance's record  
* with given primary key got deferred in last sync
*************************************************************************************/
com.wt.WorkTimeAttendance.prototype.isRecordDeferredForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.wt.WorkTimeAttendance.prototype.isRecordDeferredForUpload function");
	var pks = this.getPKTable();
	com.wt.WorkTimeAttendance.isRecordDeferredForUpload(pks,successcallback,errorcallback);
};
com.wt.WorkTimeAttendance.isRecordDeferredForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.isRecordDeferredForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WorkTimeAttendance.isRecordDeferredForUpload",  "isRecordDeferredForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WorkTimeAttendance.getTableName();
	var wcs = [] ;
	var flag;
	if(com.wt.WorkTimeAttendance.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.wt.WorkTimeAttendance.isRecordDeferredForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {deferred:flag});
	}
};

/************************************************************************************
* isRecordPendingForUpload returns true or false depending on whether WorkTimeAttendance's record  
* with given primary key is pending for upload
*************************************************************************************/
com.wt.WorkTimeAttendance.prototype.isRecordPendingForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.wt.WorkTimeAttendance.prototype.isRecordPendingForUpload function");
	var pks = this.getPKTable();
	com.wt.WorkTimeAttendance.isRecordPendingForUpload(pks,successcallback,errorcallback);
};
com.wt.WorkTimeAttendance.isRecordPendingForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.isRecordPendingForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WorkTimeAttendance.isRecordPendingForUpload",  "isRecordPendingForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WorkTimeAttendance.getTableName();
	var wcs = [] ;
	var flag;
	if(com.wt.WorkTimeAttendance.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "NOT LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.wt.WorkTimeAttendance.isRecordPendingForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {pending:flag});
	}
};




/************************************************************************************
* Start of helper functions used internally, not to be used as ORMs
*************************************************************************************/

//Deletes all the dependant tables in the relationship tables.Need to pass transaction handler as input
com.wt.WorkTimeAttendance.removeCascade = function(tx, wcs, errorcallback, markForUpload, isCascade, parentTable, isLocal){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.removeCascade function");
	var tbname = com.wt.WorkTimeAttendance.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	function removeCascadeChildren(){
	}
	if(isCascade){
		if(removeCascadeChildren()===false){
			return false;
		}
		if(kony.sync.deleteBatch(tx, tbname, wcs, isLocal,markForUpload, null)===false){
			return false;
		}
		return true;
	}else{
		var sql = "select * from \"" + tbname + "\" " + wcs;
		var resultSet = kony.sync.executeSql(tx, sql, null);
		if(resultSet===false){
			return false;
		}	
		var num_records = resultSet.rows.length;
		if(num_records === 0){
			return true;
		}else{
			sync.log.error(kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable));
			errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeReferentialIntegrity,kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable)));
			return false;
		}
	}
};


com.wt.WorkTimeAttendance.convertTableToObject = function(res){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.convertTableToObject function");
	objMap = [];
	if(res!==null){
		for(var i in res){
			var obj = new com.wt.WorkTimeAttendance();
			obj.attendanceId = res[i].attendanceId;
			obj.employeeCode = res[i].employeeCode;
			obj.email = res[i].email;
			obj.startDateStr = res[i].startDateStr;
			obj.startTimestamp = res[i].startTimestamp;
			obj.startLocLatitude = res[i].startLocLatitude;
			obj.startLocLongitude = res[i].startLocLongitude;
			obj.endDateStr = res[i].endDateStr;
			obj.endTimestamp = res[i].endTimestamp;
			obj.endLocLatitude = res[i].endLocLatitude;
			obj.endLocLongitude = res[i].endLocLongitude;
			obj.attendanceType = res[i].attendanceType;
			obj.startLocComment = res[i].startLocComment;
			obj.endLocComment = res[i].endLocComment;
			obj.isDeleted = res[i].isDeleted;
			obj.lastModifiedDate = res[i].lastModifiedDate;
			obj.startTimestampStr = res[i].startTimestampStr;
			obj.endTimestampStr = res[i].endTimestampStr;
			obj.branchNumber = res[i].branchNumber;
			obj.businessCode = res[i].businessCode;
			obj.countryCode = res[i].countryCode;
			obj.attendanceRef = res[i].attendanceRef;
			obj.nonServiceEventTypeCode = res[i].nonServiceEventTypeCode;
			obj.markForUpload = (Math.floor(res[i].konysyncchangetype/10)==9)? false:true;
			objMap[i] = obj;
		}
	}
	return objMap;
};

com.wt.WorkTimeAttendance.filterAttributes = function(valuestable, insert){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.filterAttributes function");
	var attributeTable = {};
	attributeTable.attendanceId = "attendanceId";
	attributeTable.employeeCode = "employeeCode";
	attributeTable.email = "email";
	attributeTable.startDateStr = "startDateStr";
	attributeTable.startTimestamp = "startTimestamp";
	attributeTable.startLocLatitude = "startLocLatitude";
	attributeTable.startLocLongitude = "startLocLongitude";
	attributeTable.endDateStr = "endDateStr";
	attributeTable.endTimestamp = "endTimestamp";
	attributeTable.endLocLatitude = "endLocLatitude";
	attributeTable.endLocLongitude = "endLocLongitude";
	attributeTable.attendanceType = "attendanceType";
	attributeTable.startLocComment = "startLocComment";
	attributeTable.endLocComment = "endLocComment";
	attributeTable.startTimestampStr = "startTimestampStr";
	attributeTable.endTimestampStr = "endTimestampStr";
	attributeTable.branchNumber = "branchNumber";
	attributeTable.businessCode = "businessCode";
	attributeTable.countryCode = "countryCode";
	attributeTable.attendanceRef = "attendanceRef";
	attributeTable.nonServiceEventTypeCode = "nonServiceEventTypeCode";

	var PKTable = {};
	PKTable.attendanceId = {}
	PKTable.attendanceId.name = "attendanceId";
	PKTable.attendanceId.isAutoGen = true;
	var newvaluestable = {};
	for (var k in valuestable){
		var v = valuestable[k];
		if(kony.sync.isNull(attributeTable[k])) { 
			sync.log.warn("Ignoring the attribute " + k + " for the SyncObject WorkTimeAttendance. "  + k + " not defined as an attribute in SyncConfiguration.");
		}else if(!kony.sync.isNull(PKTable[k])) {
			if(insert===false){
				sync.log.warn("Ignoring the primary key " + k + " for the SyncObject WorkTimeAttendance. Primary Key should not be the part of the attributes to be updated in the local device database.");
			}else if(PKTable[k].isAutoGen){
				sync.log.warn("Ignoring the auto-generated primary key " + k + " for the SyncObject WorkTimeAttendance. Auto-generated Primary Key should not be the part of the attributes to be inserted in the local device database.");
			}else{
				newvaluestable[k] = v;
			}
		}
		else{
			newvaluestable[k] = v;
		}
	}
	return newvaluestable;
};

com.wt.WorkTimeAttendance.formOrderByClause = function(orderByMap){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.formOrderByClause function");
	if(!kony.sync.isNull(orderByMap)){
		var valuestable = kony.sync.convertOrderByMapToValuesTable(orderByMap);
		//var filteredValuestable = com.wt.WorkTimeAttendance.filterAttributes(valuestable, true);
		return kony.sync.convertToValuesTableOrderByMap(orderByMap,valuestable);
	}
	return null;
};

com.wt.WorkTimeAttendance.prototype.getValuesTable = function(isInsert){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.prototype.getValuesTable function");
	var valuesTable = {};
	if(isInsert===true){
		valuesTable.attendanceId = this.attendanceId;
	}
	valuesTable.employeeCode = this.employeeCode;
	valuesTable.email = this.email;
	valuesTable.startDateStr = this.startDateStr;
	valuesTable.startTimestamp = this.startTimestamp;
	valuesTable.startLocLatitude = this.startLocLatitude;
	valuesTable.startLocLongitude = this.startLocLongitude;
	valuesTable.endDateStr = this.endDateStr;
	valuesTable.endTimestamp = this.endTimestamp;
	valuesTable.endLocLatitude = this.endLocLatitude;
	valuesTable.endLocLongitude = this.endLocLongitude;
	valuesTable.attendanceType = this.attendanceType;
	valuesTable.startLocComment = this.startLocComment;
	valuesTable.endLocComment = this.endLocComment;
	valuesTable.startTimestampStr = this.startTimestampStr;
	valuesTable.endTimestampStr = this.endTimestampStr;
	valuesTable.branchNumber = this.branchNumber;
	valuesTable.businessCode = this.businessCode;
	valuesTable.countryCode = this.countryCode;
	valuesTable.attendanceRef = this.attendanceRef;
	valuesTable.nonServiceEventTypeCode = this.nonServiceEventTypeCode;
	return valuesTable;
};

com.wt.WorkTimeAttendance.prototype.getPKTable = function(){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.prototype.getPKTable function");
	var pkTable = {};
	pkTable.attendanceId = {key:"attendanceId",value:this.attendanceId};
	return pkTable;
};

com.wt.WorkTimeAttendance.getPKTable = function(){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.getPKTable function");
	var pkTable = [];
	pkTable.push("attendanceId");
	return pkTable;
};

com.wt.WorkTimeAttendance.pkCheck = function(pks,wcs,errorcallback,opName){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.pkCheck function");
	var wc = [];
	if(kony.sync.isNull(pks)){
		sync.log.error("Primary Key attendanceId not specified in  " + opName + "  an item in WorkTimeAttendance");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("attendanceId",opName,"WorkTimeAttendance")));
		return false;	
	}
	else if(kony.sync.isValidJSTable(pks)){
		if(!kony.sync.isNull(pks.attendanceId)){
			if(!kony.sync.isNull(pks.attendanceId.value)){
				wc.key = "attendanceId";
				wc.value = pks.attendanceId.value;
			}
			else{
				wc.key = "attendanceId";
				wc.value = pks.attendanceId;
			}
		}else{
			sync.log.error("Primary Key attendanceId not specified in  " + opName + "  an item in WorkTimeAttendance");
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("attendanceId",opName,"WorkTimeAttendance")));
			return false;
		}
	}
	else{
		wc.key = "attendanceId";
		wc.value = pks;
	}	
	kony.table.insert(wcs,wc);
	return true;
};

com.wt.WorkTimeAttendance.validateNull = function (valuestable,errorcallback){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.validateNull function");
	return true;
};

com.wt.WorkTimeAttendance.validateNullInsert = function (valuestable,errorcallback){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.validateNullInsert function");
	return true;
};

com.wt.WorkTimeAttendance.getRelationshipMap = function(relationshipMap,valuestable){
	sync.log.trace("Entering com.wt.WorkTimeAttendance.getRelationshipMap function");
	var r1 = {};

	return relationshipMap;
};


com.wt.WorkTimeAttendance.checkPKValueTables = function (valuetables)	{
	var checkPksNotNullFlag = true;
	for(var i = 0; i < valuetables.length; i++)	{
		if(kony.sync.isNull(valuetables[i])){
			checkPksNotNullFlag = false;
			break;
		}
	}
	return checkPksNotNullFlag;
};

com.wt.WorkTimeAttendance.getTableName = function(){
	return "WorkTimeAttendance";
};




// **********************************End WorkTimeAttendance's helper methods************************