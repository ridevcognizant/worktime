/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : UI Screen - Settings(Burger Menu) - Methods
***********************************************************************/

/** Settings Menu (Burger Menu)- UI refreshing like i18n updates, default visibility and all **/
function refreshSettingsUI(){
  	try {
      	printMessage("INSIDE refreshSettingsUI");
      	var frmObj								= kony.application.getCurrentForm();
        frmObj.flxSettingsContainer.centerX 	= "-50%";  	
        frmObj.lblVersion.text					= getI18nString("common.label.version") + " " + appConfig.appVersion;
        frmObj.lblUsername.text					= gblSelData.EMPLOYEE_NAME;
      	if (!isEmpty(gblSelData.BRANCH_NAME)) {
          	frmObj.lblMoreData.text				= gblSelData.COUNTRY_CODE + " - " + gblSelData.BUSINESS_CODE + " (" + gblSelData.BRANCH_NUMBER + " - " + gblSelData.BRANCH_NAME + ")";
        } else {
          	frmObj.lblMoreData.text				= gblSelData.COUNTRY_CODE + " - " + gblSelData.BUSINESS_CODE + " (" + getI18nString("common.label.branchNumber") + " " + gblSelData.BRANCH_NUMBER + ")";
        }
        frmObj.lblSendDB.text					= getI18nString("common.label.sendDatabase");
        frmObj.lblLogout.text					= getI18nString("common.label.logout");
        frmObj.lblSync.text						= getI18nString("common.label.sync");
        displayLastSyncDate();	
    } catch(e) {
      	logException(e, "refreshSettingsUI", true);
	}         
}

/** Settings Menu (Burger Menu)- Open menu button action **/
function onClickOpenMenuButton(){
  	function openMenu_Callback(){
      	printMessage("INSIDE openMenu_Callback");
    }
  
  	try {
      	printMessage("INSIDE onClickOpenMenuButton");
  		var frmObj								= kony.application.getCurrentForm();
  		displayLastSyncDate(); 
      
      	frmObj.flxSettingsContainer.animate(
			kony.ui.createAnimation({"100": {"centerX": "50%", "stepConfig": {"timingFunction": kony.anim.LINEAR}}}),
        	{"delay": 0, "iterationCount": 1, "fillMode": kony.anim.FILL_MODE_FORWARDS, "duration": 0.25},
      		{"animationEnd": openMenu_Callback}
        );
 	} catch(e) {
      	logException(e, "onClickOpenMenuButton", true);
	} 
}

/** Settings Menu (Burger Menu)- Close menu button action **/
function closeSettingsMenu(){
  	function closeMenu_Callback(){
      	printMessage("INSIDE closeMenu_Callback");
    }
  
  	try {
      	printMessage("INSIDE closeSettingsMenu");
		var frmObj								= kony.application.getCurrentForm();
	
		frmObj.flxSettingsContainer.animate(
			kony.ui.createAnimation({"100" : {"centerX": "-50%", "stepConfig": {"timingFunction" :kony.anim.LINEAR}}}),
			{"delay": 0, "iterationCount": 1, "fillMode": kony.anim.FILL_MODE_FORWARDS, "duration": 0.25},
 			{"animationEnd": closeMenu_Callback}
        );
  	} catch(e) {
      	logException(e, "closeSettingsMenu", true);
	} 
}

/** Settings Menu (Burger Menu)- About button action **/
function onClickSendDeviceDB(){
  	try {
      	printMessage("INSIDE onClickSendDeviceDB");
  		closeSettingsMenu();
      	var PackageName   						= gblPackageName;
        var DBName        						= kony.sync.getDBName();
        DBName 			  						= DBName.toString();
      	var AppName		  						= appConfig.appName;
        var EmailSubject  						= AppName + " Payload";
        var EmailContent  						= "A copy of database from " + AppName + " app has been attached to the mail, you can share with support team.";
        var EmailIDs 	  						= [];
        var DBRename	 						= "WorkTime_" + appConfig.appVersion;    
        EmailDB.emailSyncDB(PackageName, DBName, EmailSubject, EmailContent, EmailIDs, AppName, DBRename);
  	} catch(e) {
      	logException(e, "onClickSendDeviceDB", true);
	} 
}

/** Settings Menu (Burger Menu)- Sync button action **/
function onClickSync(){
  	try {
      	printMessage("INSIDE onClickSync");
  		closeSettingsMenu();
        var isActive							= isWorkTimeRunning();
        if (isActive === true) {
            showMessagePopup("common.message.cannotSync", true, dismissMessagePopup);
        } else {          
          	showLoading("common.message.loading");
            initDataSync("SYNC_DATA_UPLOAD", "UPLOAD_TRACE_DATA", true, false);
        }
 	} catch(e) {
      	logException(e, "onClickSync", true);
	} 
}

/** Settings Menu (Burger Menu)- Logout button action **/
function onClickLogout(){  	
  	try {
      	printMessage("INSIDE onClickLogout");
        closeSettingsMenu();
        var isActive							= isWorkTimeRunning();
        if (isActive === true) {
            showMessagePopup("common.message.cannotLogout", true, dismissMessagePopup);
        } else {
          	showLoading("common.message.loading");
          	getPendingUploadsFromAttendance(successGetPendingUploadsFromAttendance, errorGetPendingUploadsFromAttendance);
        }
   	} catch(e) {
      	logException(e, "onClickLogout", true);
	} 
}

/** The below function will handle touch start on settings screen **/
function handleTouchStartOnSettings(widgetRef, x, y){
  	try {
      	printMessage("INSIDE handleTouchStartOnSettings");
  		gblTouchMovePrevX						= x;
   	} catch(e) {
      	logException(e, "handleTouchStartOnSettings", true);
	} 
}

/** The below function will handle touch move on settings screen **/
function handleTouchMoveOnSettings(widgetRef, x, y){
  	try {
      	printMessage("INSIDE handleTouchMoveOnSettings");
        var frmObj								= kony.application.getCurrentForm();
        var curCenterXPercentageVal				= frmObj.flxSettingsContainer.centerX;
        var curCenterXIntVal					= parseInt(curCenterXPercentageVal.replace("%", ""));  	
        var deviceWidthVal						= gblDeviceInfo.deviceWidth;
        var xDisplacement						= (gblTouchMovePrevX > x) ? (gblTouchMovePrevX - x) : (x - gblTouchMovePrevX);
        var xDisplacementInPercent				= (xDisplacement * 100) / deviceWidthVal;
        var newCenterXInPercent					= (gblTouchMovePrevX > x) ? (curCenterXIntVal - xDisplacementInPercent) : (curCenterXIntVal + xDisplacementInPercent);
        if (newCenterXInPercent < 50 && newCenterXInPercent > -50) {
            frmObj.flxSettingsContainer.centerX	= newCenterXInPercent.toFixed(2) + "%";
            frmObj.flxSettingsContainer.forceLayout();
        }

        gblTouchMovePrevX						= x;
 	} catch(e) {
      	logException(e, "handleTouchMoveOnSettings", true);
	} 
}

/** The below function will handle touch end on settings screen **/
function handleTouchEndOnSettings(widgetRef, x, y){
  	try {
      	printMessage("INSIDE handleTouchEndOnSettings");
        var frmObj								= kony.application.getCurrentForm();
        var deviceWidthVal						= gblDeviceInfo.deviceWidth;  
        var flxMoveMinX							= (deviceWidthVal * 30) / 100;
        if (x === gblTouchMovePrevX) {
            closeSettingsMenu();
        } else if (x < flxMoveMinX) {
            closeSettingsMenu();
        } else {
            onClickOpenMenuButton();
        }
  	} catch(e) {
      	logException(e, "handleTouchEndOnSettings", true);
	} 
}

/** Last sync date displya method **/
function displayLastSyncDate(){
  	try {
      	printMessage("INSIDE displayLastSyncDate");
        var frmObj								= kony.application.getCurrentForm();
        ////var lastSyncDate						= kony.store.getItem(WTConstant.LAST_SYNC_DATE); 
      	var lastSyncDate						= gblSelData.LAST_SYNC_DATE; 
        if (!isEmpty(lastSyncDate)) {
            frmObj.lblLastSync.text				= getI18nString("common.label.lastSyncDate");
            frmObj.lblLastSyncDate.text			= (!isEmpty(lastSyncDate)) ? lastSyncDate : ""; 
        } else {
            frmObj.lblLastSync.text				= "";
            frmObj.lblLastSyncDate.text			= ""; 
        }
  	} catch(e) {
      	logException(e, "displayLastSyncDate", true);
	} 
}