/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer all the offline database and sync methods
***********************************************************************/

// kony.db.changeVersion("1.0","1.1",transactioCall,errorCall,sucessCall);

function executeCustomSQL(statement, params, successCallback, errorCallback){
  	try {
      	printMessage("INSIDE executeCustomSQL - statement - ", statement);
		var dbname 									= kony.sync.getDBName();  		
		kony.sync.single_select_execute(dbname, statement, params, successCallback, errorCallback);
    } catch (e) {
      	logException(e, "executeCustomSQL", true);
    }
}

function getLoggedUserFromDB(){
  	try {
      	printMessage("INSIDE getLoggedUserFromDB");
      	var wcs										= "";
      	var sqlQuery								= "SELECT employeeCode, countryCode, businessCode, languageCode, email, employeeName, icabsKey, branchNumber, branchName, lastSyncDate, isLoggedIn, isWorkTimeActive, activeNTAttendanceRef, activeOTAttendanceRef, activeNSEAttendanceRef FROM WTUser ";
      	executeCustomSQL(sqlQuery, null, successGetLoggedUser, errorGetLoggedUser);
    } catch(e) {
      	logException(e, "getLoggedUserFromDB", true);
	}
}

/** Get details from iCabInstances table - Transaction **/
function getICabsInstancesFromDB(){
  	try {
      	printMessage("INSIDE getICabsInstancesFromDB");
      	var wcs										= "ORDER BY countryCode, businessCode ASC";
      	com.wt.WTiCabInstances.find(wcs, successGetICabsInstances, errorGetICabsInstances);
    } catch(e) {
      	logException(e, "getICabsInstancesFromDB", true);
	}
}

/** Get details from employee details table - Transaction **/
function getEmployeeDetailsFromDB(){
  	try {
      	printMessage("INSIDE getEmployeeDetailsFromDB");
      	var wcs										= "";
      	com.wt.WTEmployee.find(wcs, successGetEmployeeDetails, errorGetEmployeeDetails);
    } catch(e) {
      	logException(e, "getEmployeeDetailsFromDB", true);
	}
}

function getBranchesFromDB() {
  	try {
      	printMessage("INSIDE getBranchesFromDB");
      	var wcs										= "WHERE countryCode = '" + gblSelData.COUNTRY_CODE + "' AND businessCode = '" + gblSelData.BUSINESS_CODE + "' ORDER BY branchName ASC";
      	com.wt.WTBranches.find(wcs, successGetBranches, errorGetBranches);
    } catch(e) {
      	logException(e, "getBranchesFromDB", true);
	}
}

function getNonServiceEventTypesFromDB() {
  	try {
      	printMessage("INSIDE getNonServiceEventTypesFromDB");
      	var wcs										= "WHERE isSendToPDA = 'true' AND isNonServiceEvent = 'true' ORDER BY workOrderTypeDescription ASC";
      	com.wt.WTWorkOrderType.find(wcs, successGetNonServiceEventTypes, errorGetNonServiceEventTypes);
    } catch(e) {
      	logException(e, "getNonServiceEventTypesFromDB", true);
	}
}

/** All the details related to logged user **/
function saveLoggedUserInDB(loggedUserObj){
  	try {
      	printMessage("INSIDE saveLoggedUserInDB - loggedUserObj - ", loggedUserObj);
      	var valuesTable								= {};
      	valuesTable.employeeCode					= loggedUserObj.EMPLOYEE_CODE;	
      	valuesTable.countryCode						= loggedUserObj.COUNTRY_CODE;
      	valuesTable.businessCode					= loggedUserObj.BUSINESS_CODE;
      	valuesTable.email							= loggedUserObj.EMAIL;
      	valuesTable.isLoggedIn						= loggedUserObj.IS_LOGGED_IN;
      	valuesTable.isWorkTimeActive				= loggedUserObj.IS_WORK_TIME_ACTIVE;
      	valuesTable.lastSyncDate					= loggedUserObj.LAST_SYNC_DATE;
      	valuesTable.branchName						= loggedUserObj.BRANCH_NAME;
      	valuesTable.branchNumber					= loggedUserObj.BRANCH_NUMBER;
      	valuesTable.employeeName					= loggedUserObj.EMPLOYEE_NAME;
      	valuesTable.languageCode					= loggedUserObj.LANGUAGE_CODE;
      	valuesTable.icabsKey						= loggedUserObj.SEL_ICABS_KEY;
      	valuesTable.activeNTAttendanceRef			= "";
      	valuesTable.activeOTAttendanceRef			= "";
      	valuesTable.activeNSEAttendanceRef			= "";

      	var today 									= new Date();
        var dateStr 								= today.toLocaleDateString();
        var timeStr 								= today.toLocaleTimeString();
        var curDateStr								= dateStr + " " + timeStr;
      	valuesTable.loginDateTime					= curDateStr;
      
      	valuesTable.buildMode 						= gblBuildMode;
		valuesTable.apkVersion 						= appConfig.appVersion;
		valuesTable.deviceId 						= kony.sdk.getDeviceId();
		valuesTable.packageName 					= gblPackageName;
      	
      	printMessage("INSIDE saveLoggedUserInDB - valuesTable - ", valuesTable);
      	com.wt.WTUser.create(valuesTable, successSaveLoggedUser, errorSaveLoggedUser, false);
    } catch(e) {
      	logException(e, "saveLoggedUserInDB", true);
	}
}

/** Record from temp attendance table - This is to check active work attendance  **/
function getActiveWorkAttendanceFromDB(attendanceRef, successCallback, errorCallback){
  	try {
      	printMessage("INSIDE getActiveWorkAttendanceFromDB - attendanceRef - ", attendanceRef);
      	var sqlQuery								= "SELECT * FROM attendancetemp WHERE attendanceRef = '" + attendanceRef + "'";
      	executeCustomSQL(sqlQuery, null, successCallback, errorCallback);
    } catch(e) {
      	logException(e, "getActiveWorkAttendanceFromDB", true);
	}
}

/** All the details related to logged user **/
function updateLoggedUserInDB(loggedUserObj, successCallback, errorCallback){
  	try {
      	printMessage("INSIDE updateLoggedUserInDB - loggedUserObj - ", loggedUserObj);
      	var valuesTable								= {};
      	valuesTable.employeeCode					= loggedUserObj.EMPLOYEE_CODE;
      	valuesTable.countryCode						= loggedUserObj.COUNTRY_CODE;	
      	valuesTable.businessCode					= loggedUserObj.BUSINESS_CODE;
        	
      	if (loggedUserObj.IS_LOGGED_IN === true || loggedUserObj.IS_LOGGED_IN === 1) {
          	valuesTable.isLoggedIn					= true;
        } else {
          	valuesTable.isLoggedIn					= false;
        }
      	
      	if (!isEmpty(loggedUserObj.LAST_SYNC_DATE)) {
          	valuesTable.lastSyncDate				= loggedUserObj.LAST_SYNC_DATE;
        }
      
      	if (loggedUserObj.IS_WORK_TIME_ACTIVE === true || loggedUserObj.IS_WORK_TIME_ACTIVE === 1) {
          	valuesTable.isWorkTimeActive			= true;
        } else {
          	valuesTable.isWorkTimeActive			= false;
        }
      	
      	if (!isEmpty(loggedUserObj.ACTIVE_NT_ATTENDANCE_REF)) {
      		valuesTable.activeNTAttendanceRef		= loggedUserObj.ACTIVE_NT_ATTENDANCE_REF;
        } else {
          	valuesTable.activeNTAttendanceRef		= "";
        }
      
      	if (!isEmpty(loggedUserObj.ACTIVE_OT_ATTENDANCE_REF)) {
      		valuesTable.activeOTAttendanceRef		= loggedUserObj.ACTIVE_OT_ATTENDANCE_REF;
        } else {
          	valuesTable.activeOTAttendanceRef		= "";
        }
      
      	if (!isEmpty(loggedUserObj.ACTIVE_NSE_ATTENDANCE_REF)) {
      		valuesTable.activeNSEAttendanceRef		= loggedUserObj.ACTIVE_NSE_ATTENDANCE_REF;
        } else {
          	valuesTable.activeNSEAttendanceRef		= "";
        }
      	
      	var wcs										= "WHERE employeeCode = '" + loggedUserObj.EMPLOYEE_CODE + "' AND countryCode = '" + loggedUserObj.COUNTRY_CODE + "' AND businessCode = '" + loggedUserObj.BUSINESS_CODE + "'";
      	printMessage("INSIDE updateLoggedUserInDB - valuesTable - ", valuesTable);
      	printMessage("INSIDE updateLoggedUserInDB - wcs - ", wcs);
      	com.wt.WTUser.update(wcs, valuesTable, successCallback, errorCallback, false);
    } catch(e) {
      	logException(e, "updateLoggedUserInDB", true);
	}
}

/** Saving active work attendance in temp attendance table - This is a local table **/
function saveActiveWorkAttendanceInTempTbl() {
  	try {
      	printMessage("INSIDE saveActiveWorkAttendanceInTempTbl - ");
      	var recordObj								= null;
      	if (gblAttendanceMode == WTConstant.NORMAL_TIME) {
          	recordObj								= gblNTAttendanceObject;
        } else if (gblAttendanceMode == WTConstant.OVER_TIME) {
          	recordObj								= gblOTAttendanceObject;
        } else {
          	recordObj								= gblNSEAttendanceObject;
        }
      	printMessage("INSIDE saveActiveWorkAttendanceInTempTbl - recordObj - ", recordObj);
      	var attendanceDetails						= {};
        attendanceDetails.branchNumber         	 	= gblSelData.BRANCH_NUMBER;
        attendanceDetails.startDateStr				= recordObj.startDateStr;      	
        attendanceDetails.startTimestamp			= (!isEmpty(recordObj.startTimestamp)) ? recordObj.startTimestamp.toString() : 0;
        attendanceDetails.startLocLatitude			= (!isEmpty(recordObj.startLocLatitude)) ? recordObj.startLocLatitude : "";
        attendanceDetails.startLocLongitude			= (!isEmpty(recordObj.startLocLongitude)) ? recordObj.startLocLongitude : "";	
      	attendanceDetails.startLocComment			= recordObj.startLocComment;
      	attendanceDetails.startTimestampStr    	 	= recordObj.startTimestampStr;
      	attendanceDetails.attendanceType			= recordObj.attendanceType;
      	attendanceDetails.attendanceRef				= recordObj.attendanceRef;
      	attendanceDetails.nonServiceEventTypeCode	= recordObj.nonServiceEventTypeCode;
      	printMessage("INSIDE saveActiveWorkAttendanceInTempTbl - attendanceDetails - ", attendanceDetails);
      	com.wt.attendancetemp.create(attendanceDetails, successSaveActiveWorkAttendance, errorSaveActiveWorkAttendance, false);
    } catch(e) {
      	logException(e, "saveActiveWorkAttendanceInTempTbl", true);
	}
}

/** Saving completed work attendance in original work attendance table - This will sync back to MBaaS database using create operation **/
function createWorkAttendanceInDB(allRecords, successCallback, errorCallback) {
  	try {
      	printMessage("INSIDE createWorkAttendanceInDB - allRecords - ", allRecords);
      	var allRecordsLen							= allRecords.length;
      	var recordObj								= null;
      	var attendanceDetails						= null;
      	var waRecords								= [];
      
      	for (var i = 0; i < allRecordsLen; i++) {
          	recordObj								= allRecords[i];
          	attendanceDetails						= {};
            attendanceDetails.employeeCode			= gblSelData.EMPLOYEE_CODE;	
            attendanceDetails.email					= gblSelData.EMAIL;
            attendanceDetails.branchNumber         	= gblSelData.BRANCH_NUMBER;
            attendanceDetails.countryCode			= gblSelData.COUNTRY_CODE;	
            attendanceDetails.businessCode			= gblSelData.BUSINESS_CODE;
            attendanceDetails.startDateStr			= recordObj.startDateStr;      	
            attendanceDetails.startTimestamp		= (!isEmpty(recordObj.startTimestamp)) ? recordObj.startTimestamp.toString() : 0;
            attendanceDetails.startLocLatitude		= (!isEmpty(recordObj.startLocLatitude)) ? recordObj.startLocLatitude : "";
            attendanceDetails.startLocLongitude		= (!isEmpty(recordObj.startLocLongitude)) ? recordObj.startLocLongitude : "";	
            attendanceDetails.startLocComment		= recordObj.startLocComment;
            attendanceDetails.startTimestampStr     = recordObj.startTimestampStr;
            attendanceDetails.attendanceType		= recordObj.attendanceType;
            attendanceDetails.endDateStr			= recordObj.endDateStr;
            attendanceDetails.endTimestamp			= (!isEmpty(recordObj.endTimestamp)) ? recordObj.endTimestamp.toString() : 0;
            attendanceDetails.endLocLatitude		= (!isEmpty(recordObj.endLocLatitude)) ? recordObj.endLocLatitude : "";
            attendanceDetails.endLocLongitude		= (!isEmpty(recordObj.endLocLongitude)) ? recordObj.endLocLongitude : "";
            attendanceDetails.endLocComment			= recordObj.endLocComment;
            attendanceDetails.endTimestampStr		= recordObj.endTimestampStr;
            attendanceDetails.attendanceRef			= recordObj.attendanceRef;
          	attendanceDetails.nonServiceEventTypeCode	= recordObj.nonServiceEventTypeCode;
          	waRecords.push(attendanceDetails);
            printMessage("INSIDE createWorkAttendanceInDB - attendanceDetails - ", attendanceDetails);
        }
		
      	if (waRecords.length > 0) {
          	com.wt.WorkTimeAttendance.createAll(waRecords, successCallback, errorCallback, true);
        } else {
          	/** Unexpected condition **/
          	printMessage("INSIDE createWorkAttendanceInDB - UNEXPECTED CONDITION - waRecords - ", waRecords);
        }
    } catch(e) {
      	logException(e, "createWorkAttendanceInDB", true);
	}
}

function deleteActiveWorkAttendaceFromTempTbl(){
  	try {
      	printMessage("INSIDE deleteActiveWorkAttendaceFromTempTbl - gblSelData - ", gblSelData);
      	var wcs										= "";
      	if (gblAttendanceMode == WTConstant.NORMAL_TIME) {
          	wcs										= "WHERE attendanceRef = '" + gblSelData.ACTIVE_OT_ATTENDANCE_REF + "'";
        } else if (gblAttendanceMode == WTConstant.OVER_TIME) {
          	wcs										= "WHERE attendanceRef = '" + gblSelData.ACTIVE_NT_ATTENDANCE_REF + "'";
        }
      	com.wt.attendancetemp.removeDeviceInstance(wcs, successDeleteActiveWorkAttendace, errorDeleteActiveWorkAttendace);
   	} catch(e) {
      	logException(e, "deleteActiveWorkAttendaceFromTempTbl", true);
	}
}

function deleteAllActiveWorkAttendaceFromTempTbl(){
  	try {
      	printMessage("INSIDE deleteAllActiveWorkAttendaceFromTempTbl - gblSelData - ", gblSelData);
      	var tempArray								= [];
      	if (!isEmpty(gblSelData.ACTIVE_NT_ATTENDANCE_REF)) {
          	tempArray.push(gblSelData.ACTIVE_NT_ATTENDANCE_REF);
        }
      	if (!isEmpty(gblSelData.ACTIVE_OT_ATTENDANCE_REF)) {
          	tempArray.push(gblSelData.ACTIVE_OT_ATTENDANCE_REF);
        }
      	if (!isEmpty(gblSelData.ACTIVE_NSE_ATTENDANCE_REF)) {
          	tempArray.push(gblSelData.ACTIVE_NSE_ATTENDANCE_REF);
        }
      	
      	if (tempArray.length > 0) {
          	var allRefs								= tempArray.join("','");
          	var wcs									= "WHERE attendanceRef IN ('" + allRefs + "')";
      		com.wt.attendancetemp.removeDeviceInstance(wcs, successDeleteActiveWorkAttendaceAlt, errorDeleteActiveWorkAttendaceAlt);
        } else {
          	/** Unexpected condition **/
          	printMessage("INSIDE deleteAllActiveWorkAttendaceFromTempTbl - UNEXPECTED CONDITION - tempArray - ", tempArray);
        }
   	} catch(e) {
      	logException(e, "deleteAllActiveWorkAttendaceFromTempTbl", true);
	}
}

function deleteActiveNTWorkAttendaceFromTempTbl(){
  	try {
      	printMessage("INSIDE deleteActiveNTWorkAttendaceFromTempTbl - gblSelData - ", gblSelData);
      	var wcs										= "WHERE attendanceRef = '" + gblSelData.ACTIVE_NT_ATTENDANCE_REF + "'";
      	com.wt.attendancetemp.removeDeviceInstance(wcs, successDeleteActiveWorkAttendaceAlt, errorDeleteActiveWorkAttendaceAlt);
   	} catch(e) {
      	logException(e, "deleteActiveNTWorkAttendaceFromTempTbl", true);
	}
}

function deleteActiveOTWorkAttendaceFromTempTbl(){
  	try {
      	printMessage("INSIDE deleteActiveOTWorkAttendaceFromTempTbl - gblSelData - ", gblSelData);
      	var wcs										= "WHERE attendanceRef = '" + gblSelData.ACTIVE_OT_ATTENDANCE_REF + "'";
      	com.wt.attendancetemp.removeDeviceInstance(wcs, successDeleteActiveWorkAttendaceAlt, errorDeleteActiveWorkAttendaceAlt);
   	} catch(e) {
      	logException(e, "deleteActiveOTWorkAttendaceFromTempTbl", true);
	}
}

function deleteActiveNSEWorkAttendaceFromTempTbl(){
  	try {
      	printMessage("INSIDE deleteActiveNSEWorkAttendaceFromTempTbl - gblSelData - ", gblSelData);
      	var wcs										= "WHERE attendanceRef = '" + gblSelData.ACTIVE_NSE_ATTENDANCE_REF + "'";
      	com.wt.attendancetemp.removeDeviceInstance(wcs, successDeleteActiveWorkAttendaceAlt, errorDeleteActiveWorkAttendaceAlt);
   	} catch(e) {
      	logException(e, "deleteActiveNSEWorkAttendaceFromTempTbl", true);
	}
}

/** Method - Create trace record in temp table **/
function createTraceRecordInTempTbl(recordObj, isTraceOpen){
  	try {
      	printMessage("INSIDE createTraceRecordInTempTbl - recordObj - ", recordObj);
      	printMessage("INSIDE createTraceRecordInTempTbl - isTraceOpen - ", isTraceOpen);
        var valuesTable							= {};
        var traceTimestamp						= (isTraceOpen === true) ? recordObj.startTimestamp : recordObj.endTimestamp;
        var traceLocLatitude 					= (isTraceOpen === true) ? recordObj.startLocLatitude : recordObj.endLocLatitude;
        var traceLocLongitude					= (isTraceOpen === true) ? recordObj.startLocLongitude : recordObj.endLocLongitude;
        var traceComment						= (isTraceOpen === true) ? recordObj.startLocComment : recordObj.endLocComment;
        valuesTable.traceTimestamp 				= (!isEmpty(traceTimestamp)) ? traceTimestamp.toString() : 0;
        valuesTable.traceLocLatitude 			= (!isEmpty(traceLocLatitude)) ? traceLocLatitude : "";
        valuesTable.traceLocLongitude 			= (!isEmpty(traceLocLongitude)) ? traceLocLongitude : "";
        valuesTable.traceIsOpen 				= (isTraceOpen == "true" || isTraceOpen === true || isTraceOpen === 1) ? 1 : 0;
        valuesTable.traceComment 				= traceComment;
        valuesTable.branchNumber 				= gblSelData.BRANCH_NUMBER;
      	valuesTable.attendanceRef 				= recordObj.attendanceRef;
      	printMessage("INSIDE createTraceRecordInTempTbl - valuesTable - ", valuesTable);
      
      	var customQueryRes						= customSQLQueryForAction("INSERT", valuesTable, "emptracetemp");
      	printMessage("INSIDE createTraceRecordInTempTbl - customQueryRes - ", customQueryRes);
      	printMessage("INSIDE createTraceRecordInTempTbl - recordObj.endTimestamp - ", recordObj.endTimestamp);
      	
      	if (recordObj.endTimestamp === 0 && (isTraceOpen === true || isTraceOpen === 1)) {
          	executeCustomSQL(customQueryRes.QUERY, customQueryRes.VALUES, successCreateTraceRecordOnStartDay, errorCreateTraceRecordOnStartDay);
        } else {
          	executeCustomSQL(customQueryRes.QUERY, customQueryRes.VALUES, successCreateTraceRecordOnEndWork, errorCreateTraceRecordOnEndWork);
        }
  	} catch(e) {
      	logException(e, "createTraceRecordInTempTbl", true);
	}
}

/** Method - Fetch today's work time records (The one which started today) **/
function getCurrentDayRecords(){
  	try {
      	printMessage("INSIDE getCurrentDayRecords");
        var todayStr							= getToday();
      	printMessage("INSIDE getCurrentDayRecords - todayStr - ", todayStr);
      	var sqlQuery							= "SELECT * FROM WorkTimeAttendance WHERE startDateStr = '" + todayStr + "' ORDER BY startTimestamp DESC";
      	executeCustomSQL(sqlQuery, null, successGetCurrentDayRecords, errorGetCurrentDayRecords);
   	} catch(e) {
      	logException(e, "getCurrentDayRecords", true);
	}
}

/** Method - Fetch todays work time records **/
function getTodaysWorkTimeRecords(){
  	try {
      	printMessage("INSIDE getTodaysWorkTimeRecords");
        var todayStr							= getToday();
      	var sqlQuery							= "SELECT * FROM WorkTimeAttendance WHERE startDateStr = '" + todayStr + "' OR endDateStr = '" + todayStr + "'";
      	executeCustomSQL(sqlQuery, null, successGetTodaysWorkTimeRecords, errorGetTodaysWorkTimeRecords);
   	} catch(e) {
      	logException(e, "getTodaysWorkTimeRecords", true);
	}
}

/** Get trace records from temp trace table - Transaction **/
function getEmployeeTracesFromTempTbl(){
  	try {
      	printMessage("INSIDE getEmployeeTracesFromTempTbl");
  		var wcs									= "";
      	com.wt.emptracetemp.find(wcs, successGetEmployeeTracesFromTempTbl, errorGetEmployeeTracesFromTempTbl);
   	} catch(e) {
      	logException(e, "getAllTempTracesTransaction", true);
	}
}

/** Method - Create attendance and trace records in database tables **/
function moveTempRecordsToTraceTbl(allTraceRecords){
  	try {
      	printMessage("INSIDE moveTempRecordsToTraceTbl - allTraceRecords - ", allTraceRecords);      	
        com.wt.WorkTimeTrace.createAll(allTraceRecords, successMoveTempRecordsToTraceTbl, errorMoveTempRecordsToTraceTbl, true);
    } catch(e) {
      	logException(e, "moveTempRecordsToTraceTbl", true);
	}
}

/** Delete all trace records from temp trace table - Transaction **/
function deleteAllTempTraceRecordsFromDB(){
  	try {
      	printMessage("INSIDE deleteAllTempTraceRecordsFromDB");
      	var sqlQuery							= "DELETE FROM emptracetemp";
      	executeCustomSQL(sqlQuery, null, successDeleteAllTempTraces, errorDeleteAllTempTraces);
   	} catch(e) {
      	logException(e, "deleteAllTempTraceRecordsFromDB", true);
	}
}

function getPendingUploadsFromAttendance(successCallback, errorCallback) {
  	try {
      	printMessage("INSIDE getPendingUploadsFromAttendance");
      	com.wt.WorkTimeAttendance.getPendingUpload(successCallback, errorCallback);
 	} catch (e) {
      	logException(e, "getPendingUploadsFromAttendance", true);
    } 
}

function getPendingUploadsFromTrace(successCallback, errorCallback) {
  	try {
      	printMessage("INSIDE getPendingUploadsFromTrace");
      	com.wt.WorkTimeTrace.getPendingUpload(successCallback, errorCallback);
 	} catch (e) {
      	logException(e, "getPendingUploadsFromTrace", true);
    } 
}

function deleteOldSyncedWorkAttendances() {
  	try {
      	printMessage("INSIDE deleteOldSyncedWorkAttendances");
      	var wcs									= "WHERE attendanceId > 0 AND strftime('%Y-%m-%d', startDateStr) < date('now','-1 day')";
      	printMessage("INSIDE deleteOldSyncedWorkAttendances - wcs - ", wcs);
      	com.wt.WorkTimeAttendance.removeDeviceInstance(wcs, success_commonCallback, error_commonCallback);
 	} catch (e) {
      	logException(e, "deleteOldSyncedWorkAttendances", true);
    } 
}

function deleteOldSupportViewEntries() {
  	try {
      	printMessage("INSIDE deleteOldSupportViewEntries");
      	var wcs									= "WHERE strftime('%Y-%m-%d', createDateTime) < date('now','-7 day')";
      	printMessage("INSIDE deleteOldSupportViewEntries - wcs - ", wcs);
      	com.wt.WTSupportData.removeDeviceInstance(wcs, success_commonCallback, error_commonCallback);
 	} catch (e) {
      	logException(e, "deleteOldSupportViewEntries", true);
    } 
}

function createSupportEntry(aRecord) {
	function successUpdateSupportData(result) {
		try {
			printMessage("INSIDE successUpdateSupportData - result - ", result);			
		} catch(e) {
			logException(e, "successUpdateSupportData", true);
		}
	}
	
	function errorUpdateSupportData(error) {
		try {
			printMessage("INSIDE errorUpdateSupportData - error - ", error);
		} catch(e) {
			logException(e, "errorUpdateSupportData", true);
		}	
	}
	
	function successCreateSupportData(result) {
		try {
			printMessage("INSIDE successCreateSupportData - result - ", result);			
		} catch(e) {
			logException(e, "successCreateSupportData", true);
		}
	}
	
	function errorCreateSupportData(error) {
		try {
			printMessage("INSIDE errorCreateSupportData - error - ", error);
		} catch(e) {
			logException(e, "errorCreateSupportData", true);
		}	
	}
	
	function successGetSupportEntry(result) {
		try {
			printMessage("INSIDE successGetSupportEntry - result - ", result);
			var valuesTable						= {};
			var dateTime						= getLocalDateTimeStr();
			if (!isEmpty(result) && result.length > 0) {
				var existingRecord				= result[0];
              	valuesTable.supportId			= existingRecord.supportId;
				valuesTable.requestData			= JSON.stringify(aRecord.requestData);
				valuesTable.responseData		= JSON.stringify(aRecord.responseData);
				valuesTable.comment				= aRecord.comment;
				valuesTable.occurrence			= parseInt(existingRecord.occurrence) + 1;
				valuesTable.updateDateTime		= dateTime;
				var updateWCS					= "WHERE supportId = '" + existingRecord.supportId + "'";
				com.wt.WTSupportData.update(updateWCS, valuesTable, successUpdateSupportData, errorUpdateSupportData, false);
			} else {				
				valuesTable.supportId			= generateGUID();
				valuesTable.createDateTime		= dateTime;
				valuesTable.methodName			= aRecord.methodName;
				valuesTable.requestData			= JSON.stringify(aRecord.requestData);
				valuesTable.responseData		= JSON.stringify(aRecord.responseData);
				valuesTable.comment				= aRecord.comment;
				valuesTable.occurrence			= 1;	
				valuesTable.updateDateTime		= dateTime;
				com.wt.WTSupportData.create(valuesTable, successCreateSupportData, errorCreateSupportData, false);
			}
		} catch(e) {
			logException(e, "successGetSupportEntry", true);
		}	
	}
	
	function errorGetSupportEntry(error) {
		try {
			printMessage("INSIDE errorGetSupportEntry - error - ", error);
		} catch(e) {
			logException(e, "errorGetSupportEntry", true);
		}	
	}
	
	try {
      	printMessage("INSIDE createSupportEntry - aRecord - ", aRecord);
      	var wcs									= "WHERE methodName = '" + aRecord.methodName + "'";
  		com.wt.WTSupportData.find(wcs, successGetSupportEntry, errorGetSupportEntry);
   	} catch(e) {
      	logException(e, "createSupportEntry", true);
	}
}

/********************************************* Custom DB operations *************************************************/

/** To open DB handler **/
/*function openLocalDatabase(){
  	try {
      	printMessage("INSIDE openLocalDatabase");
        var dbname 								= kony.sync.getDBName();
        var dbObjectId 							= kony.sync.getConnectionOnly(dbname, dbname);
        return dbObjectId;
   	} catch(e) {
      	logException(e, "openLocalDatabase", false);
	}
}*/