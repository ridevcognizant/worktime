/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer all utility methods
***********************************************************************/

/** To show loading indicator **/
function showLoading(messageKey){
	try {
      	printMessage("INSIDE showLoading - ");
      	var lodingText 					= getI18nString(messageKey);
		kony.application.showLoadingScreen("loadingSkin", lodingText, "center", true, true, null);
	} catch(e) {
      	logException(e, "showLoading");
	}	
}

/** To dismiss loading indicator **/
function dismissLoading(){	
  	try {
      	printMessage("INSIDE dismissLoading - ");
		kony.application.dismissLoadingScreen();
   	} catch(e) {
      	logException(e, "dismissLoading");
	}
}

/** To get i18n localized string for given key **/
function getI18nString(key){
  	try {
      	printMessage("INSIDE getI18nString - ");
		return key ? kony.i18n.getLocalizedString(key) : "";
  	} catch(e) {
      	logException(e, "getI18nString");
	}
}

/** To remove whitespaces  **/
function trim(inputString){
  	try {
      	printMessage("INSIDE trim - ");
		var outputString 				= "";
		if(inputString !== null){
			outputString 				= kony.string.trim(inputString);
		}
		return outputString;
  	} catch(e) {
      	logException(e, "trim");
	}
}

/** To check a string is valid or not  **/
function isEmpty(val){
  	try {
      	printMessage("INSIDE isEmpty - ");
		return (val === undefined || val === null || val === '' || val.length <= 0) ? true : false;
  	} catch(e) {
      	logException(e, "isEmpty");
	}
}

/** To make given string to uppercase  **/
function toUpperCase(inputString){
  	try {
      	printMessage("INSIDE toUpperCase - ");
        var outputVal 					= "";
        if(!isEmpty(inputString)){
            outputVal 					= inputString.toUpperCase();
        }
        return outputVal;
   	} catch(e) {
      	logException(e, "toUpperCase");
	}
}

/** To make given string to uppercase  **/
function toLowerCase(inputString){
  	try {
      	printMessage("INSIDE toLowerCase - ");
        var outputVal 					= "";
        if(!isEmpty(inputString)){
            outputVal 					= inputString.toLowerCase();
        }
        return outputVal;
   	} catch(e) {
      	logException(e, "toLowerCase");
	}
}

/** To add a leading zero on given string  **/
function appendLeadingZero(data) {
  	try {
      	printMessage("INSIDE appendLeadingZero - ");
		return (parseInt(data) < 10) ? ("0" + data.toString()) : data;
   	} catch(e) {
      	logException(e, "appendLeadingZero");
	}
}

/** To validate string length limit  **/
function isValidCharsCountInString(textStr, charLimit){
  	try {
      	printMessage("INSIDE isValidCharsCountInString - ");
        var boolResult					= true;
        if (!isEmpty(textStr)){
            if (textStr.length > charLimit){
                boolResult				= false;
            }
        }
        return boolResult;
   	} catch(e) {
      	logException(e, "isValidCharsCountInString");
	}
}

/** To get Current locale  **/
function getCurrentLocale(){
  	try {
      	printMessage("INSIDE getCurrentLocale - ");
        var currentLocale 				= "";
        try {
            currentLocale 				= kony.i18n.getCurrentLocale();		
        } catch(i18nError) {
            printMessage("Exception While getting currentLocale  : "+i18nError );
        }	
        return currentLocale;
  	} catch(e) {
      	logException(e, "getCurrentLocale");
	}
}

/** To check network availability  **/
function isNetworkAvailable(){	
  	try {
      	printMessage("INSIDE isNetworkAvailable - ");
		return kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY);
 	} catch(e) {
      	logException(e, "isNetworkAvailable");
	}
}

/** To exit application  **/
function exitApplication(){
  	try {
      	printMessage("INSIDE exitApplication - ");
		kony.application.exit();
   	} catch(e) {
      	logException(e, "exitApplication");
	}
}

/** To print a message on console  **/
function printMessage(message, dataObj){
  	try {
  		if (dataObj !== null && dataObj !== undefined) {
      		message						= message + " : " + JSON.stringify(dataObj);
    	}
		kony.print("*** WORK TIME *** : " + message);
    } catch(e) {
      	kony.print("*** WORK TIME *** : INSIDE printMessage - Exception - " + e.message);
	}	
}

/** To get a matching record from given array of elements  **/
function getMatchingItemFromArray(recordsArray, matchingKey, matchingValue){
  	try {
      	printMessage("INSIDE getMatchingItemFromArray - ");
        var matchingItem				= null;
        if (!isEmpty(recordsArray) && recordsArray.length > 0){
            var arrayLength				= recordsArray.length;
            var aRecord					= null;
            for (var i = 0; i < arrayLength; i++){
                aRecord					= recordsArray[i];

                if (aRecord[matchingKey] === matchingValue){
                    matchingItem		= aRecord;
                    break;
                }
            }
        }

        return matchingItem;
  	} catch(e) {
      	logException(e, "getMatchingItemFromArray");
	}	
}

/** To find index of a record from an array  **/
function getItemIndexFromArray(recordsArray, record){
  	try {
      	printMessage("INSIDE getItemIndexFromArray - ");
		var index 						= (!isEmpty(recordsArray) && !isEmpty(record)) ? recordsArray.indexOf(record) : -1;
		return index;
   	} catch(e) {
      	logException(e, "getMatchingItemFromArray");
	}
}

/** To remove a record from an array  **/
function removeItemFromArray(recordsArray, record){
  	try {
      	printMessage("INSIDE removeItemFromArray - ");
        var index						= getItemIndexFromArray(recordsArray, record);
        if(index !== -1) {
            recordsArray.splice(index, 1);
        }
  	} catch(e) {
      	logException(e, "removeItemFromArray");
	}
}

/** To search a record in given array  **/
function isRecordExist(record, recordsArray, matchingKey, matchingKey2){
  	try {
      	printMessage("INSIDE isRecordExist - ");
        var result						= false;
        if (!isEmpty(recordsArray) && recordsArray.length > 0){
            var arrayLength				= recordsArray.length;
            var aRecord					= null;
            for (var i = 0; i < arrayLength; i++){
                aRecord					= recordsArray[i];

                if (aRecord[matchingKey] === record[matchingKey] || (!isEmpty(matchingKey2) && aRecord[matchingKey2] === record[matchingKey2])){
                    result				= true;
                    break;
                }
            }
        }

        return result;
  	} catch(e) {
      	logException(e, "isRecordExist");
	}
}

/** To identify matched record from an array  **/
function getMatchedRecordFromArray(recordsArray, matchingKey, matchingValue){
  	try {
      	printMessage("INSIDE getMatchedRecordFromArray - ");
        var resultRecord				= null;
        if (!isEmpty(recordsArray) && recordsArray.length > 0){
            var arrayLength				= recordsArray.length;
            var aRecord					= null;
            for (var i = 0; i < arrayLength; i++){
                aRecord					= recordsArray[i];

                if (aRecord[matchingKey] === matchingValue){
                    resultRecord		= aRecord;
                    break;
                }
            }
        }

        return resultRecord;
  	} catch(e) {
      	logException(e, "getMatchedRecordFromArray");
	}
}

/** To clone an object  **/
function clone(obj) {
  	try {
      	printMessage("INSIDE clone - ");
      	if (isEmpty(obj)) 
            return obj;

        var copy 						= {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) 
              copy[attr] 				= obj[attr];
        }
        return copy;
    } catch(e) {
		logException(e, "clone");
	}
}

/** To find out the time difference between given time and current time **/
function timeDifferenceWithCurrentTime(pastTimeStamp){
  	try {
      	printMessage("INSIDE timeDifferenceWithCurrentTime - ");
  		var currentTimeStamp			= getCurrentTimestamp();
  		var timeDiff					= timeDifferenceBtwTimes(currentTimeStamp, parseInt(pastTimeStamp));
  		return timeDiff;
  	} catch(e) {
      	logException(e, "timeDifferenceWithCurrentTime");
	}
}

/** To find out the time difference between two given times **/
function timeDifferenceBtwTimes(newTimeStamp, oldTimeStamp){
  	try {
      	printMessage("INSIDE timeDifferenceBtwTimes - ");
  		var timeDiff					= parseInt(newTimeStamp) - parseInt(oldTimeStamp);  
  		return timeDiff;
   	} catch(e) {
      	logException(e, "timeDifferenceBtwTimes");
	}
}

/** To format time in hour minutes and seconds style **/
function getActualTime(timeStampValue){  
  	try {
      	printMessage("INSIDE getActualTime - ");
        var timeDiffHours 				= Math.floor(parseInt(timeStampValue) / 1000 / 60 / 60);

        var timeDiff1 					= parseInt(timeStampValue) - (timeDiffHours * 1000 * 60 * 60);
        var timeDiffMinutes 			= Math.floor(timeDiff1 / 1000 / 60);

        var timeDiff2 					= timeDiff1 - (timeDiffMinutes * 1000 * 60);
        var timeDiffSeconds 			= Math.floor(timeDiff2 / 1000);

        timeDiffHours					= (timeDiffHours < 10) ? ("0" + timeDiffHours) : timeDiffHours;
        timeDiffMinutes					= (timeDiffMinutes < 10) ? ("0" + timeDiffMinutes) : timeDiffMinutes;
        timeDiffSeconds					= (timeDiffSeconds < 10) ? ("0" + timeDiffSeconds) : timeDiffSeconds;

        var totalTimeDiffInFormat		= timeDiffHours + ":" + timeDiffMinutes + ":" + timeDiffSeconds;

        return totalTimeDiffInFormat;
 	} catch(e) {
      	logException(e, "getActualTime");
	}
}

/** To find out current timestamp **/
function getCurrentTimestamp(){
  	try {
      	printMessage("INSIDE getCurrentTimestamp - ");
  		var currentDate 				= new Date();
  		var currentTimestamp			= currentDate.getTime();
  		return currentTimestamp;
  	} catch(e) {
      	logException(e, "getCurrentTimestamp");
	}
}

/** To find out the time from current timestamp **/
function getTimeFromTimestamp(timeInMs){
  	try {
      	printMessage("INSIDE getTimeFromTimestamp - ");
        var givenDate 					= new Date(parseInt(timeInMs));
        var timeSplits					= {};
        var hrs							= givenDate.getHours();
        var mnts						= givenDate.getMinutes();
        var scnds						= givenDate.getSeconds();

        hrs								= appendLeadingZero(hrs);
        mnts							= appendLeadingZero(mnts);
        scnds							= appendLeadingZero(scnds);

        timeSplits.h					= hrs;
        timeSplits.m					= mnts;
        timeSplits.s					= scnds;
        return timeSplits;
 	} catch(e) {
      	logException(e, "getTimeFromTimestamp");
	}
}

/** To find out the date from current timestamp **/
function getDateFromTimestamp(timeInMs){
  	try {
      	printMessage("INSIDE getDateFromTimestamp - timeInMs - ", timeInMs);
        var givenDate 					= new Date(parseInt(timeInMs));
        var dateSplits					= {};

        var d							= givenDate.getDate();
        var m							= givenDate.getMonth() + 1;
        var y							= givenDate.getFullYear();

        d								= (d < 10) ? ("0"+d) : d;
        m								= (m < 10) ? ("0"+m) : m;

        dateSplits.yr					= y;
        dateSplits.mh					= m;
        dateSplits.dy					= d;
        return dateSplits;
  	} catch(e) {
      	logException(e, "getDateFromTimestamp");
	}
}

/** To find out current date as string **/
function getToday(){
  	try {
      	printMessage("INSIDE getToday - ");
        var today 						= new Date();
        var d							= today.getDate();
        var m							= today.getMonth() + 1;
        var y							= today.getFullYear();

        d								= (d < 10) ? ("0"+d) : d;
        m								= (m < 10) ? ("0"+m) : m;

        ////var todayStr					= d + "-" + m + "-" + y; 
      	var todayStr					= y + "-" + m + "-" + d; 
        return todayStr;
   	} catch(e) {
      	logException(e, "getToday");
	}
}

/** change from UTC format to HH:MM:SS GMT format*/
function getTimeInGMT(timeInMs){
	try {
      	printMessage("INSIDE getTimeInGMT - timeInMs - ", timeInMs);
   		var mDateObj 					= (!isEmpty(timeInMs)) ? new Date(parseInt(timeInMs)) : new Date();
     	var gmthrs						= appendLeadingZero(mDateObj.getHours());  	////appendLeadingZero(mDateObj.getUTCHours());   
  		var gmtmnts					 	= appendLeadingZero(mDateObj.getMinutes());	////appendLeadingZero(mDateObj.getUTCMinutes());
  		var gmtscnds					= appendLeadingZero(mDateObj.getSeconds());	////appendLeadingZero(mDateObj.getUTCSeconds());
	  	var timeStr  					= gmthrs + ":" + gmtmnts + ":" + gmtscnds;
  		return timeStr;
  	} catch(e) {
      	logException(e, "getTimeInGMT");
	}
}

/** change from UTC format to HH:MM:SS GMT format*/
function getDateTimeInGMT(timeInMs){
	try {
      	printMessage("INSIDE getDateTimeInGMT - timeInMs - ", timeInMs);
      	var dateSplits					= getDateFromTimestamp(timeInMs);
      	var dateStr						= dateSplits.yr + "-" + dateSplits.mh + "-" + dateSplits.dy;
        var timeStr						= getTimeInGMT(timeInMs);
  		var dateTimeStr					= dateStr + " " + timeStr;
      	return dateTimeStr;	
  	} catch(e) {
      	logException(e, "getDateTimeInGMT");
	}
}

function getLocalDateTimeStr() {
  	try {
      	printMessage("INSIDE getTimeInGMT - ");
      	var today 						= new Date();
      	var dateStr 					= today.toLocaleDateString();
      	var timeStr 					= today.toLocaleTimeString();
      	var localDateTimeStr			= dateStr + " " + timeStr;
      	return localDateTimeStr;
    } catch(e) {
      	logException(e, "getTimeInGMT");
	}
}

function generateGUID() {
	try {
		printMessage("INSIDE generateGUID");
		/** GUID format xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx **/
		var uuid 								= '';
		var i;
		for (i = 0; i < 32; i += 1) {
		  switch (i) {
		  case 8:
		  case 20:
			uuid += '-';
			uuid += (Math.random() * 16 | 0).toString(16);
			break;
		  case 12:
			uuid += '-';
			uuid += '4';
			break;
		  case 16:
			uuid += '-';
			uuid += (Math.random() * 4 | 8).toString(16);
			break;
		  default:
			uuid += (Math.random() * 16 | 0).toString(16);
		  }
		}
		printMessage("INSIDE generateGUID - uuid - ", uuid);
		return uuid;
	} catch(e) {
      	logException(e, "generateGUID");
	}
}