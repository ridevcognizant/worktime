/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer all constant declarations
***********************************************************************/

function WTConstant() {

}

WTConstant.SERVICE_HTTP_STATUS				= 200;
WTConstant.SERVICE_OPSTATUS					= 0;
WTConstant.SERVICE_MSG_SUCCESS				= "SUCCESS";
WTConstant.LOCATION_UNAVAILABLE				= "POSITION_UNAVAILABLE";
WTConstant.LOCATION_PERMSN_DENIED			= "PERMISSION_DENIED";
WTConstant.LOCATION_TIMEOUT					= "TIMEOUT";

WTConstant.DEF_COUNTRY_CODE					= "UK";
WTConstant.DEF_BUSINESS_CODE				= "D";
WTConstant.DEF_LANGUAGE_CODE				= "ENG";
WTConstant.DEF_LOCALE						= "en_GB";

WTConstant.FORM_HOME						= "Home";
WTConstant.FORM_LOGIN						= "Login";

WTConstant.RADIO_BTN_ON						= "radio_on.png";
WTConstant.RADIO_BTN_OFF					= "radio_off.png";

WTConstant.SYNC_UPLOAD_ERROR_POLICY			= "continueonerror";

WTConstant.EMAIL_SELECTION					= "EMAIL_SELECTION";
WTConstant.ICABS_SELECTION					= "ICABS_SELECTION";
WTConstant.BRANCH_SELECTION					= "BRANCH_SELECTION";

WTConstant.NORMAL_TIME						= "Normal Time";
WTConstant.OVER_TIME						= "Overtime";
WTConstant.NSE_TIME							= "NSE Time";

WTConstant.APP_MODE_IE						= "IE";
WTConstant.APP_MODE_UK						= "UK";