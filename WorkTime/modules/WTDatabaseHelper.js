/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Database Helper - Methods
***********************************************************************/

function successGetLoggedUser(result) {
  	try {
      	printMessage("INSIDE successGetLoggedUser - result - ", result);
      	if (!isEmpty(result) && result.length > 0) {
          	var loggedUserObj							= result[0];
          	if (loggedUserObj.isLoggedIn === true || loggedUserObj.isLoggedIn === 1) {              	
              	Login.lblUsername.text					= (!isEmpty(loggedUserObj.employeeName)) ? loggedUserObj.employeeName : "";
              
                gblSelData.LANGUAGE_CODE				= (!isEmpty(loggedUserObj.languageCode)) ? loggedUserObj.languageCode : "";
                gblSelData.COUNTRY_CODE					= (!isEmpty(loggedUserObj.countryCode)) ? loggedUserObj.countryCode : "";
                gblSelData.BUSINESS_CODE				= (!isEmpty(loggedUserObj.businessCode)) ? loggedUserObj.businessCode : "";
                gblSelData.EMAIL						= (!isEmpty(loggedUserObj.email)) ? loggedUserObj.email : "";
                gblSelData.EMPLOYEE_CODE				= (!isEmpty(loggedUserObj.employeeCode)) ? loggedUserObj.employeeCode : "";
                gblSelData.EMPLOYEE_NAME				= (!isEmpty(loggedUserObj.employeeName)) ? loggedUserObj.employeeName : "";
                gblSelData.SEL_ICABS_KEY				= (!isEmpty(loggedUserObj.icabsKey)) ? loggedUserObj.icabsKey : "";
                gblSelData.BRANCH_NUMBER				= (!isEmpty(loggedUserObj.branchNumber)) ? loggedUserObj.branchNumber : "";
                gblSelData.BRANCH_NAME					= (!isEmpty(loggedUserObj.branchName)) ? loggedUserObj.branchName : "";
                gblSelData.IS_LOGGED_IN					= true;
                gblSelData.LAST_SYNC_DATE				= (!isEmpty(loggedUserObj.lastSyncDate)) ? loggedUserObj.lastSyncDate : "";
              	gblSelData.ACTIVE_NT_ATTENDANCE_REF		= (!isEmpty(loggedUserObj.activeNTAttendanceRef)) ? loggedUserObj.activeNTAttendanceRef.toString() : "";
              	gblSelData.ACTIVE_OT_ATTENDANCE_REF		= (!isEmpty(loggedUserObj.activeOTAttendanceRef)) ? loggedUserObj.activeOTAttendanceRef.toString() : "";
              	gblSelData.ACTIVE_NSE_ATTENDANCE_REF	= (!isEmpty(loggedUserObj.activeNSEAttendanceRef)) ? loggedUserObj.activeNSEAttendanceRef.toString() : "";
              
              	gblNTAttendanceObject					= null;
      			gblOTAttendanceObject					= null;
      			gblNSEAttendanceObject					= null; 
              	
              	if (loggedUserObj.isWorkTimeActive === true || loggedUserObj.isWorkTimeActive === 1) {
              		gblSelData.IS_WORK_TIME_ACTIVE		= true;
                  	
                  	if (!isEmpty(gblSelData.ACTIVE_NT_ATTENDANCE_REF)) {
                      	getActiveWorkAttendanceFromDB(gblSelData.ACTIVE_NT_ATTENDANCE_REF, successGetActiveNTWorkAttendance, errorGetActiveNTWorkAttendance);
                    } else if (!isEmpty(gblSelData.ACTIVE_OT_ATTENDANCE_REF)) {
                        getActiveWorkAttendanceFromDB(gblSelData.ACTIVE_OT_ATTENDANCE_REF, successGetActiveOTWorkAttendance, errorGetActiveOTWorkAttendance);
                    } else if (!isEmpty(gblSelData.ACTIVE_NSE_ATTENDANCE_REF)) {
                        getActiveWorkAttendanceFromDB(gblSelData.ACTIVE_NSE_ATTENDANCE_REF, successGetActiveNSEWorkAttendance, errorGetActiveNSEWorkAttendance);
                    } else {
                      	getCurrentDayRecords();
                    }
                } else {
                  	gblSelData.IS_WORK_TIME_ACTIVE		= false;
                  	getCurrentDayRecords();
                }
            } else {
              	resetSync(getDeviceConfiguredEmails, onClickMessagePopupOkButton);
            }
        } else {
          	getDeviceConfiguredEmails();
        }
  	} catch(e) {
      	logException(e, "successGetLoggedUser", true);
	}
}

function errorGetLoggedUser(error) {
  	try {
      	printMessage("INSIDE errorGetLoggedUser - error - ", error);
      	resetSync(getDeviceConfiguredEmails, onClickMessagePopupOkButton);
  	} catch(e) {
      	logException(e, "errorGetLoggedUser", true);
	}
}

function successGetCurrentDayRecords(resultset) {
  	try {
      	printMessage("INSIDE successGetCurrentDayRecords - resultset - ", resultset);
      	var hasNormalTime								= false;
      	var hasOverTime									= false;
      	if (isEmpty(gblNTAttendanceObject) && isEmpty(gblOTAttendanceObject)) {
          	/** No work attendance global object means - Work time is not active, so reset first **/
          	resetTimeOnButtons("BOTH");
        } else {
          	if (gblAppMode == WTConstant.APP_MODE_UK) {
              	if (!isEmpty(gblOTAttendanceObject)) {
                  	hasOverTime							= true;
          			resetTimeOnButtons("NORMAL");
                } else {
                  	hasNormalTime						= true;
          			resetTimeOnButtons("OVERTIME");
                }
            } else {
              	if (!isEmpty(gblOTAttendanceObject)) {
                  	hasOverTime							= true;
          			resetTimeOnButtons("NORMAL");
                } 
              
              	if (!isEmpty(gblNTAttendanceObject)) {
                  	hasNormalTime						= true;
          			resetTimeOnButtons("OVERTIME");
                }
            }
        }
                
      	if (!isEmpty(resultset) && resultset.length > 0){
            var rowItem 								= null;
          	var resultsetLen							= resultset.length;
          	var startTimestamp							= 0;
          	var endTimestamp							= 0;
          	var tempRecord								= null;
          
            for (var i = 0; i < resultsetLen; i++){
              	if (hasOverTime === true && hasNormalTime === true) {
                  	/** Both records identified so no more looping required **/
                  	break;
                }
              
              	rowItem 								= resultset[i];
              	printMessage("INSIDE successGetCurrentDayRecords - rowItem - ", rowItem);
              	if (hasOverTime === false && rowItem.attendanceType == WTConstant.OVER_TIME) {
                  	tempRecord							= {};
                  	tempRecord.startTimestamp			= (!isEmpty(rowItem.startTimestamp)) ? rowItem.startTimestamp.toString() : 0;
                    tempRecord.endTimestamp				= (!isEmpty(rowItem.endTimestamp)) ? rowItem.endTimestamp.toString() : 0;
                  	tempRecord.attendanceType			= rowItem.attendanceType;
                  	applyWTRunningStatusOnScreen(tempRecord);
                  	hasOverTime							= true;
                }
              
              	if (hasNormalTime === false && rowItem.attendanceType == WTConstant.NORMAL_TIME) {
                  	tempRecord							= {};
                  	tempRecord.startTimestamp			= (!isEmpty(rowItem.startTimestamp)) ? rowItem.startTimestamp.toString() : 0;
                    tempRecord.endTimestamp				= (!isEmpty(rowItem.endTimestamp)) ? rowItem.endTimestamp.toString() : 0;
                  	tempRecord.attendanceType			= rowItem.attendanceType;
                  	applyWTRunningStatusOnScreen(tempRecord);
                  	hasNormalTime						= true;
                }
            }
        }
      	////showHomeScreen();
      	getNonServiceEventTypesFromDB();
	} catch(e) {
      	logException(e, "successGetCurrentDayRecords", true);
	}
}  

function errorGetCurrentDayRecords(error) {
  	try {
      	printMessage("INSIDE errorGetCurrentDayRecords - error - ", error);
      	if (isEmpty(gblNTAttendanceObject) && isEmpty(gblOTAttendanceObject)) {
          	/** No work attendance global object means - Work time is not active, so reset first **/
          	resetTimeOnButtons("BOTH");
        } else {
          	if (gblAppMode == WTConstant.APP_MODE_UK) {
              	if (!isEmpty(gblOTAttendanceObject)) {
          			resetTimeOnButtons("NORMAL");
                } else {
          			resetTimeOnButtons("OVERTIME");
                }
            } else {
              	if (!isEmpty(gblOTAttendanceObject)) {
          			resetTimeOnButtons("NORMAL");
                } 
              
              	if (!isEmpty(gblNTAttendanceObject)) {
          			resetTimeOnButtons("OVERTIME");
                }
            }
        }
      	
      	////showHomeScreen();
      	getNonServiceEventTypesFromDB();
   	} catch(e) {
      	logException(e, "errorGetCurrentDayRecords", true);
	}
}

/** Success callback - SQL query execution - get iCabInstances records **/
function successGetICabsInstances(result){
  	try {
      	printMessage("INSIDE successGetICabsInstances - result - ", result);      	
      	if (!isEmpty(result) && result.length > 0) {	
          	var icabsCount								= result.length;
          	var aRecord									= null;
          	if (icabsCount > 1) {
              	var masterDataArray						= [];
                var iCabsKey							= "";
                var iCabsName							= "";
                var tempArray							= [];
                for (var i = 0; i < icabsCount; i++){
                    aRecord								= result[i];
                    iCabsKey							= aRecord.countryCode + "_" + aRecord.businessCode;
                    iCabsName							= aRecord.countryCode + " " + aRecord.businessCode;
                    masterDataArray.push([iCabsKey, iCabsName]);
                    tempArray.push(iCabsKey);
                }
              	gblPopupMode							= WTConstant.ICABS_SELECTION;	
              	showDataSelectionPopup(masterDataArray, tempArray, gblSelData.SEL_ICABS_KEY);
              	dismissLoading();
            } else {
              	aRecord									= result[0];
              	gblSelData.COUNTRY_CODE					= aRecord.countryCode;
      			gblSelData.BUSINESS_CODE				= aRecord.businessCode;
      			gblSelData.SEL_ICABS_KEY				= aRecord.countryCode + "_" + aRecord.businessCode;
      	      	initDataSync("SYNC_INITIAL_DOWNLOAD", "DOWNLOAD_EMPLOYEE", true, true);
            } 
        } else {
          	showMessagePopup("common.message.noSourceFound", true, onClickMessagePopupOkButton);
          	dismissLoading();
        }      
    } catch(e) {
      	logException(e, "successGetICabsInstances", true);
	}
}

/** Error callback - SQL query execution - get iCabInstances records **/
function errorGetICabsInstances(error) {
  	try {
      	printMessage("INSIDE errorGetICabsInstances - error - ", error);
      	showMessagePopup("common.message.noSourceFound", true, onClickMessagePopupOkButton);
      	dismissLoading();
    } catch(e) {
      	logException(e, "errorGetICabsInstances", true);
	}
}

/** Success callback - SQL query execution - get employee records **/
function successGetEmployeeDetails(result){
  	try {
      	printMessage("INSIDE successGetEmployeeDetails - result - ", result);
      	if (!isEmpty(result) && result.length > 0) {	
          	var aRecord									= result[0];
            gblSelData.EMPLOYEE_CODE					= aRecord.employeeCode;
            gblSelData.EMPLOYEE_NAME					= aRecord.employeeName1 + " " + aRecord.employeeNameLast;
          	gblSelData.BRANCH_NUMBER					= aRecord.employeeBranchNumber;
            Login.lblUsername.text						= gblSelData.EMPLOYEE_NAME;
          	if (gblSkipBranchSelection === true) {
      			gblSelData.BRANCH_NAME					= "";
              	gblSelData.IS_LOGGED_IN					= true;
              	saveLoggedUserInDB(gblSelData);
            } else {
              	initDataSync("SYNC_INITIAL_DOWNLOAD", "DOWNLOAD_BRANCHES", true, true);
            }
        } else {
          	showMessagePopup("common.message.invalidUser", true, onClickMessagePopupOkButton);
          	dismissLoading();
        }
    } catch(e) {
      	logException(e, "successGetEmployeeDetails", true);
	}
}

/** Error callback - SQL query execution - get employee records **/
function errorGetEmployeeDetails(error) {
  	try {
      	printMessage("INSIDE errorGetEmployeeDetails - error - ", error);
      	showMessagePopup("common.message.invalidUser", true, onClickMessagePopupOkButton);
      	dismissLoading();
    } catch(e) {
      	logException(e, "errorGetEmployeeDetails", true);
	}
}

function successGetBranches(result){
  	try {
      	printMessage("INSIDE successGetBranches - result - ", result);      	
      	if (!isEmpty(result) && result.length > 0) {	
          	var branchesCount							= result.length;
          	var aRecord									= null;
          	if (branchesCount > 1) {
              	var masterDataArray						= [];
                var tempArray							= [];
              	var branchDisplayVal					= "";
                for (var i = 0; i < branchesCount; i++){
                    aRecord								= result[i];
                  	branchDisplayVal					= aRecord.branchNumber + " - " + aRecord.branchName;
                    masterDataArray.push([aRecord.branchNumber, branchDisplayVal]);
                    tempArray.push(aRecord.branchNumber);
                }
              	gblPopupMode							= WTConstant.BRANCH_SELECTION;	
              	showDataSelectionPopup(masterDataArray, tempArray, gblSelData.BRANCH_NUMBER);
              	dismissLoading();
            } else {
              	aRecord									= result[0];
              	gblSelData.BRANCH_NUMBER				= aRecord.branchNumber;
      			gblSelData.BRANCH_NAME					= aRecord.branchName;
              	gblSelData.IS_LOGGED_IN					= true;
              	saveLoggedUserInDB(gblSelData);
            } 
        } else {
          	showMessagePopup("common.message.noBranchesFound", true, onClickMessagePopupOkButton);
          	dismissLoading();
        }      
    } catch(e) {
      	logException(e, "successGetBranches", true);
	}
}

function errorGetBranches(error) {
  	try {
      	printMessage("INSIDE errorGetBranches - error - ", error);
      	showMessagePopup("common.message.noBranchesFound", true, onClickMessagePopupOkButton);
      	dismissLoading();
    } catch(e) {
      	logException(e, "errorGetBranches", true);
	}
}
  
function successGetNonServiceEventTypes(result){
  	try {
      	printMessage("INSIDE successGetNonServiceEventTypes - result - ", result);   
      	var masterDataArray								= [];
      	if (!isEmpty(result) && result.length > 0) {	
          	var visitTypesLen							= result.length;
          	var aRecord									= null;
          	for (var i = 0; i < visitTypesLen; i++) {
              	aRecord									= result[i];
              	masterDataArray.push([aRecord.workOrderTypeCode, aRecord.workOrderTypeDescription]);
            }
        }
      
      	Home.lbxNonServiceEvent.masterData				= masterDataArray;
      	Home.lbxNonServiceEvent.placeholder				= getI18nString("common.label.selectNonServiceEventType");
      	showHomeScreen();
 	} catch(e) {
      	logException(e, "successGetNonServiceEventTypes", true);
	}
}
function errorGetNonServiceEventTypes(error) {
  	try {
      	printMessage("INSIDE errorGetNonServiceEventTypes - error - ", error);
      	var masterDataArray								= [];
      	Home.lbxNonServiceEvent.masterData				= masterDataArray;
      	Home.lbxNonServiceEvent.placeholder				= getI18nString("common.label.selectNonServiceEventType");
      	showHomeScreen();
    } catch(e) {
      	logException(e, "errorGetNonServiceEventTypes", true);
	}
}
 
function successSaveLoggedUser(result) {
  	try {
      	printMessage("INSIDE successSaveLoggedUser - result - ", result);
      	////showHomeScreen();
      	initDataSync("SYNC_INITIAL_DOWNLOAD", "DOWNLOAD_CONFIG_DATA", false, false);
  	} catch(e) {
      	logException(e, "successSaveLoggedUser", true);
	}
}

function errorSaveLoggedUser(error) {
  	try {
      	printMessage("INSIDE errorSaveLoggedUser - error - ", error);
      	resetSync(successOnDoneLogout, errorOnDoneLogout);
  	} catch(e) {
      	logException(e, "errorSaveLoggedUser", true);
	}
}

function successGetActiveNTWorkAttendance(result) {
  	try {
      	printMessage("INSIDE successGetActiveNTWorkAttendance - result - ", result);
      	if (!isEmpty(result) && result.length > 0) {
          	var aRecord									= result[0];          	
            var attendanceObject						= new Attendance();
            attendanceObject.attendanceId				= (!isEmpty(aRecord.attendanceId)) ? aRecord.attendanceId.toString() : "";
            attendanceObject.startDateStr				= (!isEmpty(aRecord.startDateStr)) ? aRecord.startDateStr : "";
            attendanceObject.startTimestamp				= (!isEmpty(aRecord.startTimestamp)) ? aRecord.startTimestamp.toString() : 0;          	
            attendanceObject.startLocLatitude			= (!isEmpty(aRecord.startLocLatitude)) ? aRecord.startLocLatitude : "";
            attendanceObject.startLocLongitude			= (!isEmpty(aRecord.startLocLongitude)) ? aRecord.startLocLongitude : "";
            attendanceObject.attendanceType				= (!isEmpty(aRecord.attendanceType)) ? aRecord.attendanceType : WTConstant.NORMAL_TIME;
          	attendanceObject.startLocComment			= (!isEmpty(aRecord.startLocComment)) ? aRecord.startLocComment : "";          	
            attendanceObject.startTimestampStr			= (!isEmpty(aRecord.startTimestampStr)) ? aRecord.startTimestampStr : "";
            attendanceObject.branchNumber				= (!isEmpty(aRecord.branchNumber)) ? aRecord.branchNumber : "";
            attendanceObject.attendanceRef				= (!isEmpty(aRecord.attendanceRef)) ? aRecord.attendanceRef : "";
          	attendanceObject.nonServiceEventTypeCode	= (!isEmpty(aRecord.nonServiceEventTypeCode)) ? aRecord.nonServiceEventTypeCode : "";
          	gblNTAttendanceObject						= attendanceObject;
        }
      	
      	printMessage("INSIDE successGetActiveNTWorkAttendance - gblNTAttendanceObject - ", gblNTAttendanceObject);
      
      	if (gblAppMode == WTConstant.APP_MODE_UK) {
          	/** Never have multiple attendance (Except Non Service Event) in active state at a time. Means either Normal Time or Overtime is in active and not both at a time **/
            if (isEmpty(gblNTAttendanceObject) && !isEmpty(gblSelData.ACTIVE_OT_ATTENDANCE_REF)) {
              	getActiveWorkAttendanceFromDB(gblSelData.ACTIVE_OT_ATTENDANCE_REF, successGetActiveOTWorkAttendance, errorGetActiveOTWorkAttendance);
            } else if (isEmpty(gblNTAttendanceObject) && isEmpty(gblOTAttendanceObject) && !isEmpty(gblSelData.ACTIVE_NSE_ATTENDANCE_REF)) {
              	getActiveWorkAttendanceFromDB(gblSelData.ACTIVE_NSE_ATTENDANCE_REF, successGetActiveNSEWorkAttendance, errorGetActiveNSEWorkAttendance);
            } else {
              	getCurrentDayRecords();
            }
        } else {
          	/** App can keep both Normal and Overtime as active at a time **/
          	if (!isEmpty(gblSelData.ACTIVE_OT_ATTENDANCE_REF)) {
              	getActiveWorkAttendanceFromDB(gblSelData.ACTIVE_OT_ATTENDANCE_REF, successGetActiveOTWorkAttendance, errorGetActiveOTWorkAttendance);
            } else if (!isEmpty(gblSelData.ACTIVE_NSE_ATTENDANCE_REF)) {
              	getActiveWorkAttendanceFromDB(gblSelData.ACTIVE_NSE_ATTENDANCE_REF, successGetActiveNSEWorkAttendance, errorGetActiveNSEWorkAttendance);
            } else {
              	getCurrentDayRecords();
            }
        }
  	} catch(e) {
      	logException(e, "successGetActiveNTWorkAttendance", true);
	}
}

function errorGetActiveNTWorkAttendance(error) {
  	try {
      	printMessage("INSIDE errorGetActiveNTWorkAttendance - error - ", error);
      	if (gblAppMode == WTConstant.APP_MODE_UK) {
          	/** Never have multiple attendance (Except Non Service Event) in active state at a time. Means either Normal Time or Overtime is in active and not both at a time **/
            if (isEmpty(gblNTAttendanceObject) && !isEmpty(gblSelData.ACTIVE_OT_ATTENDANCE_REF)) {
              	getActiveWorkAttendanceFromDB(gblSelData.ACTIVE_OT_ATTENDANCE_REF, successGetActiveOTWorkAttendance, errorGetActiveOTWorkAttendance);
            } else if (isEmpty(gblNTAttendanceObject) && isEmpty(gblOTAttendanceObject) && !isEmpty(gblSelData.ACTIVE_NSE_ATTENDANCE_REF)) {
              	getActiveWorkAttendanceFromDB(gblSelData.ACTIVE_NSE_ATTENDANCE_REF, successGetActiveNSEWorkAttendance, errorGetActiveNSEWorkAttendance);
            } else {
              	getCurrentDayRecords();
            }
        } else {
          	/** App can keep both Normal and Overtime as active at a time **/
          	if (!isEmpty(gblSelData.ACTIVE_OT_ATTENDANCE_REF)) {
              	getActiveWorkAttendanceFromDB(gblSelData.ACTIVE_OT_ATTENDANCE_REF, successGetActiveOTWorkAttendance, errorGetActiveOTWorkAttendance);
            } else if (!isEmpty(gblSelData.ACTIVE_NSE_ATTENDANCE_REF)) {
              	getActiveWorkAttendanceFromDB(gblSelData.ACTIVE_NSE_ATTENDANCE_REF, successGetActiveNSEWorkAttendance, errorGetActiveNSEWorkAttendance);
            } else {
              	getCurrentDayRecords();
            }
        }
  	} catch(e) {
      	logException(e, "errorGetActiveNTWorkAttendance", true);
	}
}

function successGetActiveOTWorkAttendance(result) {
  	try {
      	printMessage("INSIDE successGetActiveOTWorkAttendance - result - ", result);
      	if (!isEmpty(result) && result.length > 0) {
          	var aRecord									= result[0];          	
            var attendanceObject						= new Attendance();
            attendanceObject.attendanceId				= (!isEmpty(aRecord.attendanceId)) ? aRecord.attendanceId.toString() : "";
            attendanceObject.startDateStr				= (!isEmpty(aRecord.startDateStr)) ? aRecord.startDateStr : "";
            attendanceObject.startTimestamp				= (!isEmpty(aRecord.startTimestamp)) ? aRecord.startTimestamp.toString() : 0;          	
            attendanceObject.startLocLatitude			= (!isEmpty(aRecord.startLocLatitude)) ? aRecord.startLocLatitude : "";
            attendanceObject.startLocLongitude			= (!isEmpty(aRecord.startLocLongitude)) ? aRecord.startLocLongitude : "";
            attendanceObject.attendanceType				= (!isEmpty(aRecord.attendanceType)) ? aRecord.attendanceType : WTConstant.NORMAL_TIME;
          	attendanceObject.startLocComment			= (!isEmpty(aRecord.startLocComment)) ? aRecord.startLocComment : "";          	
            attendanceObject.startTimestampStr			= (!isEmpty(aRecord.startTimestampStr)) ? aRecord.startTimestampStr : "";
            attendanceObject.branchNumber				= (!isEmpty(aRecord.branchNumber)) ? aRecord.branchNumber : "";
            attendanceObject.attendanceRef				= (!isEmpty(aRecord.attendanceRef)) ? aRecord.attendanceRef : "";
          	attendanceObject.nonServiceEventTypeCode	= (!isEmpty(aRecord.nonServiceEventTypeCode)) ? aRecord.nonServiceEventTypeCode : "";
          	gblOTAttendanceObject						= attendanceObject;
        }
      	
      	printMessage("INSIDE successGetActiveOTWorkAttendance - gblOTAttendanceObject - ", gblOTAttendanceObject);
      	if (gblAppMode == WTConstant.APP_MODE_UK) {
      		if (isEmpty(gblNTAttendanceObject) && isEmpty(gblOTAttendanceObject) && !isEmpty(gblSelData.ACTIVE_NSE_ATTENDANCE_REF)) {
              	getActiveWorkAttendanceFromDB(gblSelData.ACTIVE_NSE_ATTENDANCE_REF, successGetActiveNSEWorkAttendance, errorGetActiveNSEWorkAttendance);
        	} else {
          		getCurrentDayRecords();
        	}
        } else {
          	/** App can keep both Normal and Overtime as active at a time **/
          	if (!isEmpty(gblSelData.ACTIVE_NSE_ATTENDANCE_REF)) {
              	getActiveWorkAttendanceFromDB(gblSelData.ACTIVE_NSE_ATTENDANCE_REF, successGetActiveNSEWorkAttendance, errorGetActiveNSEWorkAttendance);
            } else {
              	getCurrentDayRecords();
            }
        }
  	} catch(e) {
      	logException(e, "successGetActiveOTWorkAttendance", true);
	}
}

function errorGetActiveOTWorkAttendance(error) {
  	try {
      	printMessage("INSIDE errorGetActiveOTWorkAttendance - error - ", error);
      	if (gblAppMode == WTConstant.APP_MODE_UK) {
      		if (isEmpty(gblNTAttendanceObject) && isEmpty(gblOTAttendanceObject) && !isEmpty(gblSelData.ACTIVE_NSE_ATTENDANCE_REF)) {
              	getActiveWorkAttendanceFromDB(gblSelData.ACTIVE_NSE_ATTENDANCE_REF, successGetActiveNSEWorkAttendance, errorGetActiveNSEWorkAttendance);
        	} else {
          		getCurrentDayRecords();
        	}
        } else {
          	/** App can keep both Normal and Overtime as active at a time **/
          	if (!isEmpty(gblSelData.ACTIVE_NSE_ATTENDANCE_REF)) {
              	getActiveWorkAttendanceFromDB(gblSelData.ACTIVE_NSE_ATTENDANCE_REF, successGetActiveNSEWorkAttendance, errorGetActiveNSEWorkAttendance);
            } else {
              	getCurrentDayRecords();
            }
        }
  	} catch(e) {
      	logException(e, "errorGetActiveOTWorkAttendance", true);
	}
}

function successGetActiveNSEWorkAttendance(result) {
  	try {
      	printMessage("INSIDE successGetActiveNSEWorkAttendance - result - ", result);
      	if (!isEmpty(result) && result.length > 0) {
          	var aRecord									= result[0];          	
            var attendanceObject						= new Attendance();
            attendanceObject.attendanceId				= (!isEmpty(aRecord.attendanceId)) ? aRecord.attendanceId.toString() : "";
            attendanceObject.startDateStr				= (!isEmpty(aRecord.startDateStr)) ? aRecord.startDateStr : "";
            attendanceObject.startTimestamp				= (!isEmpty(aRecord.startTimestamp)) ? aRecord.startTimestamp.toString() : 0;          	
            attendanceObject.startLocLatitude			= (!isEmpty(aRecord.startLocLatitude)) ? aRecord.startLocLatitude : "";
            attendanceObject.startLocLongitude			= (!isEmpty(aRecord.startLocLongitude)) ? aRecord.startLocLongitude : "";
            attendanceObject.attendanceType				= (!isEmpty(aRecord.attendanceType)) ? aRecord.attendanceType : WTConstant.NORMAL_TIME;
          	attendanceObject.startLocComment			= (!isEmpty(aRecord.startLocComment)) ? aRecord.startLocComment : "";          	
            attendanceObject.startTimestampStr			= (!isEmpty(aRecord.startTimestampStr)) ? aRecord.startTimestampStr : "";
            attendanceObject.branchNumber				= (!isEmpty(aRecord.branchNumber)) ? aRecord.branchNumber : "";
            attendanceObject.attendanceRef				= (!isEmpty(aRecord.attendanceRef)) ? aRecord.attendanceRef : "";
          	attendanceObject.nonServiceEventTypeCode	= (!isEmpty(aRecord.nonServiceEventTypeCode)) ? aRecord.nonServiceEventTypeCode : "";
          	gblNSEAttendanceObject						= attendanceObject;
        }
      	
      	printMessage("INSIDE successGetActiveNSEWorkAttendance - gblNSEAttendanceObject - ", gblNSEAttendanceObject);
      	getCurrentDayRecords();
  	} catch(e) {
      	logException(e, "successGetActiveNSEWorkAttendance", true);
	}
}

function errorGetActiveNSEWorkAttendance(error) {
  	try {
      	printMessage("INSIDE errorGetActiveNSEWorkAttendance - error - ", error);
      	getCurrentDayRecords();
  	} catch(e) {
      	logException(e, "errorGetActiveNSEWorkAttendance", true);
	}
}

function successSaveActiveWorkAttendance(result) {
  	try {
      	printMessage("INSIDE successSaveActiveWorkAttendance - result - ", result);
      	var workAttendanceId							= result.attendanceId.toString();
      	if (gblAttendanceMode == WTConstant.NORMAL_TIME) {
      		gblNTAttendanceObject.attendanceId			= workAttendanceId;
      		gblSelData.ACTIVE_NT_ATTENDANCE_REF			= gblNTAttendanceObject.attendanceRef;  
          	if (gblNTAttendanceObject.endTimestamp > 0) {
              	/** This means Ending Normal Time **/
              	gblSelData.IS_WORK_TIME_ACTIVE			= false;   
            } else {
              	gblSelData.IS_WORK_TIME_ACTIVE			= true;   
            }
        } else if (gblAttendanceMode == WTConstant.OVER_TIME) {
          	gblOTAttendanceObject.attendanceId			= workAttendanceId;
      		gblSelData.ACTIVE_OT_ATTENDANCE_REF			= gblOTAttendanceObject.attendanceRef;
          	if (gblAppMode == WTConstant.APP_MODE_UK) {
              	if (gblOTAttendanceObject.endTimestamp > 0) {
                  	/** This means Ending Overtime **/
                  	gblSelData.IS_WORK_TIME_ACTIVE		= false;   
                } else {
                  	gblSelData.IS_WORK_TIME_ACTIVE		= true;   
                }
            } else {
              	/** IE Mode - Start or End Overtime will not make any changes in below flag. It will be true here **/
              	gblSelData.IS_WORK_TIME_ACTIVE			= true;
            }
        } else {
          	gblNSEAttendanceObject.attendanceId			= workAttendanceId;
      		gblSelData.ACTIVE_NSE_ATTENDANCE_REF		= gblNSEAttendanceObject.attendanceRef;
          	if (gblAppMode == WTConstant.APP_MODE_UK) {
              	if (gblNSEAttendanceObject.endTimestamp > 0) {
                  	/** This means Ending Nonservice event **/
                  	gblSelData.IS_WORK_TIME_ACTIVE		= false;   
                } else {
                  	gblSelData.IS_WORK_TIME_ACTIVE		= true;   
                }
            } else {
              	/** IE Mode - Start or End Nonservice event will not make any changes in below flag. It will be true here **/
              	gblSelData.IS_WORK_TIME_ACTIVE			= true;
            }
        }
      	
      	updateLoggedUserInDB(gblSelData, successUpdateLoggedUser, errorUpdateLoggedUser);      	      
	} catch(e) {
      	logException(e, "successSaveActiveWorkAttendance", true);
	}
} 
  
function errorSaveActiveWorkAttendance(error) {
  	try {
      	printMessage("INSIDE errorSaveActiveWorkAttendance - error - ", error);
      	var errorData									= (!isEmpty(error)) ? JSON.stringify(error) : "";
      	var errorLog									= {};
      	errorLog.message								= "Failed to save active work attendance: " + errorData;
      	logException(errorLog, "errorSaveActiveWorkAttendance", true);
      	showMessagePopup("common.message.saveOperationFailed", true, dismissMessagePopup);
      	dismissLoading();
	} catch(e) {
      	logException(e, "errorSaveActiveWorkAttendance", true);
	}
}

function successUpdateLoggedUser(result){
  	try {
      	printMessage("INSIDE successUpdateLoggedUser - result - ", result);
      	printMessage("INSIDE successUpdateLoggedUser - gblSelData - ", gblSelData);
      	printMessage("INSIDE successUpdateLoggedUser - gblAttendanceMode - ", gblAttendanceMode);
      	printMessage("INSIDE successUpdateLoggedUser - gblSelData.IS_WORK_TIME_ACTIVE - ", gblSelData.IS_WORK_TIME_ACTIVE);
      	
      	if (gblAttendanceMode == WTConstant.NORMAL_TIME) {
      		createTraceRecordInTempTbl(gblNTAttendanceObject, gblSelData.IS_WORK_TIME_ACTIVE); 
        } else if (gblAttendanceMode == WTConstant.OVER_TIME) {
      		createTraceRecordInTempTbl(gblOTAttendanceObject, gblSelData.IS_WORK_TIME_ACTIVE); 
        } else {
      		createTraceRecordInTempTbl(gblNSEAttendanceObject, gblSelData.IS_WORK_TIME_ACTIVE); 
        }
   	} catch(e) {
      	logException(e, "successUpdateLoggedUser", true);
	}
}

function errorUpdateLoggedUser(error){
  	try {
      	printMessage("INSIDE errorUpdateLoggedUser - error - ", error);
      	var errorData									= (!isEmpty(error)) ? JSON.stringify(error) : "";
      	var errorLog									= {};
      	errorLog.message								= "Failed to update logged user data: " + errorData;
      	logException(errorLog, "errorUpdateLoggedUser", true);
      	showMessagePopup("common.message.saveOperationFailed", true, dismissMessagePopup);
      	dismissLoading();
   	} catch(e) {
      	logException(e, "errorUpdateLoggedUser", true);
	}
}

function successCreateTraceRecordOnStartDay(result) {
  	try {
      	printMessage("INSIDE successCreateTraceRecordOnStartDay - result - ", result);
      	printMessage("INSIDE successCreateTraceRecordOnStartDay - gblAttendanceMode - ", gblAttendanceMode);
      
      	if (gblAppMode == WTConstant.APP_MODE_UK) {
          	if (!isEmpty(gblNTAttendanceObject) && gblAttendanceMode == WTConstant.OVER_TIME) {
              	/** This means the user jumped from Normal Time to Overtime without stopping Normal Time **/
				gblNTAttendanceObject					= null;
            } else if (!isEmpty(gblOTAttendanceObject) && gblAttendanceMode == WTConstant.NORMAL_TIME) {
              	/** This means the user jumped from Overtime to Normal Time without stopping Overtime **/
				gblOTAttendanceObject					= null;
            } else if (!isEmpty(gblNTAttendanceObject) && gblAttendanceMode == WTConstant.NSE_TIME) {
              	/** This means the user jumped from Normal Time to Non Service Event without stopping Normal Time **/
				gblNTAttendanceObject					= null;
            } else if (!isEmpty(gblOTAttendanceObject) && gblAttendanceMode == WTConstant.NSE_TIME) {
              	/** This means the user jumped from Overtime to Non Service Event without stopping Overtime **/
				gblOTAttendanceObject					= null;
            } else if (!isEmpty(gblNSEAttendanceObject) && gblAttendanceMode == WTConstant.NORMAL_TIME) {
              	/** This means the user jumped from Non Service Event to Normal Time without stopping on Service Event **/
				gblNSEAttendanceObject					= null;
            } else if (!isEmpty(gblNSEAttendanceObject) && gblAttendanceMode == WTConstant.OVER_TIME) {
              	/** This means the user jumped from Non Service Event to Overtime without stopping on Service Event **/
				gblNSEAttendanceObject					= null;
            }
        }
      
      	var dbname 										= "";
      	if (gblAppMode == WTConstant.APP_MODE_UK) {
          	dbname 										= kony.sync.getDBName();
          	Trace.startTraceCapture(dbname, "emptracetemp");
        } else {
            if (gblAttendanceMode == WTConstant.NORMAL_TIME) {
              	dbname 									= kony.sync.getDBName();
          		Trace.startTraceCapture(dbname, "emptracetemp");
            }
        }
      	
		dismissLoading();
	} catch(e) {
      	logException(e, "successCreateTraceRecordOnStartDay", true);
	}
} 
  
function errorCreateTraceRecordOnStartDay(error) {
  	try {
      	printMessage("INSIDE errorCreateTraceRecordOnStartDay - error - ", error);
      	var errorData									= (!isEmpty(error)) ? JSON.stringify(error) : "";
      	var errorLog									= {};
      	errorLog.message								= "Failed to save trace record: " + errorData;
      	logException(errorLog, "errorCreateTraceRecordOnStartDay", true);
      	showMessagePopup("common.message.saveOperationFailed", true, dismissMessagePopup);
      	dismissLoading();
	} catch(e) {
      	logException(e, "errorCreateTraceRecordOnStartDay", true);
	}
}

function successCreateWorkAttendance(result) {
  	try {
      	printMessage("INSIDE successCreateWorkAttendance - result - ", result);      	
      	/** The temp attendance table is used to save attendance record when user starts an activity like Normal Time or Overtime or Non Service Event **/
      	/** In UK Mode - Normal Time, Overtime and Non Service Event will not be exist together at a time **/
      	/** In IE Mode - Both Normal Time and Overtime or Normal Time and Non Service Event can exist together at a time **/
      	if (gblAppMode == WTConstant.APP_MODE_UK) {          	
          	deleteActiveWorkAttendaceFromTempTbl();
        } else {
          	saveActiveWorkAttendanceInTempTbl();
        }
	} catch(e) {
      	logException(e, "successCreateWorkAttendance", true);
	}
} 
  
function errorCreateWorkAttendance(error) {
  	try {
      	printMessage("INSIDE errorCreateWorkAttendance - error - ", error);
      	var errorData									= (!isEmpty(error)) ? JSON.stringify(error) : "";
      	var errorLog									= {};
      	errorLog.message								= "Failed to insert work attendance: " + errorData;
      	logException(errorLog, "errorCreateWorkAttendance", true);
      	showMessagePopup("common.message.saveOperationFailed", true, dismissMessagePopup);
      	dismissLoading();
	} catch(e) {
      	logException(e, "errorCreateWorkAttendance", true);
	}
}

function successCreateWorkAttendanceAlt(result) {
  	try {
      	printMessage("INSIDE successCreateWorkAttendanceAlt - result - ", result);  
      	if (gblAppMode == WTConstant.APP_MODE_UK) {
          	/** UK Mode - Here we can remove all active work attendance from temp table **/
          	deleteAllActiveWorkAttendaceFromTempTbl();
        } else {
          	if (gblAttendanceMode == WTConstant.NORMAL_TIME) {
              	deleteActiveNTWorkAttendaceFromTempTbl();
            } else if (gblAttendanceMode == WTConstant.OVER_TIME) {
              	deleteActiveOTWorkAttendaceFromTempTbl();
            } else {
              	deleteActiveNSEWorkAttendaceFromTempTbl();
            }
        }
	} catch(e) {
      	logException(e, "successCreateWorkAttendanceAlt", true);
	}
} 
  
function errorCreateWorkAttendanceAlt(error) {
  	try {
      	printMessage("INSIDE errorCreateWorkAttendanceAlt - error - ", error);
      	var errorData									= (!isEmpty(error)) ? JSON.stringify(error) : "";
      	var errorLog									= {};
      	errorLog.message								= "Failed to insert work attendance: " + errorData;
      	logException(errorLog, "errorCreateWorkAttendanceAlt", true);
      	showMessagePopup("common.message.saveOperationFailed", true, dismissMessagePopup);
      	dismissLoading();
	} catch(e) {
      	logException(e, "errorCreateWorkAttendanceAlt", true);
	}
}

function successDeleteActiveWorkAttendace(result) {
  	try {
      	printMessage("INSIDE successDeleteActiveWorkAttendace - result - ", result);
      	saveActiveWorkAttendanceInTempTbl();
	} catch(e) {
      	logException(e, "successDeleteActiveWorkAttendace", true);
	}
} 
  
function errorDeleteActiveWorkAttendace(error) {
  	try {
      	printMessage("INSIDE errorDeleteActiveWorkAttendace - error - ", error);
      	var errorData									= (!isEmpty(error)) ? JSON.stringify(error) : "";
      	var errorLog									= {};
      	errorLog.message								= "Failed to delete active work attendance: " + errorData;
      	logException(errorLog, "errorDeleteActiveWorkAttendace", true);
      	showMessagePopup("common.message.saveOperationFailed", true, dismissMessagePopup);
      	dismissLoading();
	} catch(e) {
      	logException(e, "errorDeleteActiveWorkAttendace", true);
	}
}

function successDeleteActiveWorkAttendaceAlt(result) {
  	try {
      	printMessage("INSIDE successDeleteActiveWorkAttendaceAlt - result - ", result);
      	printMessage("INSIDE successDeleteActiveWorkAttendaceAlt - gblAttendanceMode - ", gblAttendanceMode);
      	if (gblAppMode == WTConstant.APP_MODE_UK) {
          	gblSelData.IS_WORK_TIME_ACTIVE				= false;
          	gblSelData.ACTIVE_NT_ATTENDANCE_REF			= "";
      		gblSelData.ACTIVE_OT_ATTENDANCE_REF			= "";
      		gblSelData.ACTIVE_NSE_ATTENDANCE_REF		= "";
        } else {
          	if (gblAttendanceMode == WTConstant.OVER_TIME) {
              	gblSelData.ACTIVE_OT_ATTENDANCE_REF		= "";
            } else if (gblAttendanceMode == WTConstant.NSE_TIME) {
              	gblSelData.ACTIVE_NSE_ATTENDANCE_REF	= "";
            } else {
              	gblSelData.IS_WORK_TIME_ACTIVE			= false;
          		gblSelData.ACTIVE_NT_ATTENDANCE_REF		= "";
      			gblSelData.ACTIVE_OT_ATTENDANCE_REF		= "";
      			gblSelData.ACTIVE_NSE_ATTENDANCE_REF	= "";
            }
        }	
      	printMessage("INSIDE successDeleteActiveWorkAttendaceAlt - gblSelData - ", gblSelData);
      	updateLoggedUserInDB(gblSelData, successUpdateLoggedUser, errorUpdateLoggedUser);
	} catch(e) {
      	logException(e, "successDeleteActiveWorkAttendaceAlt", true);
	}
}

function errorDeleteActiveWorkAttendaceAlt(error) {
  	try {
      	printMessage("INSIDE errorDeleteActiveWorkAttendaceAlt - error - ", error);
      	var errorData								= (!isEmpty(error)) ? JSON.stringify(error) : "";
      	var errorLog								= {};
      	errorLog.message							= "Failed to delete active work attendance: " + errorData;
      	logException(errorLog, "errorDeleteActiveWorkAttendaceAlt", true);
      	showMessagePopup("common.message.saveOperationFailed", true, dismissMessagePopup);
      	dismissLoading();
	} catch(e) {
      	logException(e, "errorDeleteActiveWorkAttendaceAlt", true);
	}
}

function successCreateTraceRecordOnEndWork(result) {
  	try {
      	printMessage("INSIDE successCreateTraceRecordOnEndWork - result - ", result);
      	printMessage("INSIDE successCreateTraceRecordOnEndWork - gblAttendanceMode - ", gblAttendanceMode);
      	if (gblAppMode == WTConstant.APP_MODE_UK) {
          	/** UK Mode - Any activity stop will end traces **/
          	Trace.stopTraceCapture();
          	getTodaysWorkTimeRecords();
        } else {
          	if (gblAttendanceMode == WTConstant.NORMAL_TIME) {
              	/** IE Mode - Only Normal Time end will stop all traces. Else traces will continue **/
              	Trace.stopTraceCapture();
				getTodaysWorkTimeRecords();
            } else {
              	getEmployeeTracesFromTempTbl();
            }
        }
	} catch(e) {
      	logException(e, "successCreateTraceRecordOnEndWork", true);
	}
} 
  
function errorCreateTraceRecordOnEndWork(error) {
  	try {
      	printMessage("INSIDE errorCreateTraceRecordOnEndWork - error - ", error);
      	if (gblAppMode == WTConstant.APP_MODE_UK) {
          	/** UK Mode - Any activity stop will end traces **/
          	Trace.stopTraceCapture();
          	getTodaysWorkTimeRecords();
        } else {
          	if (gblAttendanceMode == WTConstant.NORMAL_TIME) {
              	/** IE Mode - Only Normal Time end will stop all traces. Else traces will continue **/
              	Trace.stopTraceCapture();
				getTodaysWorkTimeRecords();
            } else {
              	getEmployeeTracesFromTempTbl();
            }
        }
	} catch(e) {
      	logException(e, "errorCreateTraceRecordOnEndWork", true);
	}
}
      
/** Success callback - SQL query execution for fetch todays attendance records **/
function successGetTodaysWorkTimeRecords(resultset) {
  	try {
      	printMessage("INSIDE successGetTodaysWorkTimeRecords - resultset - ", resultset);
        var normalWorkTime							= 0;
        var overtimeWorkTime						= 0;
      	var nonServiceTime							= 0;
        var activeTime								= 0;
        /** Base time means - the day starting time mid night 12'0 clk [Since it showing todays statistics, no need to include previous day work time] **/
        var todayDate								= new Date();
        var todaysBaseDate							= new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate(), 0, 0, 0, 1);
      	printMessage("INSIDE successGetTodaysWorkTimeRecords - todaysBaseDate - ", todaysBaseDate);
        var todaysBaseTime							= todaysBaseDate.getTime();
      	printMessage("INSIDE successGetTodaysWorkTimeRecords - todaysBaseTime - ", todaysBaseTime);
        var startTimeVal							= 0;
        if (!isEmpty(resultset) && resultset.length > 0){
            var rowItem 							= null;
          	var resultsetLen						= resultset.length;
          	var startTimestamp						= 0;
          	var endTimestamp						= 0;
            for (var i = 0; i < resultsetLen; i++){
                rowItem 							= resultset[i];
              	printMessage("INSIDE successGetTodaysWorkTimeRecords - rowItem - ", rowItem);
              	startTimestamp 						= (!isEmpty(rowItem.startTimestamp)) ? rowItem.startTimestamp.toString() : 0;
              	printMessage("INSIDE successGetTodaysWorkTimeRecords - startTimestamp - ", startTimestamp);
              	endTimestamp						= (!isEmpty(rowItem.endTimestamp)) ? rowItem.endTimestamp.toString() : 0;
              	printMessage("INSIDE successGetTodaysWorkTimeRecords - endTimestamp - ", endTimestamp);
                startTimeVal						= (startTimestamp < todaysBaseTime) ? todaysBaseTime : startTimestamp;
              	printMessage("INSIDE successGetTodaysWorkTimeRecords - startTimeVal - ", startTimeVal);
                activeTime							= timeDifferenceBtwTimes(endTimestamp, startTimeVal); 
              	printMessage("INSIDE successGetTodaysWorkTimeRecords - activeTime - ", activeTime);
              	printMessage("INSIDE successGetTodaysWorkTimeRecords - rowItem.attendanceType - ", rowItem.attendanceType);
              	if (rowItem.attendanceType == WTConstant.NORMAL_TIME) {
                  	normalWorkTime					= normalWorkTime + activeTime; 
                } else if (rowItem.attendanceType == WTConstant.OVER_TIME) {
                  	overtimeWorkTime				= overtimeWorkTime + activeTime;
                } else {
                  	nonServiceTime					= nonServiceTime + activeTime;
                }	
            }
        } 
      
      	var totalWorkTime							= (normalWorkTime > 0) ? getActualTime(normalWorkTime) : "00:00:00";
        var totalOverTime							= (overtimeWorkTime > 0) ? getActualTime(overtimeWorkTime) : "00:00:00";
      	var totalNonServiceTime						= (nonServiceTime > 0) ? getActualTime(nonServiceTime) : "00:00:00";
        printMessage("INSIDE successGetTodaysWorkTimeRecords - totalWorkTime - ", totalWorkTime);
		printMessage("INSIDE successGetTodaysWorkTimeRecords - totalOverTime - ", totalOverTime);
      	printMessage("INSIDE successGetTodaysWorkTimeRecords - totalNonServiceTime - ", totalNonServiceTime);
      
        var currentTimeStamp						= getCurrentTimestamp();
        var theDay									= getDateFromTimestamp(currentTimeStamp);
        var monthI18nStr							= getMonthNamei18n(theDay.mh);
      	printMessage("INSIDE successGetTodaysWorkTimeRecords - theDay - ", theDay);
      	printMessage("INSIDE successGetTodaysWorkTimeRecords - monthI18nStr - ", monthI18nStr);
        Home.lblDate.text							= getI18nString("common.label.date") + " " + theDay.dy + " " + getI18nString(monthI18nStr) + " " + theDay.yr;
        Home.lblTotalWorkTime.text					= getI18nString("popup.label.totalWorkTime") + " " + totalWorkTime;
        Home.lblTotalOvertime.text					= getI18nString("popup.label.totalOvertime") + " " + totalOverTime;
      	Home.lblTotalNonServiceTime.text			= getI18nString("popup.label.totalNonServiceTime") + " " + totalNonServiceTime;
        setFlxOverlayMsgOkButtonStyle(false);
        Home.flxPopupOverlayContainer.setVisibility(true);  
      	getEmployeeTracesFromTempTbl();
  	} catch(e) {
      	logException(e, "successGetTodaysWorkTimeRecords", true);
	}
}

/** Error callback - SQL query execution for fetch todays attendance records **/
function errorGetTodaysWorkTimeRecords(error) {
  	try {
      	printMessage("INSIDE errorGetTodaysWorkTimeRecords - error - ", error);
        getEmployeeTracesFromTempTbl();
   	} catch(e) {
      	logException(e, "errorGetTodaysWorkTimeRecords", true);
	}
}

function successGetEmployeeTracesFromTempTbl(result) {
  	try {
      	printMessage("INSIDE successGetEmployeeTracesFromTempTbl - result - ", result);
      	if (!isEmpty(result) && result.length > 0) {
          	var resultLen							= result.length;	
          	var allTraces							= [];
          	var aRecord								= null;
          	var aTrace								= null;
          	printMessage("INSIDE successGetEmployeeTracesFromTempTbl - gblSelData - ", gblSelData);
          	for (var i = 0; i < resultLen; i++) {
              	aRecord								= result[i];
              	printMessage("INSIDE successGetEmployeeTracesFromTempTbl - aRecord - ", aRecord);
              	printMessage("INSIDE successGetEmployeeTracesFromTempTbl - aRecord.traceIsOpen - ", aRecord.traceIsOpen);
              	aTrace								= {};
                aTrace.email 						= (!isEmpty(aRecord.email)) ? aRecord.email : gblSelData.EMAIL;
                aTrace.traceTimestamp 				= (!isEmpty(aRecord.traceTimestamp)) ? aRecord.traceTimestamp.toString() : 0;
                aTrace.traceLocLatitude 			= aRecord.traceLocLatitude;
                aTrace.traceLocLongitude 			= aRecord.traceLocLongitude;
                aTrace.traceIsOpen 					= (aRecord.traceIsOpen == "true" || aRecord.traceIsOpen === true || aRecord.traceIsOpen === 1) ? 1 : 0;
                aTrace.traceComment 				= (!isEmpty(aRecord.traceComment)) ? aRecord.traceComment : "";
                aTrace.traceTimestampStr 			= (aTrace.traceTimestamp > 0) ? getDateTimeInGMT(aTrace.traceTimestamp) : "";
                aTrace.branchNumber 				= (!isEmpty(aRecord.branchNumber)) ? aRecord.branchNumber : gblSelData.BRANCH_NUMBER;
              	if (gblAttendanceMode == WTConstant.NORMAL_TIME) {
                  	aTrace.attendanceRef			= gblNTAttendanceObject.attendanceRef;
                } else if (gblAttendanceMode == WTConstant.OVER_TIME) {
                  	aTrace.attendanceRef			= gblOTAttendanceObject.attendanceRef;
                } else {
                  	aTrace.attendanceRef			= gblNSEAttendanceObject.attendanceRef;
                } 
              	
              	allTraces.push(aTrace);
            }          	
          	moveTempRecordsToTraceTbl(allTraces);
        } else {
          	moveToNextAfterTraceInsertion(true);
        }
    } catch(e) {
      	logException(e, "successGetEmployeeTracesFromTempTbl", true);
	}
}

function errorGetEmployeeTracesFromTempTbl(error) {
  	try {
      	printMessage("INSIDE errorGetEmployeeTracesFromTempTbl - error - ", error);
      	moveToNextAfterTraceInsertion(true);
    } catch(e) {
      	logException(e, "errorGetEmployeeTracesFromTempTbl", true);
	}
}

function successMoveTempRecordsToTraceTbl(result) {
  	try {
      	printMessage("INSIDE successMoveTempRecordsToTraceTbl - result - ", result);
		deleteAllTempTraceRecordsFromDB();
    } catch(e) {
      	logException(e, "successMoveTempRecordsToTraceTbl", true);
	}
}

function errorMoveTempRecordsToTraceTbl(error) {
  	try {
      	printMessage("INSIDE errorMoveTempRecordsToTraceTbl - error - ", error);
		moveToNextAfterTraceInsertion(true);
    } catch(e) {
      	logException(e, "errorMoveTempRecordsToTraceTbl", true);
	}
}

function moveToNextAfterTraceInsertion(isDeleteTempTracePending) {
  	try {
      	printMessage("INSIDE moveToNextAfterTraceInsertion - isDeleteTempTracePending - ", isDeleteTempTracePending);
		if (gblAppMode == WTConstant.APP_MODE_UK) {
          	gblAttendanceMode						= "";
          	gblNTAttendanceObject					= null;
          	gblOTAttendanceObject					= null;
          	gblNSEAttendanceObject					= null;
          	initDataSync("SYNC_DATA_UPLOAD", "UPLOAD_TRACE_DATA", true, false);
        } else {
          	if (gblAttendanceMode == WTConstant.NORMAL_TIME) {
              	gblAttendanceMode					= "";
          		gblNTAttendanceObject				= null;
          		gblOTAttendanceObject				= null;
          		gblNSEAttendanceObject				= null;
            	initDataSync("SYNC_DATA_UPLOAD", "UPLOAD_TRACE_DATA", true, false);
          	} else {
              	if (isDeleteTempTracePending === true) {
                  	deleteAllTempTraceRecordsFromDB();
                } else {
                  	if (gblAttendanceMode == WTConstant.OVER_TIME) {
                      	gblOTAttendanceObject		= null;
                    } else {
                      	gblNSEAttendanceObject		= null;
                    }
                  	/** IE Mode - After Overtime or Non Service Event, the app will continue Normal Time **/
                  	gblAttendanceMode				= WTConstant.NORMAL_TIME; 
                  	printMessage("INSIDE moveToNextAfterTraceInsertion - gblAttendanceMode - ", gblAttendanceMode);
                  	printMessage("INSIDE moveToNextAfterTraceInsertion - gblOTAttendanceObject - ", gblOTAttendanceObject);
                  	printMessage("INSIDE moveToNextAfterTraceInsertion - gblNSEAttendanceObject - ", gblNSEAttendanceObject);
                  	printMessage("INSIDE moveToNextAfterTraceInsertion - gblNTAttendanceObject - ", gblNTAttendanceObject);
                  	dismissLoading();
                }
          	}
        }
    } catch(e) {
      	logException(e, "moveToNextAfterTraceInsertion", true);
	}
}

function successDeleteAllTempTraces(result) {
  	try {
      	printMessage("INSIDE successDeleteAllTempTraces - result - ", result);
      	moveToNextAfterTraceInsertion(false);
    } catch(e) {
      	logException(e, "successDeleteAllTempTraces", true);
	}
}

function errorDeleteAllTempTraces(error) {
  	try {
      	printMessage("INSIDE errorDeleteAllTempTraces - error - ", error);
		moveToNextAfterTraceInsertion(false);
    } catch(e) {
      	logException(e, "errorDeleteAllTempTraces", true);
	}
}

function successGetPendingUploadsFromAttendance(result) {
  	try {
      	printMessage("INSIDE successGetPendingUploadsFromAttendance - result - ", result);
      	if (!isEmpty(result) && result.length > 0) {
          	showMessagePopup("common.message.unsyncedRecordsPresent", true, dismissMessagePopup);
          	dismissLoading();
        } else {
          	getPendingUploadsFromTrace(successGetPendingUploadsFromTrace, errorGetPendingUploadsFromTrace);
        }      	
    } catch(e) {
      	logException(e, "successGetPendingUploadsFromAttendance", true);
	}
}

function errorGetPendingUploadsFromAttendance(error) {
  	try {
      	printMessage("INSIDE errorGetPendingUploadsFromAttendance - error - ", error);
    } catch(e) {
      	logException(e, "errorGetPendingUploadsFromAttendance", true);
	}
}

function successGetPendingUploadsFromTrace(result) {
  	try {
      	printMessage("INSIDE successGetPendingUploadsFromTrace - result - ", result);
        if (!isEmpty(result) && result.length > 0) {
          	showMessagePopup("common.message.unsyncedRecordsPresent", true, dismissMessagePopup);
        } else {
          	showConfirmPopup("common.message.confirmLogout", onClickLogoutConfirmPopupYesBtn);
        }
        dismissLoading();
    } catch(e) {
      	logException(e, "successGetPendingUploadsFromTrace", true);
	}
}

function errorGetPendingUploadsFromTrace(error) {
  	try {
      	printMessage("INSIDE errorGetPendingUploadsFromTrace - error - ", error);
    } catch(e) {
      	logException(e, "errorGetPendingUploadsFromTrace", true);
	}
}

/** Common success callback - For sql statement execution **/
function success_commonCallback() {
	printMessage("INSIDE success_commonCallback");
}

/** Common error callback - For sql statement execution **/
function error_commonCallback(error) {
	printMessage("INSIDE error_commonCallback");
}

function customSQLQueryForAction(action, rowRecord, objectName, wcs) {
  	try {
   		printMessage("INSIDE customSQLQueryForAction - action - ", action);
      	printMessage("INSIDE customSQLQueryForAction - rowRecord - ", rowRecord);
      	printMessage("INSIDE customSQLQueryForAction - objectName - ", objectName);
      	printMessage("INSIDE customSQLQueryForAction - wcs - ", wcs);
      	var result										= {};    	
      	var sqlQuery									= "";
      	var allColumns 									= "";
      	var allValues									= "";
  		var columnNames									= [];
		var columnValues								= [];
      	var frameWorkCols								= ["konysyncchangetype", "konysyncversionnumber", "konysyncchangetype"];
      	var columnVal									= "";
      	var columnName									= "";
      	if (action == "INSERT") {
            for (columnName in rowRecord) {
                if (frameWorkCols.indexOf(columnName) === -1) {
                    printMessage("INSIDE customSQLQueryForAction - columnName - ", columnName);
                    columnNames.push(columnName);
                    columnVal							= rowRecord[columnName];
                    printMessage("INSIDE customSQLQueryForAction - columnVal - ", columnVal);
                    columnVal							= (isDataUndefinedOrNull(columnVal)) ? "NULL" : columnVal;
                    columnValues.push(columnVal);
                }
            }
	
            if (columnNames.length > 0) {
                allColumns 								= columnNames.join("','");
                allValues 								= columnValues.join("','");
              	sqlQuery								= "INSERT INTO " + objectName + " ('" + allColumns + "') VALUES ('" + allValues + "')";
              	result.QUERY							= sqlQuery;
              	result.VALUES							= null;
            }
       	} else if (action == "UPDATE") {
           	var updateColumn							= "";
           	for (columnName in rowRecord) {
                if (frameWorkCols.indexOf(columnName) === -1) {
                  	printMessage("INSIDE customSQLQueryForAction - columnName - ", columnName);
                  	updateColumn			= "'" + columnName + "' = ?";
                    columnNames.push(updateColumn);
                    columnVal				= rowRecord[columnName];
                    printMessage("INSIDE customSQLQueryForAction - columnVal - ", columnVal);
                    columnVal				= (isDataUndefinedOrNull(columnVal)) ? "NULL" : columnVal;
                    columnValues.push(columnVal);
                }
            }
          
          	if (columnNames.length > 0) {
                allColumns 					= columnNames.join(",");
              	sqlQuery					= "UPDATE " + objectName + " SET " + allColumns + " " + wcs;
              	result.QUERY				= sqlQuery;
              	result.VALUES				= columnValues;
            }
      	}
      	printMessage("INSIDE customSQLQueryForAction - columnNames - ", columnNames);
      	printMessage("INSIDE customSQLQueryForAction - columnValues - ", columnValues);
      	printMessage("INSIDE customSQLQueryForAction - sqlQuery - ", sqlQuery);
      	printMessage("INSIDE customSQLQueryForAction - result - ", result);
        
      	return result;
	} catch (e) {
      	logException(e, 'customSQLQueryForAction', true);
    }	
}