/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer the geo location based methods
***********************************************************************/

/** To capture current location **/
/** timeout = 5 seconds and cached location age = 10 minutes **/
function captureCurrentLocation(successCallbackHandler, errorCallbackHandler){
  	try {
      	printMessage("INSIDE captureCurrentLocation");
        var positionOptions 								= {timeout: 5000, maximumAge: 600000};   	
        var successCallbackMethod							= successCallbackHandler;
        var failureCallbackMethod							= errorCallbackHandler;
        kony.location.getCurrentPosition(successCallbackMethod, failureCallbackMethod, positionOptions);
  	} catch(e) {
      	logException(e, "captureCurrentLocation", true);
	} 
}

/** Success callback locaion capture **/
function successLocationCapture(position) {
  	try {
      	printMessage("INSIDE successLocationCapture");
      	var attendanceObjs									= [];
      	if (gblAttendanceMode == WTConstant.NORMAL_TIME) {
          	gblNTAttendanceObject.startLocLatitude			= position.coords.latitude;
        	gblNTAttendanceObject.startLocLongitude			= position.coords.longitude;
          	
          	/** UK Mode - When we start Normal Time, need to end other activity if it is in active state **/
          	if (gblAppMode == WTConstant.APP_MODE_UK) {
             	if (!isEmpty(gblOTAttendanceObject)) {
                  	/** Below object changes are used to stop Overtime **/
              		gblOTAttendanceObject.endLocLatitude	= position.coords.latitude;
            		gblOTAttendanceObject.endLocLongitude	= position.coords.longitude;
              		gblOTAttendanceObject.endDateStr		= gblNTAttendanceObject.startDateStr;
            		gblOTAttendanceObject.endTimestamp		= gblNTAttendanceObject.startTimestamp;
            		gblOTAttendanceObject.endTimestampStr	= getTimeInGMT(gblNTAttendanceObject.startTimestamp);
                  	attendanceObjs.push(gblOTAttendanceObject);
                } else if (!isEmpty(gblNSEAttendanceObject)) {
                  	/** Below object changes are used to stop Non Service Event **/
              		gblNSEAttendanceObject.endLocLatitude	= position.coords.latitude;
            		gblNSEAttendanceObject.endLocLongitude	= position.coords.longitude;
              		gblNSEAttendanceObject.endDateStr		= gblNTAttendanceObject.startDateStr;
            		gblNSEAttendanceObject.endTimestamp		= gblNTAttendanceObject.startTimestamp;
            		gblNSEAttendanceObject.endTimestampStr	= getTimeInGMT(gblNTAttendanceObject.startTimestamp);
                  	attendanceObjs.push(gblNSEAttendanceObject);
                }
            }
        } else if (gblAttendanceMode == WTConstant.OVER_TIME) {
          	gblOTAttendanceObject.startLocLatitude			= position.coords.latitude;
        	gblOTAttendanceObject.startLocLongitude			= position.coords.longitude;
          
          	/** UK Mode - When we start Over Time, need to end other activity if it is in active state **/
          	if (gblAppMode == WTConstant.APP_MODE_UK) {
             	if (!isEmpty(gblNTAttendanceObject)) {
                  	/** Below object changes are used to stop Normal Time **/
              		gblNTAttendanceObject.endLocLatitude	= position.coords.latitude;
            		gblNTAttendanceObject.endLocLongitude	= position.coords.longitude;
              		gblNTAttendanceObject.endDateStr		= gblOTAttendanceObject.startDateStr;
            		gblNTAttendanceObject.endTimestamp		= gblOTAttendanceObject.startTimestamp;
            		gblNTAttendanceObject.endTimestampStr	= getTimeInGMT(gblOTAttendanceObject.startTimestamp);
                  	attendanceObjs.push(gblNTAttendanceObject);
                } else if (!isEmpty(gblNSEAttendanceObject)) {
                  	/** Below object changes are used to stop Non Service Event **/
              		gblNSEAttendanceObject.endLocLatitude	= position.coords.latitude;
            		gblNSEAttendanceObject.endLocLongitude	= position.coords.longitude;
              		gblNSEAttendanceObject.endDateStr		= gblOTAttendanceObject.startDateStr;
            		gblNSEAttendanceObject.endTimestamp		= gblOTAttendanceObject.startTimestamp;
            		gblNSEAttendanceObject.endTimestampStr	= getTimeInGMT(gblOTAttendanceObject.startTimestamp);
                  	attendanceObjs.push(gblNSEAttendanceObject);
                }
            }
        } else {
          	gblNSEAttendanceObject.startLocLatitude			= position.coords.latitude;
        	gblNSEAttendanceObject.startLocLongitude		= position.coords.longitude;
          
          	/** UK Mode - When we start Non Service Event, need to end other activity if it is in active state **/
          	if (gblAppMode == WTConstant.APP_MODE_UK) {
             	if (!isEmpty(gblNTAttendanceObject)) {
                  	/** Below object changes are used to stop Normal Time **/
              		gblNTAttendanceObject.endLocLatitude	= position.coords.latitude;
            		gblNTAttendanceObject.endLocLongitude	= position.coords.longitude;
              		gblNTAttendanceObject.endDateStr		= gblNSEAttendanceObject.startDateStr;
            		gblNTAttendanceObject.endTimestamp		= gblNSEAttendanceObject.startTimestamp;
            		gblNTAttendanceObject.endTimestampStr	= getTimeInGMT(gblNSEAttendanceObject.startTimestamp);
                  	attendanceObjs.push(gblNTAttendanceObject);
                } else if (!isEmpty(gblOTAttendanceObject)) {
                  	/** Below object changes are used to stop Overtime **/
              		gblOTAttendanceObject.endLocLatitude	= position.coords.latitude;
            		gblOTAttendanceObject.endLocLongitude	= position.coords.longitude;
              		gblOTAttendanceObject.endDateStr		= gblNSEAttendanceObject.startDateStr;
            		gblOTAttendanceObject.endTimestamp		= gblNSEAttendanceObject.startTimestamp;
            		gblOTAttendanceObject.endTimestampStr	= getTimeInGMT(gblNSEAttendanceObject.startTimestamp);
                  	attendanceObjs.push(gblOTAttendanceObject);
                }
            }
        }
      
      	printMessage("INSIDE successLocationCapture - attendanceObjs - ", attendanceObjs);
      	if (attendanceObjs.length > 0) {
          	createWorkAttendanceInDB(attendanceObjs, successCreateWorkAttendance, errorCreateWorkAttendance);
        } else {
          	saveActiveWorkAttendanceInTempTbl();
        }
    } catch(e) {
      	logException(e, "successLocationCapture", true);
	}          
}

/** Failure callback locaion capture **/
function errorLocationCapture(positionerror){
  	try {
      	printMessage("INSIDE errorLocationCapture");   
      	var attendanceObjs									= [];
      	if (gblAttendanceMode == WTConstant.NORMAL_TIME) {
          	gblNTAttendanceObject.startLocLatitude			= "";
        	gblNTAttendanceObject.startLocLongitude			= "";
          	gblNTAttendanceObject.startLocComment			= positionerror.message;
          
          	/** UK Mode - When we start Normal Time, need to end other activity if it is in active state **/
          	if (gblAppMode == WTConstant.APP_MODE_UK) {
             	if (!isEmpty(gblOTAttendanceObject)) {
                  	/** Below object changes are used to stop Overtime **/
              		gblOTAttendanceObject.endLocLatitude	= "";
            		gblOTAttendanceObject.endLocLongitude	= "";
                  	gblOTAttendanceObject.endLocComment		= positionerror.message;
              		gblOTAttendanceObject.endDateStr		= gblNTAttendanceObject.startDateStr;
            		gblOTAttendanceObject.endTimestamp		= gblNTAttendanceObject.startTimestamp;
            		gblOTAttendanceObject.endTimestampStr	= getTimeInGMT(gblNTAttendanceObject.startTimestamp);
                  	attendanceObjs.push(gblOTAttendanceObject);
                } else if (!isEmpty(gblNSEAttendanceObject)) {
                  	/** Below object changes are used to stop Non Service Event **/
              		gblNSEAttendanceObject.endLocLatitude	= "";
            		gblNSEAttendanceObject.endLocLongitude	= "";
                  	gblNSEAttendanceObject.endLocComment	= positionerror.message;
              		gblNSEAttendanceObject.endDateStr		= gblNTAttendanceObject.startDateStr;
            		gblNSEAttendanceObject.endTimestamp		= gblNTAttendanceObject.startTimestamp;
            		gblNSEAttendanceObject.endTimestampStr	= getTimeInGMT(gblNTAttendanceObject.startTimestamp);
                  	attendanceObjs.push(gblNSEAttendanceObject);
                }
            }
        } else if (gblAttendanceMode == WTConstant.OVER_TIME) {
          	gblOTAttendanceObject.startLocLatitude			= "";
        	gblOTAttendanceObject.startLocLongitude			= "";
          	gblOTAttendanceObject.startLocComment			= positionerror.message;
          
          	/** UK Mode - When we start Over Time, need to end other activity if it is in active state **/
          	if (gblAppMode == WTConstant.APP_MODE_UK) {
             	if (!isEmpty(gblNTAttendanceObject)) {
                  	/** Below object changes are used to stop Normal Time **/
              		gblNTAttendanceObject.endLocLatitude	= "";
            		gblNTAttendanceObject.endLocLongitude	= "";
                  	gblNTAttendanceObject.endLocComment		= positionerror.message;
              		gblNTAttendanceObject.endDateStr		= gblOTAttendanceObject.startDateStr;
            		gblNTAttendanceObject.endTimestamp		= gblOTAttendanceObject.startTimestamp;
            		gblNTAttendanceObject.endTimestampStr	= getTimeInGMT(gblOTAttendanceObject.startTimestamp);
                  	attendanceObjs.push(gblNTAttendanceObject);
                } else if (!isEmpty(gblNSEAttendanceObject)) {
                  	/** Below object changes are used to stop Non Service Event **/
              		gblNSEAttendanceObject.endLocLatitude	= "";
            		gblNSEAttendanceObject.endLocLongitude	= "";
                  	gblNSEAttendanceObject.endLocComment	= positionerror.message;
              		gblNSEAttendanceObject.endDateStr		= gblOTAttendanceObject.startDateStr;
            		gblNSEAttendanceObject.endTimestamp		= gblOTAttendanceObject.startTimestamp;
            		gblNSEAttendanceObject.endTimestampStr	= getTimeInGMT(gblOTAttendanceObject.startTimestamp);
                  	attendanceObjs.push(gblNSEAttendanceObject);
                }
            }
        } else {
          	gblNSEAttendanceObject.startLocLatitude			= "";
        	gblNSEAttendanceObject.startLocLongitude		= "";
          	gblNSEAttendanceObject.startLocComment			= positionerror.message;
          
          	/** UK Mode - When we start Non Service Event, need to end other activity if it is in active state **/
          	if (gblAppMode == WTConstant.APP_MODE_UK) {
             	if (!isEmpty(gblNTAttendanceObject)) {
                  	/** Below object changes are used to stop Normal Time **/
              		gblNTAttendanceObject.endLocLatitude	= "";
            		gblNTAttendanceObject.endLocLongitude	= "";
                  	gblNTAttendanceObject.endLocComment		= positionerror.message;
              		gblNTAttendanceObject.endDateStr		= gblNSEAttendanceObject.startDateStr;
            		gblNTAttendanceObject.endTimestamp		= gblNSEAttendanceObject.startTimestamp;
            		gblNTAttendanceObject.endTimestampStr	= getTimeInGMT(gblNSEAttendanceObject.startTimestamp);
                  	attendanceObjs.push(gblNTAttendanceObject);
                } else if (!isEmpty(gblOTAttendanceObject)) {
                  	/** Below object changes are used to stop Overtime **/
              		gblOTAttendanceObject.endLocLatitude	= "";
            		gblOTAttendanceObject.endLocLongitude	= "";
                  	gblOTAttendanceObject.endLocComment		= positionerror.message;
              		gblOTAttendanceObject.endDateStr		= gblNSEAttendanceObject.startDateStr;
            		gblOTAttendanceObject.endTimestamp		= gblNSEAttendanceObject.startTimestamp;
            		gblOTAttendanceObject.endTimestampStr	= getTimeInGMT(gblNSEAttendanceObject.startTimestamp);
                  	attendanceObjs.push(gblOTAttendanceObject);
                }
            }
        }

      	printMessage("INSIDE errorLocationCapture - attendanceObjs - ", attendanceObjs);
      	if (attendanceObjs.length > 0) {
          	createWorkAttendanceInDB(attendanceObjs, successCreateWorkAttendance, errorCreateWorkAttendance);
        } else {
          	saveActiveWorkAttendanceInTempTbl();
        }
      
        if (positionerror.message == WTConstant.LOCATION_UNAVAILABLE || positionerror.message == WTConstant.LOCATION_TIMEOUT || positionerror.message == WTConstant.LOCATION_PERMSN_DENIED) {
            showMessagePopup("common.message.turnOnLocation", true, dismissMessagePopup);
        }
 	} catch(e) {
      	logException(e, "errorLocationCapture", true);
	} 
}

/** Success callback locaion capture - Alternative call **/
function successLocationCaptureAlt(position) {
  	try {
      	printMessage("INSIDE successLocationCaptureAlt");
      	var attendanceObjs									= [];
      	if (gblAttendanceMode == WTConstant.NORMAL_TIME) {
          	gblNTAttendanceObject.endLocLatitude			= position.coords.latitude;
          	gblNTAttendanceObject.endLocLongitude			= position.coords.longitude;
          	attendanceObjs.push(gblNTAttendanceObject);
        } else if (gblAttendanceMode == WTConstant.OVER_TIME) {
          	gblOTAttendanceObject.endLocLatitude			= position.coords.latitude;
          	gblOTAttendanceObject.endLocLongitude			= position.coords.longitude;
          	attendanceObjs.push(gblOTAttendanceObject);
        } else {
          	gblNSEAttendanceObject.endLocLatitude			= position.coords.latitude;
          	gblNSEAttendanceObject.endLocLongitude			= position.coords.longitude;
          	attendanceObjs.push(gblNSEAttendanceObject);
        }
      
      	printMessage("INSIDE successLocationCaptureAlt - attendanceObjs - ", attendanceObjs);
      	if (attendanceObjs.length > 0) {
          	createWorkAttendanceInDB(attendanceObjs, successCreateWorkAttendanceAlt, errorCreateWorkAttendanceAlt);
        } else {
          	/** Invalid case. This will not occure **/
        }
    } catch(e) {
      	logException(e, "successLocationCaptureAlt", true);
	}          
}

/** Failure callback locaion capture - Alternative call **/
function errorLocationCaptureAlt(positionerror){
  	try {
      	printMessage("INSIDE errorLocationCaptureAlt");  
      	var attendanceObjs									= [];
      	if (gblAttendanceMode == WTConstant.NORMAL_TIME) {
          	gblNTAttendanceObject.endLocLatitude			= "";
          	gblNTAttendanceObject.endLocLongitude			= "";
          	gblNTAttendanceObject.endLocComment				= positionerror.message;
          	attendanceObjs.push(gblNTAttendanceObject);
        } else if (gblAttendanceMode == WTConstant.OVER_TIME) {
          	gblOTAttendanceObject.endLocLatitude			= "";
          	gblOTAttendanceObject.endLocLongitude			= "";
          	gblOTAttendanceObject.endLocComment				= positionerror.message;
          	attendanceObjs.push(gblOTAttendanceObject);
        } else {
          	gblNSEAttendanceObject.endLocLatitude			= "";
          	gblNSEAttendanceObject.endLocLongitude			= "";
          	gblNSEAttendanceObject.endLocComment			= positionerror.message;
          	attendanceObjs.push(gblNSEAttendanceObject);
        }
      
      	printMessage("INSIDE errorLocationCaptureAlt - attendanceObjs - ", attendanceObjs);
      	if (attendanceObjs.length > 0) {
          	createWorkAttendanceInDB(attendanceObjs, successCreateWorkAttendanceAlt, errorCreateWorkAttendanceAlt);
        } else {
          	/** Invalid case. This will not occure **/
        }

        if (positionerror.message == WTConstant.LOCATION_UNAVAILABLE || positionerror.message == WTConstant.LOCATION_TIMEOUT || positionerror.message == WTConstant.LOCATION_PERMSN_DENIED) {
            showMessagePopup("common.message.turnOnLocation", true, dismissMessagePopup);
        }
 	} catch(e) {
      	logException(e, "errorLocationCaptureAlt", true);
	} 
}