/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer web service based methods
***********************************************************************/

/** Integration service - AuthorizeUser  **/
function callAuthenticateUserIntService(inputData){
  	try {
      	printMessage("INSIDE callAuthenticateUserIntService");
        var integrationClient 			= null;
        var serviceName 				= inputData.serviceName;
        var operationName				= inputData.operationName;
        var params						= inputData.inputs;
        var headers						= null;
		integrationClient 				= KNYMobileFabric.getIntegrationService(serviceName);
  		integrationClient.invokeOperation(operationName, headers, params, success_authorizeEmailCallback, failure_authorizeEmailCallback); 
	} catch(e) {
      	logException(e, "callAuthenticateUserIntService", false);
   	}
}

/** Initialize Sync Module  **/
function initSyncModule(successCallback, errorCallback){ 	  	
  	try {
      	printMessage("INSIDE initSyncModule");
      	sync.init(successCallback, errorCallback);
    } catch(e) {
      	logException(e, "initSyncModule", true);
   	}
}

/** Reset sync **/
function resetSync(successCallback, errorCallback){
  	try {
      	printMessage("INSIDE resetSync - ");
  		showLoading("common.message.loading");
  		////sync.reset(onSyncResetSuccessCallback, onSyncResetFailureCallback);
      	sync.reset(successCallback, errorCallback);
   	} catch (e) {
      	logException(e, "resetSync");
    }  
}