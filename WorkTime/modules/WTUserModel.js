//****************Sync Version:Sync-Dev-8.0.0_v201711101237_r14*******************
// ****************Generated On Thu Jan 17 16:18:13 UTC 2019WTUser*******************
// **********************************Start WTUser's helper methods************************
if (typeof(kony) === "undefined") {
	kony = {};
}

if (typeof(kony.sync) === "undefined") {
	kony.sync = {};
}

if (typeof(kony.sync.log) === "undefined") {
	kony.sync.log = {};
}

if (typeof(sync) === "undefined") {
	sync = {};
}

if (typeof(sync.log) === "undefined") {
	sync.log = {};
}



if(typeof(com)=== "undefined"){ com = {}; }
if(typeof(com.wt)=== "undefined"){ com.wt = {}; }

/************************************************************************************
* Creates new WTUser
*************************************************************************************/
com.wt.WTUser = function(){
	this.employeeCode = null;
	this.countryCode = null;
	this.businessCode = null;
	this.languageCode = null;
	this.email = null;
	this.employeeName = null;
	this.icabsKey = null;
	this.branchNumber = null;
	this.branchName = null;
	this.lastSyncDate = null;
	this.isLoggedIn = null;
	this.buildMode = null;
	this.apkVersion = null;
	this.deviceId = null;
	this.packageName = null;
	this.loginDateTime = null;
	this.isWorkTimeActive = null;
	this.activeNTAttendanceRef = null;
	this.activeOTAttendanceRef = null;
	this.activeNSEAttendanceRef = null;
	this.markForUpload = true;
};

com.wt.WTUser.prototype = {
	get employeeCode(){
		return this._employeeCode;
	},
	set employeeCode(val){
		this._employeeCode = val;
	},
	get countryCode(){
		return this._countryCode;
	},
	set countryCode(val){
		this._countryCode = val;
	},
	get businessCode(){
		return this._businessCode;
	},
	set businessCode(val){
		this._businessCode = val;
	},
	get languageCode(){
		return this._languageCode;
	},
	set languageCode(val){
		this._languageCode = val;
	},
	get email(){
		return this._email;
	},
	set email(val){
		this._email = val;
	},
	get employeeName(){
		return this._employeeName;
	},
	set employeeName(val){
		this._employeeName = val;
	},
	get icabsKey(){
		return this._icabsKey;
	},
	set icabsKey(val){
		this._icabsKey = val;
	},
	get branchNumber(){
		return this._branchNumber;
	},
	set branchNumber(val){
		this._branchNumber = val;
	},
	get branchName(){
		return this._branchName;
	},
	set branchName(val){
		this._branchName = val;
	},
	get lastSyncDate(){
		return this._lastSyncDate;
	},
	set lastSyncDate(val){
		this._lastSyncDate = val;
	},
	get isLoggedIn(){
		return kony.sync.getBoolean(this._isLoggedIn)+"";
	},
	set isLoggedIn(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute isLoggedIn in WTUser.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._isLoggedIn = val;
	},
	get buildMode(){
		return this._buildMode;
	},
	set buildMode(val){
		this._buildMode = val;
	},
	get apkVersion(){
		return this._apkVersion;
	},
	set apkVersion(val){
		this._apkVersion = val;
	},
	get deviceId(){
		return this._deviceId;
	},
	set deviceId(val){
		this._deviceId = val;
	},
	get packageName(){
		return this._packageName;
	},
	set packageName(val){
		this._packageName = val;
	},
	get loginDateTime(){
		return this._loginDateTime;
	},
	set loginDateTime(val){
		this._loginDateTime = val;
	},
	get isWorkTimeActive(){
		return kony.sync.getBoolean(this._isWorkTimeActive)+"";
	},
	set isWorkTimeActive(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute isWorkTimeActive in WTUser.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._isWorkTimeActive = val;
	},
	get activeNTAttendanceRef(){
		return this._activeNTAttendanceRef;
	},
	set activeNTAttendanceRef(val){
		this._activeNTAttendanceRef = val;
	},
	get activeOTAttendanceRef(){
		return this._activeOTAttendanceRef;
	},
	set activeOTAttendanceRef(val){
		this._activeOTAttendanceRef = val;
	},
	get activeNSEAttendanceRef(){
		return this._activeNSEAttendanceRef;
	},
	set activeNSEAttendanceRef(val){
		this._activeNSEAttendanceRef = val;
	},
};

/************************************************************************************
* Retrieves all instances of WTUser SyncObject present in local database with
* given limit and offset where limit indicates the number of records to be retrieved
* and offset indicates number of rows to be ignored before returning the records.
* e.g. var orderByMap = []
* orderByMap[0] = {};
* orderByMap[0].key = "employeeCode";
* orderByMap[0].sortType ="desc";
* orderByMap[1] = {};
* orderByMap[1].key = "countryCode";
* orderByMap[1].sortType ="asc";
* var limit = 20;
* var offset = 5;
* com.wt.WTUser.getAll(successcallback,errorcallback, orderByMap, limit, offset)
*************************************************************************************/
com.wt.WTUser.getAll = function(successcallback, errorcallback, orderByMap, limit, offset){
	sync.log.trace("Entering com.wt.WTUser.getAll->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTUser.getTableName();
	orderByMap = kony.sync.formOrderByClause("WTUser",orderByMap);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);	
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_orderBy(query, orderByMap);
				kony.sync.qb_limitOffset(query,limit,offset);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];

	function mySuccCallback(res){
		sync.log.trace("Entering com.wt.WTUser.getAll->successcallback");
		successcallback(com.wt.WTUser.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname,sql, params,mySuccCallback,errorcallback);
};

/************************************************************************************
* Returns number of WTUser present in local database.
*************************************************************************************/
com.wt.WTUser.getAllCount = function(successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WTUser.getAllCount function");
	com.wt.WTUser.getCount("",successcallback,errorcallback);
};

/************************************************************************************
* Returns number of WTUser using where clause in the local Database
*************************************************************************************/
com.wt.WTUser.getCount = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WTUser.getCount->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTUser.getCount" , "getCount", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTUser.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select count(*) from \"" + tbname + "\" " + wcs;
	kony.sync.single_execute_sql(dbname,sql, null, mySuccCallback, errorcallback);
	function mySuccCallback(res) {
		sync.log.trace("Entering com.wt.WTUser.getCount->successcallback");
		if(null!==res){
			var count = null;
			count = res["count(*)"];
			kony.sync.verifyAndCallClosure(successcallback, {count:count});
		}
		else{
			sync.log.error("Some error occured while getting the count");
		}
	}
};

/************************************************************************************
* Creates a new instance of WTUser in the local Database. The new record will 
* be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
com.wt.WTUser.prototype.create = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.wt.WTUser.prototype.create function");
	var valuestable = this.getValuesTable(true);
	com.wt.WTUser.create(valuestable, successcallback,errorcallback,this.markForUpload);
};
com.wt.WTUser.create = function(valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering  com.wt.WTUser.create->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	
	if(!kony.sync.validateInput(arguments, "com.wt.WTUser.create" , "create", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTUser.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);

	if(kony.sync.attributeValidation(valuestable,"WTUser",errorcallback,true)===false){
		return;
	}
	
	function executeSuccess(){
		sync.log.trace("Entering  com.wt.WTUser.create->success callback");
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}

	var pks = [];
	var errMsg = "";
	
	function createSuccesscallback(res){
		if(res==null || res.length==0){
			var relationshipMap={};  
			relationshipMap = com.wt.WTUser.getRelationshipMap(relationshipMap,valuestable);
			kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
		}
		else{
			errMsg = "[" + errMsg + "]";
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey,kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
		}
	}
	
	if(kony.sync.enableORMValidations){
		errMsg = "employeeCode=" + valuestable.employeeCode;
		pks["employeeCode"] = {key:"employeeCode",value:valuestable.employeeCode};
		errMsg = errMsg + ", countryCode=" + valuestable.countryCode;
		pks["countryCode"] = {key:"countryCode",value:valuestable.countryCode};
		errMsg = errMsg + ", businessCode=" + valuestable.businessCode;
		pks["businessCode"] = {key:"businessCode",value:valuestable.businessCode};
		com.wt.WTUser.getAllDetailsByPK(pks,createSuccesscallback,errorcallback)
	}
	else{
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}
};

/************************************************************************************
* Creates number of new instances of WTUser in the local Database. These new 
* records will be merged with the enterprise datasource in the next Sync. Based upon 
* kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var valuesArray = [];
*		valuesArray[0] = {};
*		valuesArray[0].employeeCode = "employeeCode_0";
*		valuesArray[0].countryCode = "countryCode_0";
*		valuesArray[0].businessCode = "businessCode_0";
*		valuesArray[0].languageCode = "languageCode_0";
*		valuesArray[0].email = "email_0";
*		valuesArray[0].employeeName = "employeeName_0";
*		valuesArray[0].icabsKey = "icabsKey_0";
*		valuesArray[0].branchNumber = "branchNumber_0";
*		valuesArray[0].branchName = "branchName_0";
*		valuesArray[0].lastSyncDate = "lastSyncDate_0";
*		valuesArray[0].isLoggedIn = true;
*		valuesArray[0].buildMode = "buildMode_0";
*		valuesArray[0].apkVersion = "apkVersion_0";
*		valuesArray[0].deviceId = "deviceId_0";
*		valuesArray[0].packageName = "packageName_0";
*		valuesArray[0].loginDateTime = "loginDateTime_0";
*		valuesArray[0].isWorkTimeActive = true;
*		valuesArray[0].activeNTAttendanceRef = "activeNTAttendanceRef_0";
*		valuesArray[0].activeOTAttendanceRef = "activeOTAttendanceRef_0";
*		valuesArray[0].activeNSEAttendanceRef = "activeNSEAttendanceRef_0";
*		valuesArray[1] = {};
*		valuesArray[1].employeeCode = "employeeCode_1";
*		valuesArray[1].countryCode = "countryCode_1";
*		valuesArray[1].businessCode = "businessCode_1";
*		valuesArray[1].languageCode = "languageCode_1";
*		valuesArray[1].email = "email_1";
*		valuesArray[1].employeeName = "employeeName_1";
*		valuesArray[1].icabsKey = "icabsKey_1";
*		valuesArray[1].branchNumber = "branchNumber_1";
*		valuesArray[1].branchName = "branchName_1";
*		valuesArray[1].lastSyncDate = "lastSyncDate_1";
*		valuesArray[1].isLoggedIn = true;
*		valuesArray[1].buildMode = "buildMode_1";
*		valuesArray[1].apkVersion = "apkVersion_1";
*		valuesArray[1].deviceId = "deviceId_1";
*		valuesArray[1].packageName = "packageName_1";
*		valuesArray[1].loginDateTime = "loginDateTime_1";
*		valuesArray[1].isWorkTimeActive = true;
*		valuesArray[1].activeNTAttendanceRef = "activeNTAttendanceRef_1";
*		valuesArray[1].activeOTAttendanceRef = "activeOTAttendanceRef_1";
*		valuesArray[1].activeNSEAttendanceRef = "activeNSEAttendanceRef_1";
*		valuesArray[2] = {};
*		valuesArray[2].employeeCode = "employeeCode_2";
*		valuesArray[2].countryCode = "countryCode_2";
*		valuesArray[2].businessCode = "businessCode_2";
*		valuesArray[2].languageCode = "languageCode_2";
*		valuesArray[2].email = "email_2";
*		valuesArray[2].employeeName = "employeeName_2";
*		valuesArray[2].icabsKey = "icabsKey_2";
*		valuesArray[2].branchNumber = "branchNumber_2";
*		valuesArray[2].branchName = "branchName_2";
*		valuesArray[2].lastSyncDate = "lastSyncDate_2";
*		valuesArray[2].isLoggedIn = true;
*		valuesArray[2].buildMode = "buildMode_2";
*		valuesArray[2].apkVersion = "apkVersion_2";
*		valuesArray[2].deviceId = "deviceId_2";
*		valuesArray[2].packageName = "packageName_2";
*		valuesArray[2].loginDateTime = "loginDateTime_2";
*		valuesArray[2].isWorkTimeActive = true;
*		valuesArray[2].activeNTAttendanceRef = "activeNTAttendanceRef_2";
*		valuesArray[2].activeOTAttendanceRef = "activeOTAttendanceRef_2";
*		valuesArray[2].activeNSEAttendanceRef = "activeNSEAttendanceRef_2";
*		com.wt.WTUser.createAll(valuesArray, successcallback, errorcallback, true);
*************************************************************************************/
com.wt.WTUser.createAll = function(valuesArray, successcallback, errorcallback, markForUpload){
	sync.log.trace("Entering com.wt.WTUser.createAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTUser.createAll" , "createAll", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTUser.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var isProperData = true;
	var arrayLen = 0;
	var errorInfo = [];
	var arrayLength = valuesArray.length;
	var errObject = null;
	var isReferentialIntegrityFailure = false;
	var errMsg = null;
	if(kony.sync.enableORMValidations){
		var newValuesArray = [];

		//column level validations
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
			if(kony.sync.attributeValidation(valuestable,"WTUser",errorcallback,true)===false){
				return;
			}

			newValuesArray[i] = valuestable;
		}
		valuesArray = newValuesArray;
		var isDuplicateKey = false;
		//checking for duplicate records
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkDuplicatePkCallback, transactionSuccessCallback, transactionErrorCallback);
		var isError = false;
		function checkDuplicatePkCallback(tx){
			arrayLength = valuesArray.length;
			for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
				var pks = [];
				errMsg = "employeeCode=" + valuestable.employeeCode;
				pks["employeeCode"] = {key:"employeeCode",value:valuestable.employeeCode};
				errMsg = errMsg + ", countryCode=" + valuestable.countryCode;
				pks["countryCode"] = {key:"countryCode",value:valuestable.countryCode};
				errMsg = errMsg + ", businessCode=" + valuestable.businessCode;
				pks["businessCode"] = {key:"businessCode",value:valuestable.businessCode};
				var wcs = [];
				if(com.wt.WTUser.pkCheck(pks,wcs,errorcallback,"searching")===false){
					isError = true;
					return;
				}
				var query = kony.sync.qb_createQuery();
							kony.sync.qb_select(query, null);
							kony.sync.qb_from(query, tbname);
							kony.sync.qb_where(query, wcs);
				var query_compile = kony.sync.qb_compile(query);
				var sql = query_compile[0];
				var params = query_compile[1];
				var resultset = kony.sync.executeSql(tx, sql, params);
				if(resultset===false){
					isError = true;
					return;
				}
				if(resultset.rows.length!=0){
					isError = true;
					errMsg = "[" + errMsg + "]";
					isDuplicateKey = true;
					return;
				}
			}
			if(!isError){
				checkIntegrity(tx);
			}
		}
	}
	else{
		//copying by value
		var newValuesArray = [];
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			newValuesArray[i] = kony.sync.CreateCopy(valuesArray[i]);
		}
		valuesArray = newValuesArray;
		kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
	}

	function transactionErrorCallback(){
		if(isError==true){
			//Statement error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
		}
		else{
			//Transaction error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}

	function transactionSuccessCallback(){
		sync.log.trace("Entering  com.wt.WTUser.createAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
		}
		else{
			if(isReferentialIntegrityFailure){
				kony.sync.verifyAndCallClosure(errorcallback, errObject);
			}
			if(isDuplicateKey){
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey,kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
			}
		}
	}
	
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  com.wt.WTUser.createAll->checkIntegrity");
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var relationshipMap={};  
			relationshipMap = com.wt.WTUser.getRelationshipMap(relationshipMap,valuesArray[i]);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				isReferentialIntegrityFailure = true;
				return;
			}
		}
	}
};
/************************************************************************************
* Updates WTUser using primary key in the local Database. The update will be
* merged with the enterprise datasource in the next Sync.
*************************************************************************************/
com.wt.WTUser.prototype.updateByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.wt.WTUser.prototype.updateByPK function");
	var pks = this.getPKTable();
	var valuestable = this.getValuesTable(false);
	com.wt.WTUser.updateByPK(pks,valuestable, successcallback,errorcallback,this.markForUpload);
};
com.wt.WTUser.updateByPK = function(pks,valuestable, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering  com.wt.WTUser.updateByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTUser.updateByPK",  "updateByPk", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTUser.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [ ];

	if(com.wt.WTUser.pkCheck(pks,wcs,errorcallback,"updating")===false){
		return;
	}

	if(kony.sync.attributeValidation(valuestable,"WTUser",errorcallback,false)===false){
		return;
	}

	var relationshipMap={};  
	relationshipMap = com.wt.WTUser.getRelationshipMap(relationshipMap,valuestable);

	kony.sync.updateByPK(tbname, dbname, relationshipMap, pks,valuestable, successcallback,errorcallback, markForUpload, wcs);
};

/************************************************************************************
* Updates WTUser(s) using where clause in the local Database. The update(s)
* will be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
com.wt.WTUser.update = function(wcs, valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering com.wt.WTUser.update function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTUser.update",  "update", errorcallback)){
		return;
	}

	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTUser.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);

	if(kony.sync.attributeValidation(valuestable,"WTUser",errorcallback,false)===false){
		return;
	}
	function executeSuccess(){
		sync.log.trace("Entering  com.wt.WTUser.update-> success callback of Integrity Check");
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, com.wt.WTUser.getPKTable());
	}

	if(kony.sync.enableORMValidations){
		var relationshipMap={};  
		relationshipMap = com.wt.WTUser.getRelationshipMap(relationshipMap,valuestable);
		kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
	}
	else{
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, com.wt.WTUser.getPKTable());
	}
};

/************************************************************************************
* Updates WTUser(s) satisfying one or more where clauses in the local Database. 
* The update(s) will be merged with the enterprise datasource in the next Sync.
* Based upon kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var inputArray = [];
*		inputArray[0] = {};
*		inputArray[0].changeSet = {};
*		inputArray[0].changeSet.languageCode = "languageCode_updated0";
*		inputArray[0].changeSet.email = "email_updated0";
*		inputArray[0].changeSet.employeeName = "employeeName_updated0";
*		inputArray[0].changeSet.icabsKey = "icabsKey_updated0";
*		inputArray[0].whereClause = "where employeeCode = '0'";
*		inputArray[0].whereClause = "where countryCode = '0'";
*		inputArray[0].whereClause = "where businessCode = '0'";
*		inputArray[1] = {};
*		inputArray[1].changeSet = {};
*		inputArray[1].changeSet.languageCode = "languageCode_updated1";
*		inputArray[1].changeSet.email = "email_updated1";
*		inputArray[1].changeSet.employeeName = "employeeName_updated1";
*		inputArray[1].changeSet.icabsKey = "icabsKey_updated1";
*		inputArray[1].whereClause = "where employeeCode = '1'";
*		inputArray[1].whereClause = "where countryCode = '1'";
*		inputArray[1].whereClause = "where businessCode = '1'";
*		inputArray[2] = {};
*		inputArray[2].changeSet = {};
*		inputArray[2].changeSet.languageCode = "languageCode_updated2";
*		inputArray[2].changeSet.email = "email_updated2";
*		inputArray[2].changeSet.employeeName = "employeeName_updated2";
*		inputArray[2].changeSet.icabsKey = "icabsKey_updated2";
*		inputArray[2].whereClause = "where employeeCode = '2'";
*		inputArray[2].whereClause = "where countryCode = '2'";
*		inputArray[2].whereClause = "where businessCode = '2'";
*		com.wt.WTUser.updateAll(inputArray,successcallback,errorcallback);
*************************************************************************************/
com.wt.WTUser.updateAll = function(inputArray, successcallback, errorcallback, markForUpload) {
	sync.log.trace("Entering com.wt.WTUser.updateAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTUser.updateAll",  "updateAll", errorcallback)){
		return;
	}
	var dbname = "WorkTime_EMEA_SIT_2_1";
	var tbname = "WTUser";
	var isError = false;
	var errObject = null;
	if(markForUpload == false || markForUpload == "false"){
		markForUpload="false"
	}
	else{
		markForUpload="true"
	}
	if((kony.sync.enableORMValidations)){

		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var v = inputArray[i];
			var valuestable = v.changeSet;
			var isEmpty = true;
			for(var key in valuestable){
				isEmpty = false;
				break;
			}
			if(isEmpty){
				errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeNullValue,kony.sync.getErrorMessage(kony.sync.errorCodeNullValue)));
				return;
			}
			var wcs = v.whereClause;
			var twcs = wcs;
			if(kony.sync.attributeValidation(valuestable,"WTUser",errorcallback,false)===false){
				return;
			}

			newInputArray[i] = [];
			newInputArray[i].changeSet = valuestable;
			newInputArray[i].whereClause = wcs;
		}
		inputArray = newInputArray;
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);

	}
	else{
		//copying by value
		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
		    var v = inputArray[i];
		    newInputArray[i] = kony.sync.CreateCopy(v);
		}
		inputArray = newInputArray;
		kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,errorcallback,markForUpload, com.wt.WTUser.getPKTable());
	}
	
		function transactionSuccessCallback(){
		sync.log.trace("Entering  com.wt.WTUser.updateAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,transactionErrorCallback,markForUpload, com.wt.WTUser.getPKTable());
		}
	}

	function transactionErrorCallback(){
		if(errObject===false){
			//Sql statement error has occcurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
			
		}
		else if(errObject!==null){
			// Referential integrity error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, errObject);
		}
		else{
			//Transaction error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  com.wt.WTUser.updateAll->checkIntegrity");
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var relationshipMap={}; 
			relationshipMap = com.wt.WTUser.getRelationshipMap(relationshipMap,inputArray[i].changeSet);
			sync.log.debug("Relationship Map for Integrity check created:", relationshipMap);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				kony.sync.rollbackTransaction(tx);
				return;
			}
		}
	}


}
/************************************************************************************
* Deletes WTUser using primary key from the local Database. The record will be
* deleted from the enterprise datasource in the next Sync.
*************************************************************************************/
com.wt.WTUser.prototype.deleteByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WTUser.prototype.deleteByPK function");
	var pks = this.getPKTable();
	com.wt.WTUser.deleteByPK(pks,successcallback,errorcallback,this.markForUpload);
};
com.wt.WTUser.deleteByPK = function(pks, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering com.wt.WTUser.deleteByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTUser.deleteByPK",  "deleteByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTUser.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var twcs = [];
	var deletedRows;
	var record = "";
	if(com.wt.WTUser.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}	
	twcs = kony.sync.CreateCopy(wcs);
	function WTUserTransactionCallback(tx){
		sync.log.trace("Entering com.wt.WTUser.deleteByPK->WTUser_PKPresent successcallback");
		record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(record===false){
			isError = true;
			return;
		}
		if (null !== record) {
		}else{
			pkNotFound = true;
		}
		var deletedRows = kony.sync.remove(tx, tbname, wcs, false, markForUpload, null);
			if(deletedRows === false){
				isError = true;
			}
	}
	
	function WTUserErrorCallback(){
		sync.log.error("Entering com.wt.WTUser.deleteByPK->relationship failure callback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}

	}
	function WTUserSuccessCallback(){
		sync.log.trace("Entering com.wt.WTUser.deleteByPK->relationship success callback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering com.wt.WTUser.deleteByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
		
		var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(dbconnection != null){
			kony.sync.startTransaction(dbconnection, WTUserTransactionCallback, WTUserSuccessCallback, WTUserErrorCallback, "Single Execute");
		}

};

/************************************************************************************
* Deletes WTUser(s) using where clause from the local Database. The record(s)
* will be deleted from the enterprise datasource in the next Sync.
* e.g. com.wt.WTUser.remove("where employeeCode like 'A%'", successcallback,errorcallback, true);
*************************************************************************************/
com.wt.WTUser.remove = function(wcs, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering com.wt.WTUser.remove->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTUser.remove",  "remove", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTUser.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;
	var record = "";

	function WTUser_removeTransactioncallback(tx){
			wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, false, markForUpload, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function WTUser_removeSuccess(){
		sync.log.trace("Entering com.wt.WTUser.remove->WTUser_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering com.wt.WTUser.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering com.wt.WTUser.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, WTUser_removeTransactioncallback, WTUser_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Deletes WTUser using primary key from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
com.wt.WTUser.prototype.removeDeviceInstanceByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WTUser.prototype.removeDeviceInstanceByPK function");
	var pks = this.getPKTable();
	com.wt.WTUser.removeDeviceInstanceByPK(pks,successcallback,errorcallback);
};
com.wt.WTUser.removeDeviceInstanceByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WTUser.removeDeviceInstanceByPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTUser.removeDeviceInstanceByPK",  "removeDeviceInstanceByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTUser.getTableName();
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var deletedRows;
	if(com.wt.WTUser.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}
	
	function WTUserTransactionCallback(tx){
		sync.log.trace("Entering com.wt.WTUser.removeDeviceInstanceByPK -> WTUserTransactionCallback");
		var record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(null !== record && false !=record) {
			deletedRows = kony.sync.remove(tx, tbname, wcs, true, null, null);
			if(deletedRows === false){
				isError = true;
			}
		}else{
			pkNotFound = true;
		}
	}
	
	function WTUserErrorCallback(){
		sync.log.error("Entering com.wt.WTUser.removeDeviceInstanceByPK -> WTUserErrorCallback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	function WTUserSuccessCallback(){
		sync.log.trace("Entering com.wt.WTUser.removeDeviceInstanceByPK -> WTUserSuccessCallback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering com.wt.WTUser.removeDeviceInstanceByPK -> PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
	
	var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(dbconnection != null){
		kony.sync.startTransaction(dbconnection, WTUserTransactionCallback, WTUserSuccessCallback, WTUserErrorCallback, "Single Execute");
	}

};

/************************************************************************************
* Deletes WTUser(s) using where clause from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
com.wt.WTUser.removeDeviceInstance = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WTUser.removeDeviceInstance->main function");
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTUser.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;

	function WTUser_removeTransactioncallback(tx){
		wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, true, null, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function WTUser_removeSuccess(){
		sync.log.trace("Entering com.wt.WTUser.remove->WTUser_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering com.wt.WTUser.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering com.wt.WTUser.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, WTUser_removeTransactioncallback, WTUser_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Retrieves WTUser using primary key from the local Database. 
*************************************************************************************/
com.wt.WTUser.prototype.getAllDetailsByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WTUser.prototype.getAllDetailsByPK function");
	var pks = this.getPKTable();
	com.wt.WTUser.getAllDetailsByPK(pks,successcallback,errorcallback);
};
com.wt.WTUser.getAllDetailsByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WTUser.getAllDetailsByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTUser.getAllDetailsByPK",  "getAllDetailsByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTUser.getTableName();
	var wcs = [];
	if(com.wt.WTUser.pkCheck(pks,wcs,errorcallback,"searching")===false){
		return;
	}
	twcs = kony.sync.CreateCopy(wcs);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, wcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	function mySuccCallback(res){
		sync.log.trace("Entering com.wt.WTUser.getAllDetailsByPK-> success callback function");
		successcallback(com.wt.WTUser.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};






/************************************************************************************
* Retrieves WTUser(s) using where clause from the local Database. 
* e.g. com.wt.WTUser.find("where employeeCode like 'A%'", successcallback,errorcallback);
*************************************************************************************/
com.wt.WTUser.find = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WTUser.find function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTUser.find",  "find", errorcallback)){
		return;
	}
	//wcs will be a string formed by the user.
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTUser.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select * from \"" + tbname + "\" " + wcs;
	function mySuccCallback(res){
		kony.sync.verifyAndCallClosure(successcallback, com.wt.WTUser.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccCallback, errorcallback);
};

/************************************************************************************
* Marks instance of WTUser with given primary key for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
com.wt.WTUser.prototype.markForUploadbyPK = function(successcallback, errorcallback){
	sync.log.trace("Entering com.wt.WTUser.prototype.markForUploadbyPK function");
	var pks = this.getPKTable();
	com.wt.WTUser.markForUploadbyPK(pks, successcallback, errorcallback);
};
com.wt.WTUser.markForUploadbyPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.wt.WTUser.markForUploadbyPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTUser.markForUploadbyPK",  "markForUploadbyPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTUser.getTableName();
	var isError = false;
	var recordsFound = false;
	var recordsMarkedForUpload = 0;
	var wcs = [];
	if(com.wt.WTUser.pkCheck(pks, wcs, errorcallback, "marking for upload by PK")===false){
		return;
	}

	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);		
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = [];
		twcs = wcs;
		kony.table.insert(twcs,{key : kony.sync.historyTableChangeTypeColumn, value : record[kony.sync.historyTableChangeTypeColumn], optype : "EQ",comptype : "AND"});
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
					kony.sync.qb_where(query, twcs);
		kony.table.remove(twcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);
	}
	
	function single_transaction_callback (tx){
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_select(query, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query, tbname);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		var resultSet = kony.sync.executeSql(tx, sql, params);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		if(num_records > 0){
			recordsFound = true;
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, 0);
			var changeType = record[kony.sync.mainTableChangeTypeColumn];
			if(!kony.sync.isNullOrUndefined(changeType) && kony.string.startsWith(""+changeType,"9")){
				recordsMarkedForUpload = 1;
				if(markRecordForUpload(tx, record) === false){
					isError = true;
					return;
				}
			}
		}
					
				
		var query1 =kony.sync.qb_createQuery();
					kony.sync.qb_select(query1, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query1, tbname + kony.sync.historyTableName);
					kony.sync.qb_where(query1, wcs);
		var query1_compile = kony.sync.qb_compile(query1);
		var sql1 = query1_compile[0];
		var params1 = query1_compile[1];
		var resultSet1 = kony.sync.executeSql (tx, sql1, params1);
		if(resultSet1!==false){
			var num_records = resultSet1.rows.length;
			for(var i = 0; i <= num_records - 1; i++ ){
				var record = kony.db.sqlResultsetRowItem(tx, resultSet1, i);
				if(markRecordForUploadHistory(tx, record) === false){
					isError = true;
					return;
				}
				recordsFound = true;
			}
		}
		else{
			isError = true;
		}
	}
	function single_transaction_success_callback(){
		if(recordsFound === true){
			kony.sync.verifyAndCallClosure(successcallback , {count:recordsMarkedForUpload});
		}
		else{
			kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
		}
	}
	
	function single_transaction_error_callback(res){
		if (!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Marks instance(s) of WTUser matching given where clause for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
com.wt.WTUser.markForUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WTUser.markForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTUser.markForUpload",  "markForUpload", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTUser.getTableName();
	var isError = false;
	var num_records_main = 0;
	wcs = kony.sync.validateWhereClause(wcs);
	if(!kony.sync.isNull(wcs) && !kony.sync.isEmptyString(wcs)) {
		wcs = wcs + " and " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}else{	
		wcs = "where " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}
	
	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0] + " " + wcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = "";
		twcs = wcs;
		twcs = twcs + " AND " + kony.sync.historyTableChangeTypeColumn + " = " + record[kony.sync.historyTableChangeTypeColumn];
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0]  + " " + twcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function single_transaction_callback (tx){
		sync.log.trace("Entering com.wt.WTUser.markForUpload->single_transaction_callback");
		//updating main table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from \"" + tbname + "\" " + wcs ;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}
		
		num_records_main = resultSet.rows.length;
		for(var i = 0; i < num_records_main; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUpload(tx, record) === false){
				isError = true;
				return;
			}
		}
		
		//updating history table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from " + tbname + kony.sync.historyTableName + " " + wcs;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		for ( var i = 0; i <= num_records - 1; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUploadHistory(tx, record)=== false){
				isError = true;
				return;
			}
		}
	}
	
	function single_transaction_success_callback(){
		sync.log.trace("Entering com.wt.WTUser.markForUpload->single_transaction_success_callback");
		kony.sync.verifyAndCallClosure(successcallback, {count:num_records_main});
	}
	
	function single_transaction_error_callback(){
		sync.log.error("Entering com.wt.WTUser.markForUpload->single_transaction_error_callback");
		if(!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Retrieves instance(s) of WTUser pending for upload. Records are marked for
* pending upload if they have been updated or created locally and the changes have
* not been merged with enterprise datasource.
*************************************************************************************/
com.wt.WTUser.getPendingUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WTUser.getPendingUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTUser.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var sql;
	if(typeof(wcs) === "string" && wcs != null){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname + "\" "+ wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.wt.WTUser.getPendingUpload->successcallback function");
		kony.sync.verifyAndCallClosure(successcallback, com.wt.WTUser.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of WTUser pending for acknowledgement. This is relevant
* when the SyncObject is part of the SyncScope whose SyncStrategy is PersistentSync.
* In persistent Sync the  records in the local database are put into a pending 
* acknowledgement state after an upload.
*************************************************************************************/
com.wt.WTUser.getPendingAcknowledgement = function(successcallback, errorcallback){
	sync.log.trace("Entering com.wt.WTUser.getPendingAcknowledgement->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTUser.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var mysql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" <> "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	kony.sync.single_select_execute(dbname, mysql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.wt.WTUser.getPendingAcknowledgement success callback function");
		kony.sync.verifyAndCallClosure(successcallback, com.wt.WTUser.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of WTUser deferred for upload.
*************************************************************************************/
com.wt.WTUser.getDeferredUpload = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WTUser.getDeferredUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTUser.getTableName();
	var sql;
	if(typeof(wcs) === "string" && wcs != null ){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname +  "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'";
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'"; 
	}
	
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.wt.WTUser.getDeferredUpload->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, com.wt.WTUser.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
};

/************************************************************************************
* Rollbacks all changes to WTUser in local database to last synced state
*************************************************************************************/
com.wt.WTUser.rollbackPendingLocalChanges = function(successcallback, errorcallback){
	sync.log.trace("Entering com.wt.WTUser.rollbackPendingLocalChanges->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTUser.getTableName();
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.wt.WTUser.rollbackPendingLocalChanges->main function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}		
};

/************************************************************************************
* Rollbacks changes to WTUser's record with given primary key in local 
* database to last synced state
*************************************************************************************/
com.wt.WTUser.prototype.rollbackPendingLocalChangesByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WTUser.prototype.rollbackPendingLocalChangesByPK function");
	var pks = this.getPKTable();
	com.wt.WTUser.rollbackPendingLocalChangesByPK(pks,successcallback,errorcallback);
};
com.wt.WTUser.rollbackPendingLocalChangesByPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.wt.WTUser.rollbackPendingLocalChangesByPK->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTUser.rollbackPendingLocalChangesByPK",  "rollbackPendingLocalChangesByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTUser.getTableName();
	var wcs = [];
	if(com.wt.WTUser.pkCheck(pks,wcs,errorcallback,"rollbacking")===false){
		return;
	}	
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, wcs, mySuccesscallback, pkNotFoundErrCallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.wt.WTUser.rollbackPendingLocalChangesByPK->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering com.wt.WTUser.rollbackPendingLocalChangesByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
};

/************************************************************************************
* isRecordDeferredForUpload returns true or false depending on whether WTUser's record  
* with given primary key got deferred in last sync
*************************************************************************************/
com.wt.WTUser.prototype.isRecordDeferredForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.wt.WTUser.prototype.isRecordDeferredForUpload function");
	var pks = this.getPKTable();
	com.wt.WTUser.isRecordDeferredForUpload(pks,successcallback,errorcallback);
};
com.wt.WTUser.isRecordDeferredForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.wt.WTUser.isRecordDeferredForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTUser.isRecordDeferredForUpload",  "isRecordDeferredForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTUser.getTableName();
	var wcs = [] ;
	var flag;
	if(com.wt.WTUser.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.wt.WTUser.isRecordDeferredForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {deferred:flag});
	}
};

/************************************************************************************
* isRecordPendingForUpload returns true or false depending on whether WTUser's record  
* with given primary key is pending for upload
*************************************************************************************/
com.wt.WTUser.prototype.isRecordPendingForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.wt.WTUser.prototype.isRecordPendingForUpload function");
	var pks = this.getPKTable();
	com.wt.WTUser.isRecordPendingForUpload(pks,successcallback,errorcallback);
};
com.wt.WTUser.isRecordPendingForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.wt.WTUser.isRecordPendingForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTUser.isRecordPendingForUpload",  "isRecordPendingForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTUser.getTableName();
	var wcs = [] ;
	var flag;
	if(com.wt.WTUser.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "NOT LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.wt.WTUser.isRecordPendingForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {pending:flag});
	}
};




/************************************************************************************
* Start of helper functions used internally, not to be used as ORMs
*************************************************************************************/

//Deletes all the dependant tables in the relationship tables.Need to pass transaction handler as input
com.wt.WTUser.removeCascade = function(tx, wcs, errorcallback, markForUpload, isCascade, parentTable, isLocal){
	sync.log.trace("Entering com.wt.WTUser.removeCascade function");
	var tbname = com.wt.WTUser.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	function removeCascadeChildren(){
	}
	if(isCascade){
		if(removeCascadeChildren()===false){
			return false;
		}
		if(kony.sync.deleteBatch(tx, tbname, wcs, isLocal,markForUpload, null)===false){
			return false;
		}
		return true;
	}else{
		var sql = "select * from \"" + tbname + "\" " + wcs;
		var resultSet = kony.sync.executeSql(tx, sql, null);
		if(resultSet===false){
			return false;
		}	
		var num_records = resultSet.rows.length;
		if(num_records === 0){
			return true;
		}else{
			sync.log.error(kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable));
			errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeReferentialIntegrity,kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable)));
			return false;
		}
	}
};


com.wt.WTUser.convertTableToObject = function(res){
	sync.log.trace("Entering com.wt.WTUser.convertTableToObject function");
	objMap = [];
	if(res!==null){
		for(var i in res){
			var obj = new com.wt.WTUser();
			obj.employeeCode = res[i].employeeCode;
			obj.countryCode = res[i].countryCode;
			obj.businessCode = res[i].businessCode;
			obj.languageCode = res[i].languageCode;
			obj.email = res[i].email;
			obj.employeeName = res[i].employeeName;
			obj.icabsKey = res[i].icabsKey;
			obj.branchNumber = res[i].branchNumber;
			obj.branchName = res[i].branchName;
			obj.lastSyncDate = res[i].lastSyncDate;
			obj.isLoggedIn = res[i].isLoggedIn;
			obj.buildMode = res[i].buildMode;
			obj.apkVersion = res[i].apkVersion;
			obj.deviceId = res[i].deviceId;
			obj.packageName = res[i].packageName;
			obj.loginDateTime = res[i].loginDateTime;
			obj.isWorkTimeActive = res[i].isWorkTimeActive;
			obj.activeNTAttendanceRef = res[i].activeNTAttendanceRef;
			obj.activeOTAttendanceRef = res[i].activeOTAttendanceRef;
			obj.activeNSEAttendanceRef = res[i].activeNSEAttendanceRef;
			obj.markForUpload = (Math.floor(res[i].konysyncchangetype/10)==9)? false:true;
			objMap[i] = obj;
		}
	}
	return objMap;
};

com.wt.WTUser.filterAttributes = function(valuestable, insert){
	sync.log.trace("Entering com.wt.WTUser.filterAttributes function");
	var attributeTable = {};
	attributeTable.employeeCode = "employeeCode";
	attributeTable.countryCode = "countryCode";
	attributeTable.businessCode = "businessCode";
	attributeTable.languageCode = "languageCode";
	attributeTable.email = "email";
	attributeTable.employeeName = "employeeName";
	attributeTable.icabsKey = "icabsKey";
	attributeTable.branchNumber = "branchNumber";
	attributeTable.branchName = "branchName";
	attributeTable.lastSyncDate = "lastSyncDate";
	attributeTable.isLoggedIn = "isLoggedIn";
	attributeTable.buildMode = "buildMode";
	attributeTable.apkVersion = "apkVersion";
	attributeTable.deviceId = "deviceId";
	attributeTable.packageName = "packageName";
	attributeTable.loginDateTime = "loginDateTime";
	attributeTable.isWorkTimeActive = "isWorkTimeActive";
	attributeTable.activeNTAttendanceRef = "activeNTAttendanceRef";
	attributeTable.activeOTAttendanceRef = "activeOTAttendanceRef";
	attributeTable.activeNSEAttendanceRef = "activeNSEAttendanceRef";

	var PKTable = {};
	PKTable.employeeCode = {}
	PKTable.employeeCode.name = "employeeCode";
	PKTable.employeeCode.isAutoGen = false;
	PKTable.countryCode = {}
	PKTable.countryCode.name = "countryCode";
	PKTable.countryCode.isAutoGen = false;
	PKTable.businessCode = {}
	PKTable.businessCode.name = "businessCode";
	PKTable.businessCode.isAutoGen = false;
	var newvaluestable = {};
	for (var k in valuestable){
		var v = valuestable[k];
		if(kony.sync.isNull(attributeTable[k])) { 
			sync.log.warn("Ignoring the attribute " + k + " for the SyncObject WTUser. "  + k + " not defined as an attribute in SyncConfiguration.");
		}else if(!kony.sync.isNull(PKTable[k])) {
			if(insert===false){
				sync.log.warn("Ignoring the primary key " + k + " for the SyncObject WTUser. Primary Key should not be the part of the attributes to be updated in the local device database.");
			}else if(PKTable[k].isAutoGen){
				sync.log.warn("Ignoring the auto-generated primary key " + k + " for the SyncObject WTUser. Auto-generated Primary Key should not be the part of the attributes to be inserted in the local device database.");
			}else{
				newvaluestable[k] = v;
			}
		}
		else{
			newvaluestable[k] = v;
		}
	}
	return newvaluestable;
};

com.wt.WTUser.formOrderByClause = function(orderByMap){
	sync.log.trace("Entering com.wt.WTUser.formOrderByClause function");
	if(!kony.sync.isNull(orderByMap)){
		var valuestable = kony.sync.convertOrderByMapToValuesTable(orderByMap);
		//var filteredValuestable = com.wt.WTUser.filterAttributes(valuestable, true);
		return kony.sync.convertToValuesTableOrderByMap(orderByMap,valuestable);
	}
	return null;
};

com.wt.WTUser.prototype.getValuesTable = function(isInsert){
	sync.log.trace("Entering com.wt.WTUser.prototype.getValuesTable function");
	var valuesTable = {};
	if(isInsert===true){
		valuesTable.employeeCode = this.employeeCode;
	}
	if(isInsert===true){
		valuesTable.countryCode = this.countryCode;
	}
	if(isInsert===true){
		valuesTable.businessCode = this.businessCode;
	}
	valuesTable.languageCode = this.languageCode;
	valuesTable.email = this.email;
	valuesTable.employeeName = this.employeeName;
	valuesTable.icabsKey = this.icabsKey;
	valuesTable.branchNumber = this.branchNumber;
	valuesTable.branchName = this.branchName;
	valuesTable.lastSyncDate = this.lastSyncDate;
	valuesTable.isLoggedIn = this.isLoggedIn;
	valuesTable.buildMode = this.buildMode;
	valuesTable.apkVersion = this.apkVersion;
	valuesTable.deviceId = this.deviceId;
	valuesTable.packageName = this.packageName;
	valuesTable.loginDateTime = this.loginDateTime;
	valuesTable.isWorkTimeActive = this.isWorkTimeActive;
	valuesTable.activeNTAttendanceRef = this.activeNTAttendanceRef;
	valuesTable.activeOTAttendanceRef = this.activeOTAttendanceRef;
	valuesTable.activeNSEAttendanceRef = this.activeNSEAttendanceRef;
	return valuesTable;
};

com.wt.WTUser.prototype.getPKTable = function(){
	sync.log.trace("Entering com.wt.WTUser.prototype.getPKTable function");
	var pkTable = {};
	pkTable.employeeCode = {key:"employeeCode",value:this.employeeCode};
	pkTable.countryCode = {key:"countryCode",value:this.countryCode};
	pkTable.businessCode = {key:"businessCode",value:this.businessCode};
	return pkTable;
};

com.wt.WTUser.getPKTable = function(){
	sync.log.trace("Entering com.wt.WTUser.getPKTable function");
	var pkTable = [];
	pkTable.push("employeeCode");
	pkTable.push("countryCode");
	pkTable.push("businessCode");
	return pkTable;
};

com.wt.WTUser.pkCheck = function(pks,wcs,errorcallback,opName){
	sync.log.trace("Entering com.wt.WTUser.pkCheck function");
	var wc = [];
	if(!kony.sync.isNull(pks.employeeCode)){
		if(!kony.sync.isNull(pks.employeeCode.value)){
			wc.key = "employeeCode";
			wc.value = pks.employeeCode.value;
		}
		else{
			wc.key = "employeeCode";
			wc.value = pks.employeeCode;
		}
	}else{
		sync.log.error("Primary Key employeeCode not specified in " + opName + " an item in WTUser");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("employeeCode",opName,"WTUser")));
		return;
	}
	kony.table.insert(wcs,wc);
	var wc = [];
	if(!kony.sync.isNull(pks.countryCode)){
		if(!kony.sync.isNull(pks.countryCode.value)){
			wc.key = "countryCode";
			wc.value = pks.countryCode.value;
		}
		else{
			wc.key = "countryCode";
			wc.value = pks.countryCode;
		}
	}else{
		sync.log.error("Primary Key countryCode not specified in " + opName + " an item in WTUser");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("countryCode",opName,"WTUser")));
		return;
	}
	kony.table.insert(wcs,wc);
	var wc = [];
	if(!kony.sync.isNull(pks.businessCode)){
		if(!kony.sync.isNull(pks.businessCode.value)){
			wc.key = "businessCode";
			wc.value = pks.businessCode.value;
		}
		else{
			wc.key = "businessCode";
			wc.value = pks.businessCode;
		}
	}else{
		sync.log.error("Primary Key businessCode not specified in " + opName + " an item in WTUser");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("businessCode",opName,"WTUser")));
		return;
	}
	kony.table.insert(wcs,wc);
	return true;
};

com.wt.WTUser.validateNull = function (valuestable,errorcallback){
	sync.log.trace("Entering com.wt.WTUser.validateNull function");
	return true;
};

com.wt.WTUser.validateNullInsert = function (valuestable,errorcallback){
	sync.log.trace("Entering com.wt.WTUser.validateNullInsert function");
	if(kony.sync.isNull(valuestable.employeeCode) || kony.sync.isEmptyString(valuestable.employeeCode)){
		sync.log.error("Mandatory attribute employeeCode is missing for the SyncObject WTUser.");
		errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeMandatoryAttribute,kony.sync.getErrorMessage(kony.sync.errorCodeMandatoryAttribute, "WTUser", "employeeCode")));
		return false;
	}
	if(kony.sync.isNull(valuestable.countryCode) || kony.sync.isEmptyString(valuestable.countryCode)){
		sync.log.error("Mandatory attribute countryCode is missing for the SyncObject WTUser.");
		errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeMandatoryAttribute,kony.sync.getErrorMessage(kony.sync.errorCodeMandatoryAttribute, "WTUser", "countryCode")));
		return false;
	}
	if(kony.sync.isNull(valuestable.businessCode) || kony.sync.isEmptyString(valuestable.businessCode)){
		sync.log.error("Mandatory attribute businessCode is missing for the SyncObject WTUser.");
		errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeMandatoryAttribute,kony.sync.getErrorMessage(kony.sync.errorCodeMandatoryAttribute, "WTUser", "businessCode")));
		return false;
	}
	return true;
};

com.wt.WTUser.getRelationshipMap = function(relationshipMap,valuestable){
	sync.log.trace("Entering com.wt.WTUser.getRelationshipMap function");
	var r1 = {};

	return relationshipMap;
};


com.wt.WTUser.checkPKValueTables = function (valuetables)	{
	var checkPksNotNullFlag = true;
	for(var i = 0; i < valuetables.length; i++)	{
		if(kony.sync.isNull(valuetables[i])){
			checkPksNotNullFlag = false;
			break;
		}
	}
	return checkPksNotNullFlag;
};

com.wt.WTUser.getTableName = function(){
	return "WTUser";
};




// **********************************End WTUser's helper methods************************