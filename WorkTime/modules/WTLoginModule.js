/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : UI Screen - Login - Methods
***********************************************************************/

/** Login screen - PreShow **/
function onPreShowLoginScreen(){
  	try {
      	printMessage("INSIDE onPreShowLoginScreen - ");
      	gblPopupMode								= "";
      	Login.lblWelcome.text						= getI18nString("common.message.welcome"); 
      	Login.lblVersion.text						= getI18nString("common.label.version") + " " + appConfig.appVersion;
        Login.lblUsername.text						= ""; 
        Login.btnCancel.text						= getI18nString("common.label.cancel");
        Login.btnOk.text							= getI18nString("common.label.ok");      	
      	Login.lbxItemList.popupTitle				= getI18nString("common.label.select");  
      	Login.lbxItemList.masterData				= [];
      	Login.lbxItemList.setVisibility(false);
      	Login.rdbItemList.masterData				= [];
      	Login.rdbItemList.setVisibility(false);
      	Login.lblMessage.text						= "";
      	Login.btnLeft.text							= getI18nString("common.label.cancel");
        Login.btnRight.text							= getI18nString("common.label.ok");
      	Login.flxPopupOverlayContainer.onClick		= doNothing;
      	Login.flxMessageOverlayContainer.onClick	= doNothing;
      	hideAllLoginScreenPopups();
    } catch(e) {
      	logException(e, "onPreShowLoginScreen", true);
	}
}

/** Login screen - PostShow **/
function onPostShowLoginScreen(){	
  	try {
  		printMessage("INSIDE onPostShowLoginScreen - ");
  		showLoading("common.message.loading");
  		initSyncModule(success_initSyncModule, failure_initSyncModule);
  	} catch(e) {
      	logException(e, "onPreShowLoginScreen", true);
	}
}

/** Sync initialization - Success callback  **/
function success_initSyncModule(response){
  	try {
  		printMessage("INSIDE success_initSyncModule - response - ", response);
      	setSyncLogLevel();
      	getLoggedUserFromDB();
  	} catch(e) {
      	logException(e, "success_initSyncModule", true);
	}
}	

/** Sync initialization - Failure callback  **/
function failure_initSyncModule(error){
	try {
  		printMessage("INSIDE failure_initSyncModule - error - ", error);
      	showMessagePopup("common.message.syncInitFailed", true, onClickMessagePopupOkButton);
  		dismissLoading();
   	} catch(e) {
      	logException(e, "failure_initSyncModule", true);
	}
}

/** Validate configured emails in device **/
function getDeviceConfiguredEmails(){
  	try {
      	printMessage("INSIDE getDeviceConfiguredEmails - ");
    	var configuredEmails 						= EmailChecker.getEmail(); 
      	if (configuredEmails !== null && configuredEmails.length > 0) {
          	var emailsCount							= configuredEmails.length;
          	if (emailsCount > 1){
              	var masterDataArray					= [];
                var aRecord							= null;
                var tempArray						= [];
                for (var i = 0; i < emailsCount; i++){
                    aRecord							= configuredEmails[i].trim();
                    masterDataArray.push([aRecord, aRecord]);
                    tempArray.push(aRecord);
                }
              	gblPopupMode						= WTConstant.EMAIL_SELECTION;	
              	showDataSelectionPopup(masterDataArray, tempArray, gblSelData.EMAIL);
              	dismissLoading();
            } else {
              	gblSelData.EMAIL					= configuredEmails[0].trim();
      	      	initDataSync("SYNC_INITIAL_DOWNLOAD", "DOWNLOAD_BASE_DATA", true, true);
            } 
        } else {
          	showMessagePopup("common.message.noEmailConfigured", true, onClickMessagePopupOkButton);
          	dismissLoading();
        } 
   	} catch(e) {
      	logException(e, "getDeviceConfiguredEmails", true);
	}
}

function showDataSelectionPopup(dataList, tempArray, selVal) {
  	try {
      	printMessage("INSIDE showDataSelectionPopup - dataList - ", dataList);
      	printMessage("INSIDE showDataSelectionPopup - tempArray - ", tempArray);
      	printMessage("INSIDE showDataSelectionPopup - selVal - ", selVal);
      	printMessage("INSIDE showDataSelectionPopup - gblPopupMode - ", gblPopupMode);
      	if (dataList.length > 3) {
          	Login.rdbItemList.masterData			= [];
          	Login.lbxItemList.masterData			= dataList;
          	Login.lbxItemList.setVisibility(true);
          	Login.rdbItemList.setVisibility(false);
          	if (selVal !== "" && tempArray.indexOf(selVal) !== -1) {
          		Login.lbxItemList.selectedKey		= selVal;
        	} 
        } else {
          	Login.lbxItemList.masterData			= [];
          	Login.rdbItemList.masterData			= dataList;
          	Login.rdbItemList.setVisibility(true);
          	Login.lbxItemList.setVisibility(false);
          	if (selVal !== "" && tempArray.indexOf(selVal) !== -1) {
          		Login.rdbItemList.selectedKey		= selVal;
        	} 
        }
      
      	if (gblPopupMode == WTConstant.EMAIL_SELECTION) {
          	Login.lblPopupTitle.text				= getI18nString("common.label.chooseUser");	
      		Login.lbxItemList.placeholder			= getI18nString("common.label.selectEmail");
        } else if (gblPopupMode == WTConstant.ICABS_SELECTION) {
          	Login.lblPopupTitle.text				= getI18nString("common.label.chooseICabs");
          	Login.lbxItemList.placeholder			= getI18nString("common.label.selectICabs");
        } else if (gblPopupMode == WTConstant.BRANCH_SELECTION) {
          	Login.lblPopupTitle.text				= getI18nString("common.label.chooseBranch");
          	Login.lbxItemList.placeholder			= getI18nString("common.label.selectBranch");
        } else {
          	Login.lblPopupTitle.text				= "";
          	Login.lbxItemList.placeholder			= "";
        }
      	
      	Login.flxPopupOverlayContainer.setVisibility(true);  
    } catch(e) {
      	logException(e, "showDataSelectionPopup", true);
	}
}

function onClickOkFromLoginScreenPopup() {
  	try {
      	printMessage("INSIDE onClickOkFromLoginScreenPopup - gblPopupMode - ", gblPopupMode);
      	if (gblPopupMode == WTConstant.EMAIL_SELECTION) {
          	onClickEmailPopupOkButton();
        } else if (gblPopupMode == WTConstant.ICABS_SELECTION) {
          	onClickICabsPopupOkButton();
        } else if (gblPopupMode == WTConstant.BRANCH_SELECTION) {
          	onClickBranchPopupOkButton();
        }
    } catch(e) {
      	logException(e, "onClickOkFromLoginScreenPopup", true);
	}
}

/** On click Emails popup Cancel button action **/
function onClickCancelFromLoginScreenPopup(){
  	try {
      	printMessage("INSIDE onClickCancelFromLoginScreenPopup - ");
      	showLoading("common.message.loading");
      	hideAllLoginScreenPopups();
      	dismissLoading();
      	exitApplication();
   	} catch(e) {
      	logException(e, "onClickCancelFromLoginScreenPopup", true);
	} 
}

function hideAllLoginScreenPopups() {
  	try {
      	printMessage("INSIDE hideAllLoginScreenPopups - ");
      	Login.flxPopupOverlayContainer.setVisibility(false);  
      	Login.flxMessageOverlayContainer.setVisibility(false);  
    } catch(e) {
      	logException(e, "hideAllLoginScreenPopups", true);
	}
}

/** On click Emails popup Ok button action **/
function onClickEmailPopupOkButton(){
  	try {
      	printMessage("INSIDE onClickEmailPopupOkButton - ");
      	var isValidationPass						= false;
      	if (Login.rdbItemList.isVisible === true) {
          	if (isEmpty(Login.rdbItemList.selectedKey)) {
              	showMessagePopup("common.message.selectEmail", true, dismissMessagePopup);
            } else {
              	isValidationPass					= true;
              	gblSelData.EMAIL					= Login.rdbItemList.selectedKey;
            }
        } else if (Login.lbxItemList.isVisible === true) {
          	if (isEmpty(Login.lbxItemList.selectedKey)) {
              	showMessagePopup("common.message.selectEmail", true, dismissMessagePopup);
            } else {
              	isValidationPass					= true;
              	gblSelData.EMAIL					= Login.lbxItemList.selectedKey;
            }
        }
      	
      	if (isValidationPass === true) {
          	showLoading("common.message.loading");
            hideAllLoginScreenPopups();
          	initDataSync("SYNC_INITIAL_DOWNLOAD", "DOWNLOAD_BASE_DATA", true, true);
        } else {
          	printMessage("INSIDE onClickEmailPopupOkButton - validation failed");
        }
    } catch(e) {
      	logException(e, "onClickEmailPopupOkButton", true);
	}        
}

function onClickICabsPopupOkButton() {
  	try {
      	printMessage("INSIDE onClickICabsPopupOkButton - ");
      	var isValidationPass						= false;
      	var selKey									= "";
      	var keySplits								= null;
      	if (Login.rdbItemList.isVisible === true) {
          	if (isEmpty(Login.rdbItemList.selectedKey)) {
              	showMessagePopup("common.message.selectICab", true, dismissMessagePopup);
            } else {
              	isValidationPass					= true;
              	selKey								= Login.rdbItemList.selectedKey;
              	keySplits							= selKey.split("_");
              	gblSelData.COUNTRY_CODE				= keySplits[0];
      			gblSelData.BUSINESS_CODE			= keySplits[1];
              	gblSelData.SEL_ICABS_KEY			= selKey;
            }
        } else if (Login.lbxItemList.isVisible === true) {
          	if (isEmpty(Login.lbxItemList.selectedKey)) {
              	showMessagePopup("common.message.selectICab", true, dismissMessagePopup);
            } else {
              	isValidationPass					= true;
               	selKey								= Login.lbxItemList.selectedKey;
               	keySplits							= selKey.split("_");
              	gblSelData.COUNTRY_CODE				= keySplits[0];
      			gblSelData.BUSINESS_CODE			= keySplits[1];
              	gblSelData.SEL_ICABS_KEY			= selKey;
            }
        }
      	
      	if (isValidationPass === true) {
          	showLoading("common.message.loading");
          	hideAllLoginScreenPopups();
          	initDataSync("SYNC_INITIAL_DOWNLOAD", "DOWNLOAD_EMPLOYEE", true, true);
        } else {
          	printMessage("INSIDE onClickICabsPopupOkButton - validation failed");
        }
    } catch(e) {
      	logException(e, "onClickICabsPopupOkButton", true);
	}
}

function onClickBranchPopupOkButton(){
  	try {
      	printMessage("INSIDE onClickBranchPopupOkButton - ");
      	var isValidationPass						= false;
      	var selKeyName								= "";
      	if (Login.rdbItemList.isVisible === true) {
          	if (isEmpty(Login.rdbItemList.selectedKey)) {
              	showMessagePopup("common.message.selectBranch", true, dismissMessagePopup);
            } else {
              	isValidationPass					= true;
              	gblSelData.BRANCH_NUMBER			= Login.rdbItemList.selectedKey;
      			selKeyName							= Login.rdbItemList.selectedKeyValue[1];
              	gblSelData.BRANCH_NAME				= selKeyName.split(/ - (.+)/)[1];
            }
        } else if (Login.lbxItemList.isVisible === true) {
          	if (isEmpty(Login.lbxItemList.selectedKey)) {
              	showMessagePopup("common.message.selectBranch", true, dismissMessagePopup);
            } else {
              	isValidationPass					= true;
              	gblSelData.BRANCH_NUMBER			= Login.lbxItemList.selectedKey;
              	selKeyName							= Login.lbxItemList.selectedKeyValue[1];
      			gblSelData.BRANCH_NAME				= selKeyName.split(/ - (.+)/)[1];
            }
        }
      	
      	if (isValidationPass === true) {
          	showLoading("common.message.loading");
			hideAllLoginScreenPopups();
          	gblSelData.IS_LOGGED_IN					= true;
          	saveLoggedUserInDB(gblSelData);
        } else {
          	printMessage("INSIDE onClickBranchPopupOkButton - validation failed");
        }
    } catch(e) {
      	logException(e, "onClickBranchPopupOkButton", true);
	}        
}
