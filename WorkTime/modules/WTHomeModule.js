/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : UI Screen - Home - Methods
***********************************************************************/

/** Show home screen **/
function showHomeScreen(){	
  	try {
      	printMessage("INSIDE showHomeScreen - ");
		Home.show();
   	} catch(e) {
      	logException(e, "showHomeScreen", true);
	}
}

/** Home screen - PreShow **/
function onPreShowHomeScreen(){ 
  	try {
      	printMessage("INSIDE onPreShowHomeScreen - ");
        Home.lblStartWork.text								= getI18nString("common.label.startWork");
        Home.lblStopWork.text								= getI18nString("common.label.stopWork");
        Home.lblStartOvertime.text							= getI18nString("common.label.startOvertime");
        Home.lblStopOvertime.text							= getI18nString("common.label.endOvertime");  	
        Home.lblPopupTitle.text								= getI18nString("popup.label.workTimeSummary");
      	Home.btnOk.text										= getI18nString("common.label.ok");
        Home.lblProgress.text								= "";
        Home.lblTimer.text									= "";
      	Home.lblMessage.text								= "";
      	Home.btnLeft.text									= getI18nString("common.label.cancel");
        Home.btnRight.text									= getI18nString("common.label.ok");
      	Home.lblStartNonServiceEvnt.text					= getI18nString("common.label.start");
      	Home.lblStartNonServiceEvnt.skin					= "lblGreenBoldSkin";
      	Home.lbxNonServiceEvent.placeholder					= getI18nString("common.label.selectNonServiceEventType");
      	Home.lbxNonServiceEvent.popuptitle					= getI18nString("common.label.select");
      
        Home.flxTimerContainer.setVisibility(false);
        Home.flxPopupOverlayContainer.setVisibility(false);
      	Home.flxPopupOverlayContainer.onClick				= doNothing;
      	Home.flxMessageOverlayContainer.onClick				= doNothing;
      
      	printMessage("INSIDE onPreShowHomeScreen - gblNTAttendanceObject - ", gblNTAttendanceObject);
      	printMessage("INSIDE onPreShowHomeScreen - gblOTAttendanceObject - ", gblOTAttendanceObject);
      	printMessage("INSIDE onPreShowHomeScreen - gblNSEAttendanceObject - ", gblNSEAttendanceObject);
      
      	if (gblAppMode == WTConstant.APP_MODE_UK) {
          	/** UK Mode - At a time only one time tracker is active (Means Normal Time Or Overtime Or Non Service Event) **/
          	if (isEmpty(gblNTAttendanceObject) && isEmpty(gblOTAttendanceObject) && isEmpty(gblNSEAttendanceObject)) {
              	/** No activity is running **/
                switchButtonAccessibility("", false);
              	gblAttendanceMode							= "";
            } else if (!isEmpty(gblNTAttendanceObject)) {
              	/** Normal Time is running **/
                switchButtonAccessibility(WTConstant.NORMAL_TIME, true);
              	gblAttendanceMode							= WTConstant.NORMAL_TIME;
            } else if (!isEmpty(gblOTAttendanceObject)) {
              	/** Overtime is running **/
                switchButtonAccessibility(WTConstant.OVER_TIME, true);
              	gblAttendanceMode							= WTConstant.OVER_TIME;
            } else {
              	/** Non Service Event is running **/
                switchButtonAccessibility(WTConstant.NSE_TIME, true);
              	gblAttendanceMode							= WTConstant.NSE_TIME;
            }
        } else {
          	/** IE Mode - Non Service or Overtime will exist only if the Normal Time is active. So Normal Time is empty means no activity is running **/
          	if (isEmpty(gblNTAttendanceObject)) {
                /** No activity is running **/
                switchButtonAccessibility("", false);
              	gblAttendanceMode							= "";
            } else if (!isEmpty(gblNSEAttendanceObject)) {
              	/** Non Service Event is running along with Normal Time **/
                switchButtonAccessibility(WTConstant.NSE_TIME, true);
              	gblAttendanceMode							= WTConstant.NSE_TIME;
            } else if (!isEmpty(gblOTAttendanceObject)) {
              	/** Overtime is running along with Normal Time **/
                switchButtonAccessibility(WTConstant.OVER_TIME, true);
              	gblAttendanceMode							= WTConstant.OVER_TIME;
            } else {
              	/** Only Normal Time is running **/
                switchButtonAccessibility(WTConstant.NORMAL_TIME, true);
              	gblAttendanceMode							= WTConstant.NORMAL_TIME;
            }
        }
      
      	if (gblAttendanceMode == WTConstant.NORMAL_TIME) {
          	Home.flxTimerContainer.skin						= "flxGreenBgSkin";
        } else if (gblAttendanceMode == WTConstant.OVER_TIME) {
          	Home.flxTimerContainer.skin						= "flxRedBgSkin";
        } else {
          	Home.flxTimerContainer.skin						= "flxYellowBgSkin";
        }

      	hideAllHomeScreenPopups();
   	} catch(e) {
      	logException(e, "onPreShowHomeScreen", true);
	}
}

/** Home screen - PostShow **/
function onPostShowHomeScreen(){
  	try {
      	printMessage("INSIDE onPostShowHomeScreen - ");
      	if (gblAppMode == WTConstant.APP_MODE_UK) {
          	if (!isEmpty(gblNTAttendanceObject)) {
            	applyWTRunningStatusOnScreen(gblNTAttendanceObject);
          	} else if (!isEmpty(gblOTAttendanceObject)) {
            	applyWTRunningStatusOnScreen(gblOTAttendanceObject);
          	} else if (!isEmpty(gblNSEAttendanceObject)) {
              	applyWTRunningStatusOnScreen(gblNSEAttendanceObject);
            }
        } else {
          	if (!isEmpty(gblNTAttendanceObject)) {
            	applyWTRunningStatusOnScreen(gblNTAttendanceObject);
              
              	if (!isEmpty(gblOTAttendanceObject)) {
                    applyWTRunningStatusOnScreen(gblOTAttendanceObject);
                } 
              
              	if (!isEmpty(gblNSEAttendanceObject)) {
                    applyWTRunningStatusOnScreen(gblNSEAttendanceObject);
                }  
          	}
        }
      	
  		refreshSettingsUI(); 
      	dismissLoading();
  	} catch(e) {
      	logException(e, "onPostShowHomeScreen", true);
	}
}

/** To configure home screen based on current running mode **/
function applyWTRunningStatusOnScreen(record){
  	try {
      	printMessage("INSIDE applyWTRunningStatusOnScreen - record - ", record);
        if (!isEmpty(record)) {
            var workStartedTimeStamp						= getTimeFromTimestamp(record.startTimestamp);
            var workStoppedTimeStamp						= 0;
          	if (record.attendanceType == WTConstant.NORMAL_TIME) {
              	Home.lblStartTimeValue.text					= (workStartedTimeStamp.h + ":" + workStartedTimeStamp.m + ":" + workStartedTimeStamp.s);
            } else if (record.attendanceType == WTConstant.OVER_TIME) {
              	Home.lblStartOvertimeTimeValue.text			= (workStartedTimeStamp.h + ":" + workStartedTimeStamp.m + ":" + workStartedTimeStamp.s);
            } 

            if (record.endTimestamp === 0){
              	switchButtonAccessibility(record.attendanceType, true); 
              	showWorkTimerUpdates();
            } else {
              	switchButtonAccessibility(record.attendanceType, false); 
                workStoppedTimeStamp						= getTimeFromTimestamp(record.endTimestamp);
              	if (record.attendanceType == WTConstant.NORMAL_TIME) {
                  	Home.lblStopTimeValue.text				= (workStoppedTimeStamp.h + ":" + workStoppedTimeStamp.m + ":" +workStoppedTimeStamp.s);
                } else if (record.attendanceType == WTConstant.OVER_TIME) {
                  	Home.lblStopOverTimeValue.text			= (workStoppedTimeStamp.h + ":" + workStoppedTimeStamp.m + ":" + workStoppedTimeStamp.s);
                } 
            }
        }
 	} catch(e) {
      	logException(e, "applyWTRunningStatusOnScreen", true);
	}
}

function resetTimeOnButtons(identifier) {
  	try {
      	printMessage("INSIDE resetTimeOnButtons - identifier - ", identifier);
      	if (identifier == "BOTH") {
          	Home.lblStartTimeValue.text						= "";
            Home.lblStartOvertimeTimeValue.text				= "";	
            Home.lblStopTimeValue.text						= "";	
            Home.lblStopOverTimeValue.text					= "";
        } else if (identifier == "NORMAL") {
          	Home.lblStartTimeValue.text						= "";
            Home.lblStopTimeValue.text						= "";	
        } else {
          	Home.lblStartOvertimeTimeValue.text				= "";	
            Home.lblStopOverTimeValue.text					= "";
        }
    } catch(e) {
      	logException(e, "resetTimeOnButtons", true);
	}
}

function hideAllHomeScreenPopups() {
  	try {
      	printMessage("INSIDE hideAllHomeScreenPopups - ");
      	Home.flxPopupOverlayContainer.setVisibility(false);  
      	Home.flxMessageOverlayContainer.setVisibility(false);  
    } catch(e) {
      	logException(e, "hideAllHomeScreenPopups", true);
	}
}

/** Start day button click **/
function onClickStartDay(){
  	try {
      	printMessage("INSIDE onClickStartDay");
        showLoading("common.message.loading");	
      	gblAttendanceMode									= WTConstant.NORMAL_TIME;
      	switchButtonAccessibility(WTConstant.NORMAL_TIME, true);
      
        var currentTimeStamp								= getCurrentTimestamp(); 
        var currentTimeSplits								= getTimeFromTimestamp(currentTimeStamp);
        Home.lblStartTimeValue.text							= currentTimeSplits.h + ":" + currentTimeSplits.m + ":" + currentTimeSplits.s;
        Home.lblStopTimeValue.text							= "";
      
      	if (gblAppMode == WTConstant.APP_MODE_UK) {
       		var isPreWorkTimeStillActive					= isWorkTimeRunning();
        	if (isPreWorkTimeStillActive === true) {
          		/** Switching directly from Overtime to normal time. Means without stopping overtime, starting normal time **/
            	Home.lblStopOverTimeValue.text				= currentTimeSplits.h + ":" + currentTimeSplits.m + ":" + currentTimeSplits.s;
        	}
        }
      	
      	gblNTAttendanceObject								= null;
      	gblNTAttendanceObject								= new Attendance();
      	gblNTAttendanceObject.attendanceType				= WTConstant.NORMAL_TIME;
        gblNTAttendanceObject.startDateStr					= getToday();
        gblNTAttendanceObject.startTimestamp				= currentTimeStamp; 
        gblNTAttendanceObject.startTimestampStr				= getTimeInGMT();
      
        captureCurrentLocation(successLocationCapture, errorLocationCapture);
  	} catch(e) {
      	logException(e, "onClickStartDay", true);
	}
}

/** Start overtime button click **/
function onClickStartOvertime(){
  	try {
      	printMessage("INSIDE onClickStartOvertime");
        showLoading("common.message.loading");
      	gblAttendanceMode									= WTConstant.OVER_TIME;
      	switchButtonAccessibility(WTConstant.OVER_TIME, true);
      
        var currentTimeStamp								= getCurrentTimestamp(); 
        var currentTimeSplits								= getTimeFromTimestamp(currentTimeStamp);
        Home.lblStartOvertimeTimeValue.text					= currentTimeSplits.h + ":" + currentTimeSplits.m + ":" + currentTimeSplits.s;
        Home.lblStopOverTimeValue.text						= "";
      	
      	if (gblAppMode == WTConstant.APP_MODE_UK) {
            var isPreWorkTimeStillActive					= isWorkTimeRunning();
            if (isPreWorkTimeStillActive === true) {
                /** Switching directly from normal time to overtime. Means without stopping normal time, starting overtime **/
                Home.lblStopTimeValue.text					= currentTimeSplits.h + ":" + currentTimeSplits.m + ":" + currentTimeSplits.s;
            } 
        }
      
      	Home.lbxNonServiceEvent.selectedKey					= "";
      	
      	gblOTAttendanceObject								= null;
        gblOTAttendanceObject								= new Attendance();
      	gblOTAttendanceObject.attendanceType				= WTConstant.OVER_TIME;
        gblOTAttendanceObject.startDateStr					= getToday();
        gblOTAttendanceObject.startTimestamp				= currentTimeStamp;
        gblOTAttendanceObject.startTimestampStr				= getTimeInGMT();
      
        captureCurrentLocation(successLocationCapture, errorLocationCapture);
   	} catch(e) {
      	logException(e, "onClickStartOvertime", true);
	}
}

function onClickNonServiceEventBtn() {
  	try {
      	printMessage("INSIDE onClickNonServiceEventBtn");
      
      	if (Home.lblStartNonServiceEvnt.text == getI18nString("common.label.start")) {
          	startNonServiceEvent();
        } else {
          	endNonServiceEvent();
        }
    } catch(e) {
      	logException(e, "onClickNonServiceEventBtn", true);
	}
}

function startNonServiceEvent() {
  	try {
      	printMessage("INSIDE startNonServiceEvent");
      	var selNSEOption									= Home.lbxNonServiceEvent.selectedKey;
      	printMessage("INSIDE startNonServiceEvent - selNSEOption - ", selNSEOption);
      	if (isEmpty(selNSEOption)) {
          	showMessagePopup("common.message.selectNonServiceEventType", true, dismissMessagePopup);
        } else {
          	showLoading("common.message.loading");
            gblAttendanceMode								= WTConstant.NSE_TIME;
            switchButtonAccessibility(WTConstant.NSE_TIME, true);
          
          	var currentTimeStamp							= getCurrentTimestamp(); 
      	
      		if (gblAppMode == WTConstant.APP_MODE_UK) {
            	var isPreWorkTimeStillActive				= isWorkTimeRunning();
            	if (isPreWorkTimeStillActive === true) {
                  	var currentTimeSplits					= getTimeFromTimestamp(currentTimeStamp);
                  	if (!isEmpty(gblNTAttendanceObject)) {
                      	/** Switching directly from Normal Time to Non Service Event. Means without stopping Normal Time, starting Non Service Event **/
                      	Home.lblStopTimeValue.text			= currentTimeSplits.h + ":" + currentTimeSplits.m + ":" + currentTimeSplits.s;
                    } else if (!isEmpty(gblOTAttendanceObject)) {
                      	/** Switching directly from Overtime to Non Service Event. Means without stopping Overtime, starting Non Service Event **/
                      	Home.lblStopOverTimeValue.text		= currentTimeSplits.h + ":" + currentTimeSplits.m + ":" + currentTimeSplits.s;
                    }
            	} 
        	}
		
            gblNSEAttendanceObject							= null;
            gblNSEAttendanceObject							= new Attendance();
            gblNSEAttendanceObject.attendanceType			= Home.lbxNonServiceEvent.selectedKeyValue[1];
          	gblNSEAttendanceObject.nonServiceEventTypeCode	= selNSEOption;
            gblNSEAttendanceObject.startDateStr				= getToday();
            gblNSEAttendanceObject.startTimestamp			= currentTimeStamp;
            gblNSEAttendanceObject.startTimestampStr		= getTimeInGMT();
			printMessage("INSIDE startNonServiceEvent - gblNSEAttendanceObject - ", gblNSEAttendanceObject);
          
            captureCurrentLocation(successLocationCapture, errorLocationCapture);
        }
  	} catch(e) {
      	logException(e, "startNonServiceEvent", true);
	}
}

/** End day button click **/
function onClickEndDay(){  	
  	try {
      	printMessage("INSIDE onClickEndDay");
      	if (!isEmpty(gblNTAttendanceObject)) {
            showLoading("common.message.loading");  	
          	gblAttendanceMode								= WTConstant.NORMAL_TIME;
          	Home.lbxNonServiceEvent.selectedKey				= "";
          	switchButtonAccessibility(WTConstant.NORMAL_TIME, false);
          	executeWorkTimeLogic();
        }
   	} catch(e) {
      	logException(e, "onClickEndDay", true);
	}
}

/** End overtime button click **/
function onClickEndOvertime(){
  	try {
      	printMessage("INSIDE onClickEndOvertime");
      	if (!isEmpty(gblOTAttendanceObject)) {
            showLoading("common.message.loading");
          	gblAttendanceMode								= WTConstant.OVER_TIME;
          	switchButtonAccessibility(WTConstant.OVER_TIME, false);
          	executeWorkTimeLogic();
        }
   	} catch(e) {
      	logException(e, "onClickEndOvertime", true);
	}
}

function endNonServiceEvent(){
  	try {
      	printMessage("INSIDE endNonServiceEvent");
      	if (!isEmpty(gblNSEAttendanceObject)) {
            showLoading("common.message.loading");
          	gblAttendanceMode								= WTConstant.NSE_TIME;
          	Home.lbxNonServiceEvent.selectedKey				= "";
          	switchButtonAccessibility(WTConstant.NSE_TIME, false);
          	executeWorkTimeLogic();
        }
   	} catch(e) {
      	logException(e, "endNonServiceEvent", true);
	}
}

function onSuccessUploadWorkTime(result){
  	try {
      	printMessage("INSIDE onSuccessUploadWorkTime");
      	deleteOldSyncedWorkAttendances();
      	deleteOldSupportViewEntries();
        dismissLoading();
   	} catch(e) {
      	logException(e, "onSuccessUploadWorkTime", true);
	}
}

function onErrorUploadWorkTime(error){
  	try {
      	printMessage("INSIDE onErrorUploadWorkTime");
        dismissLoading();
   	} catch(e) {
      	logException(e, "onErrorUploadWorkTime", true);
	}
}

/** Work time logic implemented here **/
function executeWorkTimeLogic(){
  	try {
      	printMessage("INSIDE executeWorkTimeLogic - gblAttendanceMode - ", gblAttendanceMode);
      	printMessage("INSIDE executeWorkTimeLogic - gblAppMode - ", gblAppMode);
      	if (gblAppMode == WTConstant.APP_MODE_UK) {
        	if (gblAttendanceMode != WTConstant.NSE_TIME) {
              	hideWorkTimerUpdates();
            }  		
        } else {
          	if (gblAttendanceMode != WTConstant.NORMAL_TIME) {
              	hideWorkTimerUpdates();
            }
        }
      	
        ////stopTimer();
        ////Trace.stopService();
        var currentTimeStamp								= getCurrentTimestamp();
        var currentTimeSplits								= getTimeFromTimestamp(currentTimeStamp);
      	if (gblAttendanceMode == WTConstant.OVER_TIME) {
            Home.lblStopOverTimeValue.text					= currentTimeSplits.h + ":" + currentTimeSplits.m + ":" + currentTimeSplits.s;
          	gblOTAttendanceObject.endDateStr				= getToday();
        	gblOTAttendanceObject.endTimestamp				= currentTimeStamp;
        	gblOTAttendanceObject.endTimestampStr			= getTimeInGMT(currentTimeStamp);
        } else if (gblAttendanceMode == WTConstant.NORMAL_TIME) {
            Home.lblStopTimeValue.text						= currentTimeSplits.h + ":" + currentTimeSplits.m + ":" + currentTimeSplits.s;
          	gblNTAttendanceObject.endDateStr				= getToday();
        	gblNTAttendanceObject.endTimestamp				= currentTimeStamp;
        	gblNTAttendanceObject.endTimestampStr			= getTimeInGMT(currentTimeStamp);
        } else {
          	gblNSEAttendanceObject.endDateStr				= getToday();
        	gblNSEAttendanceObject.endTimestamp				= currentTimeStamp;
        	gblNSEAttendanceObject.endTimestampStr			= getTimeInGMT(currentTimeStamp);
        }
      	captureCurrentLocation(successLocationCaptureAlt, errorLocationCaptureAlt);
 	} catch(e) {
      	logException(e, "executeWorkTimeLogic", true);
	}
}

/** Enabling/Disabling overlay message popup ok button **/
function setFlxOverlayMsgOkButtonStyle(enabled){
  	try {
      	printMessage("INSIDE setFlxOverlayMsgOkButtonStyle");
        Home.btnOk.setEnabled(enabled);
        Home.btnOk.skin										= (enabled === true) ? "btnNormalSkin" : "btnNormalDisabledSkin";
        Home.btnOk.focusSkin								= (enabled === true) ? "btnNormalSkin" : "btnNormalDisabledSkin";
  	} catch(e) {
      	logException(e, "setFlxOverlayMsgOkButtonStyle", true);
	}
}

/** To check any work time is currently active **/
function isWorkTimeRunning(){
  	try {
      	printMessage("INSIDE isWorkTimeRunning - gblSelData - ", gblSelData);
      	var isActive										= gblSelData.IS_WORK_TIME_ACTIVE;
      	printMessage("INSIDE isWorkTimeRunning - isActive - ", isActive);
  		return isActive;
   	} catch(e) {
      	logException(e, "isWorkTimeRunning", true);
	}
}

/** On click OK button action from work summary overlay popup **/
function onClickPopupOk(){
  	try {
      	printMessage("INSIDE onClickPopupOk");
        Home.flxPopupOverlayContainer.setVisibility(false);
   	} catch(e) {
      	logException(e, "onClickPopupOk", true);
	}
}

/** To switch button accessibility based on current status **/
function switchButtonAccessibility(attendanceTypeStr, isActive){
  	try {
      	printMessage("INSIDE switchButtonAccessibility - attendanceTypeStr - ", attendanceTypeStr);
      	printMessage("INSIDE switchButtonAccessibility - isActive - ", isActive);
        
      	if (gblAppMode == WTConstant.APP_MODE_UK) {
          	/** User can start 'Start Work' or 'Start Overtime' or 'Non Service Event' without any condition **/
          	if (attendanceTypeStr === "") {
              	Home.flxStartWorkInnerContainer.setEnabled(true);
              	Home.flxStartWorkInnerContainer.skin			= "flxItemEnabledSkin";
              	Home.flxStopWorkInnerContainer.setEnabled(false);
              	Home.flxStopWorkInnerContainer.skin				= "flxItemDisabledSkin";
              
              	Home.flxStartOverTimeInnerContainer.setEnabled(true);      
              	Home.flxStartOverTimeInnerContainer.skin		= "flxItemEnabledSkin";
              	Home.flxStopOverTimeInnerContainer.setEnabled(false);
              	Home.flxStopOverTimeInnerContainer.skin			= "flxItemDisabledSkin";
              
          		Home.flxNonServiceEvntBtn.setEnabled(true);
          		Home.flxNonServiceEvntBtn.skin					= "flxItemEnabledSkin";
              	Home.flxNonServiceEventOverlay.setVisibility(false);
            } else if (attendanceTypeStr == WTConstant.NORMAL_TIME) {
              	if (isActive === true) {
                  	Home.flxTimerContainer.skin					= "flxGreenBgSkin";
                  
                  	Home.flxStartWorkInnerContainer.setEnabled(false);
              		Home.flxStartWorkInnerContainer.skin		= "flxItemDisabledSkin";
              		Home.flxStopWorkInnerContainer.setEnabled(true);
              		Home.flxStopWorkInnerContainer.skin			= "flxItemEnabledSkin";
              
              		Home.flxStartOverTimeInnerContainer.setEnabled(true);      
              		Home.flxStartOverTimeInnerContainer.skin	= "flxItemEnabledSkin";
              		Home.flxStopOverTimeInnerContainer.setEnabled(false);
              		Home.flxStopOverTimeInnerContainer.skin		= "flxItemDisabledSkin";
                  
          			Home.flxNonServiceEvntBtn.setEnabled(true);
          			Home.flxNonServiceEvntBtn.skin				= "flxItemEnabledSkin";
                  	Home.flxNonServiceEventOverlay.setVisibility(false);
                } else {
                  	Home.flxStartWorkInnerContainer.setEnabled(true);
              		Home.flxStartWorkInnerContainer.skin		= "flxItemEnabledSkin";
              		Home.flxStopWorkInnerContainer.setEnabled(false);
              		Home.flxStopWorkInnerContainer.skin			= "flxItemDisabledSkin";
              
              		Home.flxStartOverTimeInnerContainer.setEnabled(true);      
              		Home.flxStartOverTimeInnerContainer.skin	= "flxItemEnabledSkin";
              		Home.flxStopOverTimeInnerContainer.setEnabled(false);
              		Home.flxStopOverTimeInnerContainer.skin		= "flxItemDisabledSkin";
                  
          			Home.flxNonServiceEvntBtn.setEnabled(true);
          			Home.flxNonServiceEvntBtn.skin				= "flxItemEnabledSkin";
                  	Home.flxNonServiceEventOverlay.setVisibility(false);
                }
            } else if (attendanceTypeStr == WTConstant.OVER_TIME) {
              	if (isActive === true) {
                  	Home.flxTimerContainer.skin					= "flxRedBgSkin";
                  
                  	Home.flxStartWorkInnerContainer.setEnabled(true);
              		Home.flxStartWorkInnerContainer.skin		= "flxItemEnabledSkin";
              		Home.flxStopWorkInnerContainer.setEnabled(false);
              		Home.flxStopWorkInnerContainer.skin			= "flxItemDisabledSkin";
              
              		Home.flxStartOverTimeInnerContainer.setEnabled(false);      
              		Home.flxStartOverTimeInnerContainer.skin	= "flxItemDisabledSkin";
              		Home.flxStopOverTimeInnerContainer.setEnabled(true);
              		Home.flxStopOverTimeInnerContainer.skin		= "flxItemEnabledSkin";
                  
          			Home.flxNonServiceEvntBtn.setEnabled(true);
          			Home.flxNonServiceEvntBtn.skin				= "flxItemEnabledSkin";
                  	Home.flxNonServiceEventOverlay.setVisibility(false);
                } else {
                  	Home.flxStartWorkInnerContainer.setEnabled(true);
              		Home.flxStartWorkInnerContainer.skin		= "flxItemEnabledSkin";
              		Home.flxStopWorkInnerContainer.setEnabled(false);
              		Home.flxStopWorkInnerContainer.skin			= "flxItemDisabledSkin";
              
              		Home.flxStartOverTimeInnerContainer.setEnabled(true);      
              		Home.flxStartOverTimeInnerContainer.skin	= "flxItemEnabledSkin";
              		Home.flxStopOverTimeInnerContainer.setEnabled(false);
              		Home.flxStopOverTimeInnerContainer.skin		= "flxItemDisabledSkin";
                  
          			Home.flxNonServiceEvntBtn.setEnabled(true);
          			Home.flxNonServiceEvntBtn.skin				= "flxItemEnabledSkin";
                  	Home.flxNonServiceEventOverlay.setVisibility(false);
                }
            } else {
             	if (isActive === true) {
                  	Home.flxTimerContainer.skin					= "flxYellowBgSkin";
                  
                  	Home.flxStartWorkInnerContainer.setEnabled(true);
              		Home.flxStartWorkInnerContainer.skin		= "flxItemEnabledSkin";
              		Home.flxStopWorkInnerContainer.setEnabled(false);
              		Home.flxStopWorkInnerContainer.skin			= "flxItemDisabledSkin";
              
              		Home.flxStartOverTimeInnerContainer.setEnabled(true);      
              		Home.flxStartOverTimeInnerContainer.skin	= "flxItemEnabledSkin";
              		Home.flxStopOverTimeInnerContainer.setEnabled(false);
              		Home.flxStopOverTimeInnerContainer.skin		= "flxItemDisabledSkin";
                  
          			Home.flxNonServiceEvntBtn.setEnabled(true);
          			Home.flxNonServiceEvntBtn.skin				= "flxItemEnabledSkin";
                  	Home.flxNonServiceEventOverlay.setVisibility(true);
                  
                  	Home.lblStartNonServiceEvnt.text			= getI18nString("common.label.stop");
      				Home.lblStartNonServiceEvnt.skin			= "lblRedBoldSkin";
                } else {
                  	Home.flxStartWorkInnerContainer.setEnabled(true);
              		Home.flxStartWorkInnerContainer.skin		= "flxItemEnabledSkin";
              		Home.flxStopWorkInnerContainer.setEnabled(false);
              		Home.flxStopWorkInnerContainer.skin			= "flxItemDisabledSkin";
              
              		Home.flxStartOverTimeInnerContainer.setEnabled(true);      
              		Home.flxStartOverTimeInnerContainer.skin	= "flxItemEnabledSkin";
              		Home.flxStopOverTimeInnerContainer.setEnabled(false);
              		Home.flxStopOverTimeInnerContainer.skin		= "flxItemDisabledSkin";
                  
          			Home.flxNonServiceEvntBtn.setEnabled(true);
          			Home.flxNonServiceEvntBtn.skin				= "flxItemEnabledSkin";
                  	Home.flxNonServiceEventOverlay.setVisibility(false);
                  
                  	Home.lblStartNonServiceEvnt.text			= getI18nString("common.label.start");
      				Home.lblStartNonServiceEvnt.skin			= "lblGreenBoldSkin";
                }
            }
        } else {
          	/** User can start 'Start Overtime' only if 'Start Work' is active **/
          	if (attendanceTypeStr === "") {
              	Home.flxStartWorkInnerContainer.setEnabled(true);
              	Home.flxStartWorkInnerContainer.skin			= "flxItemEnabledSkin";
              	Home.flxStopWorkInnerContainer.setEnabled(false);
              	Home.flxStopWorkInnerContainer.skin				= "flxItemDisabledSkin";
              
              	Home.flxStartOverTimeInnerContainer.setEnabled(false);      
              	Home.flxStartOverTimeInnerContainer.skin		= "flxItemDisabledSkin";
              	Home.flxStopOverTimeInnerContainer.setEnabled(false);
              	Home.flxStopOverTimeInnerContainer.skin			= "flxItemDisabledSkin";
              
          		Home.flxNonServiceEvntBtn.setEnabled(false);
          		Home.flxNonServiceEvntBtn.skin					= "flxItemDisabledSkin";
              	Home.flxNonServiceEventOverlay.setVisibility(true);
            } else if (attendanceTypeStr == WTConstant.NORMAL_TIME) {
              	if (isActive === true) {
                  	Home.flxTimerContainer.skin					= "flxGreenBgSkin";
                  
                  	Home.flxStartWorkInnerContainer.setEnabled(false);
              		Home.flxStartWorkInnerContainer.skin		= "flxItemDisabledSkin";
              		Home.flxStopWorkInnerContainer.setEnabled(true);
              		Home.flxStopWorkInnerContainer.skin			= "flxItemEnabledSkin";
              
              		Home.flxStartOverTimeInnerContainer.setEnabled(true);      
              		Home.flxStartOverTimeInnerContainer.skin	= "flxItemEnabledSkin";
              		Home.flxStopOverTimeInnerContainer.setEnabled(false);
              		Home.flxStopOverTimeInnerContainer.skin		= "flxItemDisabledSkin";
                  
          			Home.flxNonServiceEvntBtn.setEnabled(true);
          			Home.flxNonServiceEvntBtn.skin				= "flxItemEnabledSkin";
                  	Home.flxNonServiceEventOverlay.setVisibility(false);
                } else {
                  	Home.flxStartWorkInnerContainer.setEnabled(true);
              		Home.flxStartWorkInnerContainer.skin		= "flxItemEnabledSkin";
              		Home.flxStopWorkInnerContainer.setEnabled(false);
              		Home.flxStopWorkInnerContainer.skin			= "flxItemDisabledSkin";
              
              		Home.flxStartOverTimeInnerContainer.setEnabled(false);      
              		Home.flxStartOverTimeInnerContainer.skin	= "flxItemDisabledSkin";
              		Home.flxStopOverTimeInnerContainer.setEnabled(false);
              		Home.flxStopOverTimeInnerContainer.skin		= "flxItemDisabledSkin";
                  
          			Home.flxNonServiceEvntBtn.setEnabled(false);
          			Home.flxNonServiceEvntBtn.skin				= "flxItemDisabledSkin";
                  	Home.flxNonServiceEventOverlay.setVisibility(true);
                }
            } else if (attendanceTypeStr == WTConstant.OVER_TIME) {
              	if (isActive === true) {
                  	Home.flxTimerContainer.skin					= "flxRedBgSkin";
                  
                  	Home.flxStartWorkInnerContainer.setEnabled(false);
              		Home.flxStartWorkInnerContainer.skin		= "flxItemDisabledSkin";
              		Home.flxStopWorkInnerContainer.setEnabled(false);
              		Home.flxStopWorkInnerContainer.skin			= "flxItemDisabledSkin";
                  
              		Home.flxStartOverTimeInnerContainer.setEnabled(false);      
              		Home.flxStartOverTimeInnerContainer.skin	= "flxItemDisabledSkin";
              		Home.flxStopOverTimeInnerContainer.setEnabled(true);
              		Home.flxStopOverTimeInnerContainer.skin		= "flxItemEnabledSkin";
                  
          			Home.flxNonServiceEvntBtn.setEnabled(false);
          			Home.flxNonServiceEvntBtn.skin				= "flxItemDisabledSkin";
                  	Home.flxNonServiceEventOverlay.setVisibility(true);
                } else {
                  	Home.flxTimerContainer.skin					= "flxGreenBgSkin";
                  
                  	Home.flxStartWorkInnerContainer.setEnabled(false);
              		Home.flxStartWorkInnerContainer.skin		= "flxItemDisabledSkin";
              		Home.flxStopWorkInnerContainer.setEnabled(true);
              		Home.flxStopWorkInnerContainer.skin			= "flxItemEnabledSkin";
                  
                  	Home.flxStartOverTimeInnerContainer.setEnabled(true);      
              		Home.flxStartOverTimeInnerContainer.skin	= "flxItemEnabledSkin";
              		Home.flxStopOverTimeInnerContainer.setEnabled(false);
              		Home.flxStopOverTimeInnerContainer.skin		= "flxItemDisabledSkin";
                  
          			Home.flxNonServiceEvntBtn.setEnabled(true);
          			Home.flxNonServiceEvntBtn.skin				= "flxItemEnabledSkin";
                  	Home.flxNonServiceEventOverlay.setVisibility(false);
                }
            } else {
              	if (isActive === true) {
                  	Home.flxTimerContainer.skin					= "flxYellowBgSkin";
                  
                  	Home.flxStartWorkInnerContainer.setEnabled(false);
              		Home.flxStartWorkInnerContainer.skin		= "flxItemDisabledSkin";
              		Home.flxStopWorkInnerContainer.setEnabled(false);
              		Home.flxStopWorkInnerContainer.skin			= "flxItemDisabledSkin";
                  
                  	Home.flxStartOverTimeInnerContainer.setEnabled(false);      
              		Home.flxStartOverTimeInnerContainer.skin	= "flxItemDisabledSkin";
              		Home.flxStopOverTimeInnerContainer.setEnabled(false);
              		Home.flxStopOverTimeInnerContainer.skin		= "flxItemDisabledSkin";
                  
          			Home.flxNonServiceEvntBtn.setEnabled(true);
          			Home.flxNonServiceEvntBtn.skin				= "flxItemEnabledSkin";
                  	Home.flxNonServiceEventOverlay.setVisibility(true);
                  
                  	Home.lblStartNonServiceEvnt.text			= getI18nString("common.label.stop");
      				Home.lblStartNonServiceEvnt.skin			= "lblRedBoldSkin";
                } else {
                  	Home.flxTimerContainer.skin					= "flxGreenBgSkin";
                  
                  	Home.flxStartWorkInnerContainer.setEnabled(false);
              		Home.flxStartWorkInnerContainer.skin		= "flxItemDisabledSkin";
              		Home.flxStopWorkInnerContainer.setEnabled(true);
              		Home.flxStopWorkInnerContainer.skin			= "flxItemEnabledSkin";
                  
                  	Home.flxStartOverTimeInnerContainer.setEnabled(true);      
              		Home.flxStartOverTimeInnerContainer.skin	= "flxItemEnabledSkin";
              		Home.flxStopOverTimeInnerContainer.setEnabled(false);
              		Home.flxStopOverTimeInnerContainer.skin		= "flxItemDisabledSkin";
                  
          			Home.flxNonServiceEvntBtn.setEnabled(true);
          			Home.flxNonServiceEvntBtn.skin				= "flxItemEnabledSkin";
                  	Home.flxNonServiceEventOverlay.setVisibility(false);
                  	
                  
                  	Home.lblStartNonServiceEvnt.text			= getI18nString("common.label.start");
      				Home.lblStartNonServiceEvnt.skin			= "lblGreenBoldSkin";
                }
            }
        }
  	} catch(e) {
      	logException(e, "switchButtonAccessibility", true);
	}
}

function showWorkTimerUpdates() {  
  	try {
      	printMessage("INSIDE showWorkTimerUpdates - gblNSEAttendanceObject - ", gblNSEAttendanceObject);
      	printMessage("INSIDE showWorkTimerUpdates - gblOTAttendanceObject - ", gblOTAttendanceObject);
      	printMessage("INSIDE showWorkTimerUpdates - gblNTAttendanceObject - ", gblNTAttendanceObject);
      	printMessage("INSIDE showWorkTimerUpdates - gblAttendanceMode - ", gblAttendanceMode);
      	
      	var isActive										= isWorkTimeRunning();
      	if (isActive === true) {
          	var timeDiff									= 0;
          	if (!isEmpty(gblNSEAttendanceObject)) {
              	timeDiff									= timeDifferenceWithCurrentTime(gblNSEAttendanceObject.startTimestamp);
            } else if (!isEmpty(gblOTAttendanceObject)) {
              	timeDiff									= timeDifferenceWithCurrentTime(gblOTAttendanceObject.startTimestamp);
            } else if (!isEmpty(gblNTAttendanceObject)) {
              	timeDiff									= timeDifferenceWithCurrentTime(gblNTAttendanceObject.startTimestamp);
            } 
          	
            Home.lblTimer.text								= getActualTime(timeDiff);
          	if (Home.flxTimerContainer.isVisible === false) {
              	Home.flxTimerContainer.setVisibility(true);
            }
        } else {
          	if (Home.flxTimerContainer.isVisible === true) {
              	Home.flxTimerContainer.setVisibility(false);
            }
        }            
   	} catch(e) {
      	logException(e, "showWorkTimerUpdates", true);
	}
}

function hideWorkTimerUpdates() {
  	try {
      	printMessage("INSIDE hideWorkTimerUpdates - ");
        Home.flxTimerContainer.setVisibility(false);
        Home.lblTimer.text									= "";
   	} catch(e) {
      	logException(e, "hideWorkTimerUpdates", true);
	}
}

function refreshAttendanceModeAndTimerBG() {
  	try {
      	printMessage("INSIDE refreshAttendanceModeAndTimerBG - ");

    } catch(e) {
      	logException(e, "refreshAttendanceModeAndTimerBG", true);
	}
}