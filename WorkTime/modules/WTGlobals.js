/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer all global variables and methods
***********************************************************************/

/** Initializing global variables **/
function initializeGlobals() {
  	try {
      	printMessage("INSIDE initializeGlobals");
      
      	/****************************************************************
        **	"DEV_EMEA" or "SIT_EMEA" or "STG_EMEA" or "PROD_EMEA" 
        **	"DEV_ASIA" or "SIT_ASIA" or "STG_ASIA" or "PROD_ASIA" 
        **	"DEV_NA" or "SIT_NA" or "STG_NA" or "PROD_NA" 
        ** 	"DEV_CHINA" or "SIT_CHINA" or "STG_CHINA" or "PROD_CHINA" 
        ****************************************************************/
      	gblBuildMode							= "SIT_EMEA";
      
      	/*********************************************************************************************
        **	gblAppMode - This is used to change the app behaviour
        ** 	Default one is: IE [All times resides inside start and End Work (Normal Time)]
        **  Other Option is: UK [Start Work (Normal Time) and Start Overtime are one after another
        **  and never overlaps]
        *********************************************************************************************/      
      	gblAppMode								= "IE";
      	
      	/******************************************************************************
        **	Change the package name based on the bundle identifier in project settings
        ** 	Default one is: "com.kony.WorkTime"
        ******************************************************************************/
      	gblPackageName                          = "com.kony.WorkTime"; 		
      
      	/******************************************************************************
        **	API Layer: Possible values are ESB or GCP or BOTH
        ******************************************************************************/
  		gblAPILayer								= "GCP";	
      
      	/******************************************************************************
        **	This value true means the app will not use branch selection option. 
        **  Otherwise app will show popup to select branch
        ******************************************************************************/
      	gblSkipBranchSelection					= true;
      	
      	/******************************************************************************
        ** 	For future enhancements
        **  gblGATimerInterval in seconds
        ******************************************************************************/
      	gblEnableGoogleAnalytics               	= false;  					
      	gblGoogleAPIKey							= "";
      	gblGATimerInterval						= 1800; 					
      	
      	/******************************************************************************
        **	Locale Mappings: To support multi language
        ******************************************************************************/
      	gblLocaleMappings						= {	"en"			: "English", 
                                                   	"es_ES"			: "Spanish - Española", 
                                                   	"zh_CN"			: "Simplified Chinese - 中文", 
                                                   	"zh_TW"			: "Traditional Chinese - 中文繁体", 
                                                   	"nl_NL"			: "Dutch - Nederlandse", 
                                                   	"tr_TR"			: "Turkish - Türkçe", 
                                                   	"pt_PT"			: "Portuguese - Português", 
                                                   	"in_ID"			: "Indonesian - bahasa Indonesia", 
                                                   	"de_DE"			: "German - Deutsche", 
                                                   	"cs_CZ"			: "Czech - čeština", 
                                                   	"sk_SK"			: "Slovak - slovenský", 
                                                   	"fr_FR"			: "French - français", 
                                                   	"lt_LT"			: "Lithuanian - lietuvių kalba",
                                                   	"da_DK"			: "Danish - dansk",
                                                   	"fi_FI"			: "Finnish - Suomalainen",
                                                   	"it_IT"			: "Italian - italiano",
                                                   	"ko_KR"			: "Korean - 한국어",
                                                   	"no_NO"			: "Norwegian - norsk",
                                                   	"sv_SE"			: "Swedish - svenska", 
                                                   	"th_TH"			: "Thai - ไทย",
                                                   	"et_EE"			: "Estonian - Eesti keel",
                                                   	"lv_LV"			: "Latvian - Latviešu"  
                                                  };
            
    	/***********************************************************************************************
        **	ENVIRONMENT CONFIGURATION
        **	ISC: Integration Service Name
        ** 	GAK: GCP API Key	
        **	ECI: ESB Client Id
        ** 	ECS: ESB Client Secret
        ** 	GPI: GA Property Id (Google analytics - Not using now. This is for future enhancements)
        ** 	SVH: Support View Header name
        ************************************************************************************************/
      	gblENVConfig							= {	"DEV_EMEA"		: {"ISC": "WTDownloadIntServicesEMEADEV", "GAK": "AIzaSyBOjo4YxrwzL2f09Ekha1stKysOfg_2DrE", "ECI": "57eceb02d85a41ef9cbc3c18b56a526c", "ECS": "5d391aae43e64a23A0AC49DFFD359C0D", "GPI": "", "SVH": "DEV CLOUD - EMEA"},
                                                   	"SIT_EMEA"		: {"ISC": "WTDownloadIntServicesEMEASIT", "GAK": "AIzaSyCeMKKGiJGXk2b5L1zG8lfu92AMjiG1lFk", "ECI": "57eceb02d85a41ef9cbc3c18b56a526c", "ECS": "5d391aae43e64a23A0AC49DFFD359C0D", "GPI": "UA-109501212-1", "SVH": "SIT CLOUD - EMEA"},
                                                   	"STG_EMEA"		: {"ISC": "WTDownloadIntServicesEMEASTG", "GAK": "AIzaSyBbmdA63y5_nOxdlFqlieBjYR10DCM0dT4", "ECI": "c89acbf594ec450480d9dbf518a0f4fe", "ECS": "0c3d0df4ffc74563BDF5AA7D001F5CAC", "GPI": "UA-109501212-8", "SVH": "STG CLOUD - EMEA"},
                                                   	"PROD_EMEA"		: {"ISC": "WTDownloadIntServicesEMEAPROD", "GAK": "AIzaSyBEwQf2mvC_IbZDgTC0bxqrCvrZWVDo7xk", "ECI": "13822718cb86439f971a65a711fad532", "ECS": "cd220f6edaa340bdB705EEA54FD2BB8E", "GPI": "UA-109501212-3", "SVH": "PROD CLOUD - EMEA"},
                                                   	"DEV_ASIA"		: {"ISC": "WTDownloadIntServicesASIADEV", "GAK": "AIzaSyBOjo4YxrwzL2f09Ekha1stKysOfg_2DrE", "ECI": "57eceb02d85a41ef9cbc3c18b56a526c", "ECS": "5d391aae43e64a23A0AC49DFFD359C0D", "GPI": "", "SVH": "DEV CLOUD - ASIA"},
                                                   	"SIT_ASIA"		: {"ISC": "WTDownloadIntServicesASIASIT", "GAK": "AIzaSyCeMKKGiJGXk2b5L1zG8lfu92AMjiG1lFk", "ECI": "57eceb02d85a41ef9cbc3c18b56a526c", "ECS": "5d391aae43e64a23A0AC49DFFD359C0D", "GPI": "", "SVH": "SIT CLOUD - ASIA"},
                                                   	"STG_ASIA"		: {"ISC": "WTDownloadIntServicesASIASTG", "GAK": "AIzaSyC0Az7AXglgNlSwbz0Gt8wKWZk25B2Co7Q", "ECI": "a6205c3eb6ff49c6968624c7e9642753", "ECS": "1101541d10784a5185DE1435ED6A73D4", "GPI": "UA-109501212-6", "SVH": "STG CLOUD - ASIA"},
                                                   	"PROD_ASIA"		: {"ISC": "WTDownloadIntServicesASIAPROD", "GAK": "AIzaSyB8gMPYzkVPX01sEAqqlyzFYJ_nIBML508", "ECI": "d82d199969514213b3e62c0b12d11556", "ECS": "b315f10db1ef42a4AD80CC4859A22F7F", "GPI": "UA-109501212-4", "SVH": "PROD CLOUD - ASIA"},
                                                   	"DEV_CHINA"		: {"ISC": "WTDownloadIntServicesCHINADEV", "GAK": "AIzaSyBOjo4YxrwzL2f09Ekha1stKysOfg_2DrE", "ECI": "57eceb02d85a41ef9cbc3c18b56a526c", "ECS": "5d391aae43e64a23A0AC49DFFD359C0D", "GPI": "", "SVH": "DEV CLOUD - CHINA"},
                                                   	"SIT_CHINA"		: {"ISC": "WTDownloadIntServicesCHINASIT", "GAK": "AIzaSyCeMKKGiJGXk2b5L1zG8lfu92AMjiG1lFk", "ECI": "57eceb02d85a41ef9cbc3c18b56a526c", "ECS": "5d391aae43e64a23A0AC49DFFD359C0D", "GPI": "", "SVH": "SIT CLOUD - CHINA"},
                                                   	"STG_CHINA"		: {"ISC": "WTDownloadIntServicesCHINASTG", "GAK": "AIzaSyC0Az7AXglgNlSwbz0Gt8wKWZk25B2Co7Q", "ECI": "a6205c3eb6ff49c6968624c7e9642753", "ECS": "1101541d10784a5185DE1435ED6A73D4", "GPI": "", "SVH": "STG CLOUD - CHINA"},
                                                   	"PROD_CHINA"	: {"ISC": "WTDownloadIntServicesCHINAPROD", "GAK": "AIzaSyB8gMPYzkVPX01sEAqqlyzFYJ_nIBML508", "ECI": "d82d199969514213b3e62c0b12d11556", "ECS": "b315f10db1ef42a4AD80CC4859A22F7F", "GPI": "", "SVH": "PROD CLOUD - CHINA"},
                                                   	"DEV_NA"		: {"ISC": "WTDownloadIntServicesNADEV", "GAK": "", "ECI": "", "ECS": "", "GPI": "", "SVH": "DEV CLOUD - NA"},
                                                   	"SIT_NA"		: {"ISC": "WTDownloadIntServicesNASIT", "GAK": "", "ECI": "", "ECS": "", "GPI": "", "SVH": "SIT CLOUD - NA"},
                                                   	"STG_NA"		: {"ISC": "WTDownloadIntServicesNASTG", "GAK": "AIzaSyCynYQ71uh-U4ym4-Z-Wzc4ZfnSMNXM07c", "ECI": "7990f612e27e4c0dbc7a97251f1d9375", "ECS": "2E1e622C5092456bB47C7ebe5489bF02", "GPI": "UA-109501212-7", "SVH": "STG CLOUD - NA"},
                                                   	"PROD_NA"		: {"ISC": "WTDownloadIntServicesNAPROD", "GAK": "AIzaSyCynYQ71uh-U4ym4-Z-Wzc4ZfnSMNXM07c", "ECI": "8949951552e44401887962539f18868c", "ECS": "974E5f5Ef01a43b281b2f2BC620580a2", "GPI": "UA-109501212-5", "SVH": "PROD CLOUD - NA"}         
        										  };
  		
      	/******************************************************************************
        ** 	GCP Authorization (Static one for testing purpose)
        ******************************************************************************/
      	gblGCPAuthorization						= "Bearer eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJzZXJ2aWNlLXRyYWstaHlnaWVuZUBkZXZlbG9wbWVudC1zdC1hcGkuaWFtLmdzZXJ2aWNlYWNjb3VudC5jb20iLCJuYmYiOjE1MTMwNjk0MjcsImV4cCI6MTU3NjE0MTQyNywiaXNzIjoic2VydmljZS10cmFrLWh5Z2llbmVAZGV2ZWxvcG1lbnQtc3QtYXBpLmlhbS5nc2VydmljZWFjY291bnQuY29tIiwiYXVkIjoiZGV2ZWxvcG1lbnQtc3QtYXBpIn0.JzCSB4WVXT8oo6ICcHBQPnYLxeA0ENJryZVkNW_ICLU_o3vHwzEJUwcE1zK-_GoJut8K2Ukhh7L5fkE_NypTgJfN7s7qZOUUHUSRLDiNreXTSNHhItqMgyUNGHflzbnVi0QiqiNu2qBAJugLaTKt_m6DbAl8gq-sMiu82cMfLXz4Bpi-4JCBY_B7wZ8GdFSLo5-VuQGnEYJiUY_AFv4h_dj-aZYgO8Zd-E_1qgsGK_hdJnmWwxXDEtVHhNDU0Nc-cg0-3nxR-0Nn6PVmXk5VJweNlJ4GPSWRoikHWL2sN-lXKIWG85bVV5FY6aQm4DMjwgZTJamjj_mp-k_MEe-IiQ";
    
      	/******************************************************************************
        ** 	Other App globals
        ** 	gblSyncParams.SYNC_TIMEOUT in seconds
        ******************************************************************************/
      	gblSyncParams							= {};
      	gblSyncParams.SYNC_BATCH_SIZE 			= "15000";
		gblSyncParams.SYNC_CHUNK_SIZE 			= 1024;
		gblSyncParams.IS_SECURE 				= false; 		
		gblSyncParams.SYNC_TIMEOUT 				= 900; 	
      	gblSyncParams.SYNC_LOG_LEVEL			= "NONE";		// ERROR or NONE or TRACE or INFO or DEBUG or FATAL
      	gblSyncParams.SYNC_TIMER_IN_SECS		= 30;
      
      	gblSelData								= {};
      	gblSelData.LANGUAGE_CODE				= "ENG";
      	gblSelData.COUNTRY_CODE					= "UK";
      	gblSelData.BUSINESS_CODE				= "D";
      	gblSelData.EMAIL						= "";
      	gblSelData.EMPLOYEE_CODE				= "";
      	gblSelData.EMPLOYEE_NAME				= "";
      	gblSelData.SEL_ICABS_KEY				= "";
      	gblSelData.BRANCH_NUMBER				= "";
      	gblSelData.BRANCH_NAME					= "";
      	gblSelData.IS_LOGGED_IN					= false;
      	gblSelData.LAST_SYNC_DATE				= "";
      	gblSelData.IS_WORK_TIME_ACTIVE			= false;
      	gblSelData.ACTIVE_NT_ATTENDANCE_REF		= "";
      	gblSelData.ACTIVE_OT_ATTENDANCE_REF		= "";
      	gblSelData.ACTIVE_NSE_ATTENDANCE_REF	= "";
      	
      	gblSyncProgress							= {};
  		gblSyncProgress.isActive				= false;		
  		gblSyncProgress.syncNavigation			= "";
    	gblSyncProgress.syncModuleName			= "";
  		gblSyncProgress.inputData				= "";
      
      	/******************************************************************************
        ** 	NT 	- Means Normal Time
        **	OT 	- Means Overtime
        **	NSE	- Means Non Service Event
        ******************************************************************************/
      	gblNTAttendanceObject					= null;
      	gblOTAttendanceObject					= null;
      	gblNSEAttendanceObject					= null;      	
        gblTouchMovePrevX						= 0;
      	gblPopupMode							= "";  
      	gblAttendanceMode						= "";
      
      	gblSyncDates							= {};
  		gblSyncDates.strLastDailySync			= "";
      
      	var strLastDailySync					= getDataFromKonyStore("strLastDailySync");
  		if (!isEmpty(strLastDailySync)){
      		gblSyncDates.strLastDailySync		= strLastDailySync;
    	}
      
      	gblSyncTimerCount						= 0;
    } catch(e) {
      	logException(e, "initializeGlobals");
	} 
}

/** Application - Pre app init call **/
function onPreAppInit(){
  	try {
      	printMessage("INSIDE onPreAppInit");
      	initializeGlobals();
      	setApplicationBehaviors();
      	getCurrentDevLocale();	
    } catch(e) {
      	logException(e, "onPreAppInit");
	}  	
}

/** Application - Post app init call **/
function onPostAppInit(){
  	try {
        printMessage("INSIDE onPostAppInit");
        gblDeviceInfo							= kony.os.deviceInfo();
      	hideDefaultLoadingIndicator();
      	startTimer();
    } catch(e) {
      	logException(e, "onPostAppInit");
	} 
}

/** To set some of the application behaviors **/
function setApplicationBehaviors() {
  	try {
        printMessage("INSIDE setApplicationBehaviors");
        var inputParamTable 					= {}; 
        inputParamTable.defaultIndicatorColor	= "#ffe6e6";
        kony.application.setApplicationBehaviors(inputParamTable); 
        var callbackParamTable 					= {}; 
        callbackParamTable.onbackground			= onAppWentBackground;
        callbackParamTable.onforeground			= onAppBecomesForeground;
        kony.application.setApplicationCallbacks(callbackParamTable);
   	} catch(e) {
      	logException(e, "setApplicationBehaviors");
	}
}

/** Reset global variables **/
function resetGlobals(){
  	try {
        printMessage("INSIDE resetGlobals");
      	gblSelData								= {};
      	gblSelData.LANGUAGE_CODE				= "ENG";
      	gblSelData.COUNTRY_CODE					= "UK";
      	gblSelData.BUSINESS_CODE				= "D";
      	gblSelData.EMAIL						= "";
      	gblSelData.EMPLOYEE_CODE				= "";
      	gblSelData.EMPLOYEE_NAME				= "";
      	gblSelData.SEL_ICABS_KEY				= "";
      	gblSelData.BRANCH_NUMBER				= "";
      	gblSelData.BRANCH_NAME					= "";
      	gblSelData.IS_LOGGED_IN					= false;
      	gblSelData.LAST_SYNC_DATE				= "";
      	gblSelData.IS_WORK_TIME_ACTIVE			= false;
      	gblSelData.ACTIVE_NT_ATTENDANCE_REF		= "";
      	gblSelData.ACTIVE_OT_ATTENDANCE_REF		= "";
      	gblSelData.ACTIVE_NSE_ATTENDANCE_REF	= "";
      	
      	gblSyncProgress							= {};
  		gblSyncProgress.isActive				= false;		
  		gblSyncProgress.syncNavigation			= "";
    	gblSyncProgress.syncModuleName			= "";
  		gblSyncProgress.inputData				= "";
      
      	gblNTAttendanceObject					= null;
      	gblOTAttendanceObject					= null;
      	gblNSEAttendanceObject					= null;
        gblTouchMovePrevX						= 0;
      	gblPopupMode							= "";
      	gblAttendanceMode						= "";
  	} catch(e) {
      	logException(e, "resetGlobals");
	}
}

/** Device back button action **/
function onDeviceBackButtonCall(){
  	try {
        printMessage("INSIDE onDeviceBackButtonCall");
        var curFormId							= kony.application.getCurrentForm().id;

        if (gblSyncProgress.isActive === true){
            /** Nothing to do here **/
        } else if (curFormId == WTConstant.FORM_LOGIN) {
            exitApplication();		
        } else if (curFormId == WTConstant.FORM_HOME) {            
          	if (Home.flxSettingsContainer.centerX != "-50%") {
              	closeSettingsMenu();
            } else {
              	var isActive					= isWorkTimeRunning();
              	if (isActive === true) {
                  	/** Nothing to do here **/
                } else {
                    showConfirmPopup("common.message.confirmExit", ActionConfirmPopupYesBtn);
                }
            }
        } 
   	} catch(e) {
      	logException(e, "onDeviceBackButtonCall");
	}
}

function startTimer() {
  	try {
      	printMessage("INSIDE startTimer - ");
      	stopTimer();
      	kony.timer.schedule("WorkTimerScheduler", refreshApplicationData, 1, true);
    } catch(e) {
      	logException(e, "startTimer", true);
	}
}

/** refreshApplicationData - Timer method to do some refresh **/
function refreshApplicationData() {  
  	try {
      	printMessage("INSIDE refreshApplicationData - ");
      	gblSyncTimerCount++;
      	if(gblSyncTimerCount === gblSyncParams.SYNC_TIMER_IN_SECS){
          gblSyncTimerCount		= 0;
          initSyncScheduler();
        }
      	if (Home !== null && Home !== undefined) {
  			showWorkTimerUpdates();
        }
   	} catch(e) {
      	logException(e, "refreshApplicationData", true);
	}
}

/** Timer stop call - Work time runner **/
function stopTimer() {
  	try {
      	printMessage("INSIDE stopTimer - ");
        kony.timer.cancel("WorkTimerScheduler");     
      	if (Home !== null && Home !== undefined) {
          	hideWorkTimerUpdates();
        }      	
   	} catch(e) {
      	logException(e, "stopTimer", true);
	}
}

/** Callback - App running in foreground **/
function onAppBecomesForeground(){
  	try {
      	printMessage("INSIDE onAppBecomesForeground");
   	} catch(e) {
      	logException(e, "onAppBecomesForeground");
	}
}

/** Callback - App running in background **/
function onAppWentBackground(){
  	try {
      	printMessage("INSIDE onAppWentBackground");
   	} catch(e) {
      	logException(e, "onAppWentBackground");
	}
}

/** To fetch current device locale  **/
function getCurrentDevLocale(){
  	try {
      	printMessage("INSIDE getCurrentDevLocale");
        var curDevLocale						= kony.i18n.getCurrentDeviceLocale();
        var language							= curDevLocale.language;
        var country								= curDevLocale.country;
        var devLocale							= (!isEmpty(language) && !isEmpty(country)) ? (language + "_" + toUpperCase(country)) : WTConstant.DEF_LOCALE;	
   	} catch(e) {
      	logException(e, "getCurrentDevLocale");
	}
}

/** To change current device locale  **/
function changeDeviceLocale(selLocale){
  	try {
      	printMessage("INSIDE changeDeviceLocale");
        if (kony.i18n.isLocaleSupportedByDevice(selLocale) && kony.i18n.isResourceBundlePresent(selLocale)){
            kony.i18n.setCurrentLocaleAsync(selLocale, success_setLocaleCallback, failure_setLocaleCallback, {});
        }  
  	} catch(e) {
      	logException(e, "changeDeviceLocale");
	}
}

/** Success callback - change device locale  **/
function success_setLocaleCallback(){
  	printMessage("INSIDE success_setLocaleCallback");
}

/** Failure callback - change device locale  **/
function failure_setLocaleCallback(){
	printMessage("INSIDE failure_setLocaleCallback");
}

function getMonthNamei18n(monthIndex){
  	try {
      	printMessage("INSIDE getMonthNamei18n");
  		var monthShortNamesi18n					= {
            "01"								: "common.month.jan",
            "02"								: "common.month.feb",
            "03"								: "common.month.mar",
            "04"								: "common.month.apr",
            "05"								: "common.month.may",
            "06"								: "common.month.jun",
            "07"								: "common.month.jul",
            "08"								: "common.month.aug",
            "09"								: "common.month.sep",
            "10"								: "common.month.oct",
            "11"								: "common.month.nov",
            "12"								: "common.month.dec"
    	};
  		return monthShortNamesi18n[monthIndex];
   	} catch(e) {
      	logException(e, "getMonthNamei18n", true);
	}
}

function setDataInKonyStore(storeKey, storeValue){
  	try {
        printMessage("INSIDE setDataInKonyStore - storeKey - ", storeKey);
        printMessage("INSIDE setDataInKonyStore - storeValue - ", storeValue);
        if (storeValue !== null && storeValue !== undefined) {
            kony.store.setItem(storeKey, storeValue);
        } else {
            printMessage("INSIDE setDataInKonyStore - failed to save in store - Given value is undefined or null - ");
        }  	
  	} catch (e) {
      	logException(e, "setDataInKonyStore");
    }
}

function getDataFromKonyStore(storeKey){
  	try {
        printMessage("INSIDE getDataFromKonyStore - storeKey - ", storeKey);
        var resultVal							= kony.store.getItem(storeKey);
        printMessage(resultVal, " Inside getDataFromKonyStore - resultVal - ");
        return resultVal;
   	} catch (e) {
      	logException(e, "getDataFromKonyStore");
    }
}

function getRegionCode() {
  	try {
      	printMessage("INSIDE getRegionCode ");
      	var regionCode							= "";
  		var buildMode							= gblBuildMode;
      	printMessage("INSIDE getRegionCode - buildMode - ", buildMode);
      	var buildModeSplits						= buildMode.split("_");
      	regionCode								= buildModeSplits[1];
      	printMessage("INSIDE getRegionCode - regionCode - ", regionCode);
      	return regionCode;
   	} catch(e) {
      	logException(e, "getRegionCode", true);
    }
}

function setSyncLogLevel(){  
  	try {
        var logLevel 							= kony.sync.log.ERROR;
        printMessage("INSIDE setSyncLogLevel - logLevel - before - ", logLevel);

        kony.sync.trackIntermediateUpdates		= false; // To sent last updated info to backend in case of multiple updates on same record 

        if (gblSyncParams.SYNC_LOG_LEVEL == "ERROR"){
            logLevel 							= kony.sync.log.ERROR;
        } else if (gblSyncParams.SYNC_LOG_LEVEL == "TRACE"){
            logLevel 							= kony.sync.log.TRACE;
        } else if (gblSyncParams.SYNC_LOG_LEVEL == "DEBUG"){
            logLevel 							= kony.sync.log.DEBUG;
        } else if (gblSyncParams.SYNC_LOG_LEVEL == "INFO"){
            logLevel 							= kony.sync.log.INFO;
        } else if (gblSyncParams.SYNC_LOG_LEVEL == "FATAL"){
            logLevel 							= kony.sync.log.FATAL;
        } else if (gblSyncParams.SYNC_LOG_LEVEL == "NONE"){
            logLevel 							= kony.sync.log.NONE;
        }

        printMessage("INSIDE setSyncLogLevel - logLevel - after - ", logLevel);

        sync.log.setLogLevel(logLevel, setLogLevelSuccessCallback, setLogLevelFailureCallback);
  	} catch (e) {
        logException(e, "setSyncLogLevel");
    }
}

function setLogLevelSuccessCallback(res){
  	printMessage("INSIDE setLogLevelSuccessCallback - res - ", res);
}

function setLogLevelFailureCallback(err){
  	printMessage("INSIDE setLogLevelFailureCallback - err - ", err);
}

function logException(exceptionObj, methodNameStr, logInDB) {
  	try {
      	printMessage("INSIDE - logException - Method name - ", methodNameStr);
  		printMessage("INSIDE - logException - Error Message - ", exceptionObj.message);
      	if (logInDB !== null && logInDB === true) {
          	var aRecord							= {};
          	aRecord.methodName					= methodNameStr;
          	aRecord.requestData					= "";
          	aRecord.responseData				= "";
          	aRecord.comment						= (exceptionObj !== null && exceptionObj.message !== null && exceptionObj.message !== "") ? exceptionObj.message : "";
          	createSupportEntry(aRecord);
        }
      	dismissLoading();
   	} catch(e) {
      	printMessage("INSIDE - logException - Exception - ", e.message);
	} 		
}

/** Void action **/
function voidClick(){
	doNothing();
}

function doNothing() {
  	try {
      	printMessage("INSIDE - doNothing ");
   	} catch(e) {
      	logException(e, "doNothing");
	} 	
}

function checkIsDayChanged(lastDateStr) {
  	try {
      	printMessage("INSIDE - checkIsDayChanged - lastDateStr - ", lastDateStr);
      	var curDate								= new Date();
      	var returnFlag 							= false;
      	if (!isEmpty(lastDateStr)) {
          	var prevDaySyncDate					= new Date(lastDateStr);
          	printMessage("INSIDE checkIsDayChanged - prevDaySyncDate - ", prevDaySyncDate);
          	if (parseInt(curDate.getFullYear()) > parseInt(prevDaySyncDate.getFullYear())) {
              	returnFlag 						= true;
            } else if (parseInt(curDate.getMonth()) > parseInt(prevDaySyncDate.getMonth())) {
              	returnFlag 						= true;
            } else if (parseInt(curDate.getDate()) > parseInt(prevDaySyncDate.getDate())) {
              	returnFlag 						= true;
            }
        } else {
          	returnFlag 							= true;
        }
      	printMessage("INSIDE checkIsDayChanged - returnFlag - ", returnFlag);
      	return returnFlag;
    } catch (e) {
      	logException(e, "checkIsDayChanged");
    }
}

function isDataUndefinedOrNull(dataStr) {
  	try {
      	printMessage("INSIDE - isDataUndefinedOrNull - dataStr - ", dataStr);
  		dataStr			= (dataStr !== null && dataStr !== undefined) ? dataStr.toString() : dataStr;
  		return (dataStr === null || dataStr === undefined || dataStr.toLowerCase().trim() == "null") ? true : false;
   	} catch (e) {
      	logException(e, "isDataUndefinedOrNull");
    }
}


function initSyncScheduler() {
  	printMessage("Inside initSyncScheduler - STP SYNC - ");
  	try{
        var isNtwAvailable	= isNetworkAvailable();
          	if (isNtwAvailable === true) {
            	printMessage("Inside initSyncScheduler - Device is online - STP SYNC - ");
            	if(gblSyncProgress.isActive === false){
                  	printMessage("Inside initSyncScheduler - gblSyncDates.strLastDailySync", gblSyncDates.strLastDailySync);
              		if (!isEmpty(gblSyncDates.strLastDailySync)){
                      var isDayChanged	= checkIsDayChanged(gblSyncDates.strLastDailySync);
                      printMessage("Inside initSyncScheduler - isDayChanged", isDayChanged);
                      if (isDayChanged === true) {
                          initDataSync("SYNC_DAILY_DOWNLOAD", "DOWNLOAD_CONFIG_DATA");
                      }
              	}
            }
        }
    } catch(e) {
      logException(e, "initSyncScheduler", false);
  	}
}

/** This is an application level API to hide the dafault loading indicator in android **/
function hideDefaultLoadingIndicator(){
	kony.application.setApplicationBehaviors({hideDefaultLoadingIndicator:true});
}