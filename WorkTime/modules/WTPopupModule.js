/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer Popup related methods
***********************************************************************/

/** Popup for displaying warnings and messages **/
function showMessagePopup(msgKey, isInfoPopup, buttonHandlerAction){
  	try {
      	printMessage("INSIDE showMessagePopup");
      	var curFrmObj							= kony.application.getCurrentForm();
      	if (curFrmObj.flxMessageOverlayContainer !== null && curFrmObj.flxMessageOverlayContainer.isVisible === false) {
          	curFrmObj.lblMessage.text			= getI18nString(msgKey);
          	curFrmObj.btnLeft.text				= getI18nString("common.label.cancel");
            curFrmObj.btnRight.text				= getI18nString("common.label.ok");
          	if (buttonHandlerAction !== null){
      			curFrmObj.btnRight.onClick		= buttonHandlerAction;
    		} else {
      			curFrmObj.btnRight.onClick		= dismissMessagePopup;
    		}
          
          	if (isInfoPopup === true) {
              	curFrmObj.btnLeft.setVisibility(false);
            } else {
              	curFrmObj.btnLeft.text			= getI18nString("common.label.no");
            	curFrmObj.btnRight.text			= getI18nString("common.label.yes");
              	curFrmObj.btnLeft.onClick		= dismissMessagePopup;
              	curFrmObj.btnLeft.setVisibility(true);
            }
          	curFrmObj.flxMessageOverlayContainer.setVisibility(true);
        }
    } catch(e) {
      	logException(e, "showMessagePopup", true);
	}
}

/** Popup - Ok button handler **/
function dismissMessagePopup(){
  	try {
      	printMessage("INSIDE dismissMessagePopup");
        var curFrmObj							= kony.application.getCurrentForm();
    	curFrmObj.flxMessageOverlayContainer.setVisibility(false);
    } catch(e) {
      	logException(e, "dismissMessagePopup", true);
	}        
}

/** Confirm popup - show **/
function showConfirmPopup(msgKey, yesButtonHandlerAction){  
  	try {
      	printMessage("INSIDE showConfirmPopup");
      	showMessagePopup(msgKey, false, yesButtonHandlerAction);
  	} catch(e) {
      	logException(e, "showConfirmPopup", true);
	}     
}

/** Confirm logout popup - Yes btn action **/
function onClickLogoutConfirmPopupYesBtn(){
  	try {
      	printMessage("INSIDE onClickLogoutConfirmPopupYesBtn");
      	showLoading("common.message.loading");
      	resetSync(successOnDoneLogout, errorOnDoneLogout);
    } catch(e) {
      	logException(e, "onClickLogoutConfirmPopupYesBtn", true);
	}        
}

function successOnDoneLogout() {
  	try {
      	printMessage("INSIDE successOnDoneLogout");
      	resetGlobals();
      	dismissLoading();
  		exitApplication();
   	} catch(e) {
      	logException(e, "successOnDoneLogout", true);
	}  
}

function errorOnDoneLogout() {
  	try {
      	printMessage("INSIDE errorOnDoneLogout");
      	dismissLoading();
  		exitApplication();
   	} catch(e) {
      	logException(e, "errorOnDoneLogout", true);
	}  
}


/** On click message popup Ok button action - This will exit application **/
function onClickMessagePopupOkButton(){
  	try {
      	printMessage("INSIDE onClickMessagePopupOkButton ");
  		showLoading("common.message.loading");
  		dismissMessagePopup();
  		dismissLoading();
  		exitApplication();
   	} catch(e) {
      	logException(e, "onClickMessagePopupOkButton", true);
	}  
}

/** Progress popup - show **/
function showProgressPopup(msgKey, titleKey){
  	showLoading(msgKey);
}

/** Progress popup - update message **/
function updateProgressMessage(msgKey){
  	 showLoading(msgKey);
}

/** Progress popup - dismiss **/
function dismissProgressMessage(){
  	 dismissLoading();
}

/** Confirm sync stop confirm - Yes btn action **/
/*function onClickSyncStopConfirmYesBtn(){
  	showLoading("common.message.loading");
  	stopSyncSession();
  	dismissConfirmPopup(); 
}*/