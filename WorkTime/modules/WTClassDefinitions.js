/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer all class definitions
***********************************************************************/

/** Class definition - Attendance **/
function Attendance(){
  	this.attendanceId				= "";
	this.startDateStr				= "";
	this.startTimestamp				= 0;
  	this.startLocLatitude			= null;
  	this.startLocLongitude			= null;
  	
  	this.endDateStr					= "";
	this.endTimestamp				= 0;
  	this.endLocLatitude				= null;
  	this.endLocLongitude			= null;
  	
  	this.attendanceType				= "";
  	this.startLocComment			= "";
  	this.endLocComment				= "";
  
  	this.employeeCode				= "";
  	this.email						= "";
  	this.employeeCode				= "";
  
	this.startTimestampStr 			= "";
	this.endTimestampStr 			= "";
	this.branchNumber 				= "";
	this.business 					= "";
	this.countryCode 				= "";
  
  	this.attendanceRef				= generateGUID();
  
  	this.nonServiceEventTypeCode	= "";
}