//****************Sync Version:Sync-Dev-8.0.0_v201711101237_r14*******************
// ****************Generated On Thu Jan 17 16:18:14 UTC 2019WTWorkOrderType*******************
// **********************************Start WTWorkOrderType's helper methods************************
if (typeof(kony) === "undefined") {
	kony = {};
}

if (typeof(kony.sync) === "undefined") {
	kony.sync = {};
}

if (typeof(kony.sync.log) === "undefined") {
	kony.sync.log = {};
}

if (typeof(sync) === "undefined") {
	sync = {};
}

if (typeof(sync.log) === "undefined") {
	sync.log = {};
}



if(typeof(com)=== "undefined"){ com = {}; }
if(typeof(com.wt)=== "undefined"){ com.wt = {}; }

/************************************************************************************
* Creates new WTWorkOrderType
*************************************************************************************/
com.wt.WTWorkOrderType = function(){
	this.isInstallation = null;
	this.isFollowUp = null;
	this.updateStateOfService = null;
	this.isCallOut = null;
	this.workOrderTypeDescription = null;
	this.workOrderTypeCode = null;
	this.updateDateTime = null;
	this.isNonServiceEvent = null;
	this.isDelivery = null;
	this.isShared = null;
	this.isSendToPDA = null;
	this.workOrderTypeDeleted = null;
	this.isRemoval = null;
	this.isAbortive = null;
	this.configId = null;
	this.markForUpload = true;
};

com.wt.WTWorkOrderType.prototype = {
	get isInstallation(){
		return kony.sync.getBoolean(this._isInstallation)+"";
	},
	set isInstallation(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute isInstallation in WTWorkOrderType.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._isInstallation = val;
	},
	get isFollowUp(){
		return kony.sync.getBoolean(this._isFollowUp)+"";
	},
	set isFollowUp(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute isFollowUp in WTWorkOrderType.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._isFollowUp = val;
	},
	get updateStateOfService(){
		return kony.sync.getBoolean(this._updateStateOfService)+"";
	},
	set updateStateOfService(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute updateStateOfService in WTWorkOrderType.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._updateStateOfService = val;
	},
	get isCallOut(){
		return kony.sync.getBoolean(this._isCallOut)+"";
	},
	set isCallOut(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute isCallOut in WTWorkOrderType.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._isCallOut = val;
	},
	get workOrderTypeDescription(){
		return this._workOrderTypeDescription;
	},
	set workOrderTypeDescription(val){
		this._workOrderTypeDescription = val;
	},
	get workOrderTypeCode(){
		return this._workOrderTypeCode;
	},
	set workOrderTypeCode(val){
		this._workOrderTypeCode = val;
	},
	get updateDateTime(){
		return this._updateDateTime;
	},
	set updateDateTime(val){
		this._updateDateTime = val;
	},
	get isNonServiceEvent(){
		return kony.sync.getBoolean(this._isNonServiceEvent)+"";
	},
	set isNonServiceEvent(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute isNonServiceEvent in WTWorkOrderType.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._isNonServiceEvent = val;
	},
	get isDelivery(){
		return kony.sync.getBoolean(this._isDelivery)+"";
	},
	set isDelivery(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute isDelivery in WTWorkOrderType.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._isDelivery = val;
	},
	get isShared(){
		return kony.sync.getBoolean(this._isShared)+"";
	},
	set isShared(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute isShared in WTWorkOrderType.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._isShared = val;
	},
	get isSendToPDA(){
		return kony.sync.getBoolean(this._isSendToPDA)+"";
	},
	set isSendToPDA(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute isSendToPDA in WTWorkOrderType.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._isSendToPDA = val;
	},
	get workOrderTypeDeleted(){
		return kony.sync.getBoolean(this._workOrderTypeDeleted)+"";
	},
	set workOrderTypeDeleted(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute workOrderTypeDeleted in WTWorkOrderType.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._workOrderTypeDeleted = val;
	},
	get isRemoval(){
		return kony.sync.getBoolean(this._isRemoval)+"";
	},
	set isRemoval(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute isRemoval in WTWorkOrderType.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._isRemoval = val;
	},
	get isAbortive(){
		return kony.sync.getBoolean(this._isAbortive)+"";
	},
	set isAbortive(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute isAbortive in WTWorkOrderType.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._isAbortive = val;
	},
	get configId(){
		return this._configId;
	},
	set configId(val){
		this._configId = val;
	},
};

/************************************************************************************
* Retrieves all instances of WTWorkOrderType SyncObject present in local database with
* given limit and offset where limit indicates the number of records to be retrieved
* and offset indicates number of rows to be ignored before returning the records.
* e.g. var orderByMap = []
* orderByMap[0] = {};
* orderByMap[0].key = "isInstallation";
* orderByMap[0].sortType ="desc";
* orderByMap[1] = {};
* orderByMap[1].key = "isFollowUp";
* orderByMap[1].sortType ="asc";
* var limit = 20;
* var offset = 5;
* com.wt.WTWorkOrderType.getAll(successcallback,errorcallback, orderByMap, limit, offset)
*************************************************************************************/
com.wt.WTWorkOrderType.getAll = function(successcallback, errorcallback, orderByMap, limit, offset){
	sync.log.trace("Entering com.wt.WTWorkOrderType.getAll->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTWorkOrderType.getTableName();
	orderByMap = kony.sync.formOrderByClause("WTWorkOrderType",orderByMap);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);	
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_orderBy(query, orderByMap);
				kony.sync.qb_limitOffset(query,limit,offset);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];

	function mySuccCallback(res){
		sync.log.trace("Entering com.wt.WTWorkOrderType.getAll->successcallback");
		successcallback(com.wt.WTWorkOrderType.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname,sql, params,mySuccCallback,errorcallback);
};

/************************************************************************************
* Returns number of WTWorkOrderType present in local database.
*************************************************************************************/
com.wt.WTWorkOrderType.getAllCount = function(successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WTWorkOrderType.getAllCount function");
	com.wt.WTWorkOrderType.getCount("",successcallback,errorcallback);
};

/************************************************************************************
* Returns number of WTWorkOrderType using where clause in the local Database
*************************************************************************************/
com.wt.WTWorkOrderType.getCount = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WTWorkOrderType.getCount->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTWorkOrderType.getCount" , "getCount", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTWorkOrderType.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select count(*) from \"" + tbname + "\" " + wcs;
	kony.sync.single_execute_sql(dbname,sql, null, mySuccCallback, errorcallback);
	function mySuccCallback(res) {
		sync.log.trace("Entering com.wt.WTWorkOrderType.getCount->successcallback");
		if(null!==res){
			var count = null;
			count = res["count(*)"];
			kony.sync.verifyAndCallClosure(successcallback, {count:count});
		}
		else{
			sync.log.error("Some error occured while getting the count");
		}
	}
};

/************************************************************************************
* Creates a new instance of WTWorkOrderType in the local Database. The new record will 
* be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
com.wt.WTWorkOrderType.prototype.create = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.wt.WTWorkOrderType.prototype.create function");
	var valuestable = this.getValuesTable(true);
	com.wt.WTWorkOrderType.create(valuestable, successcallback,errorcallback,this.markForUpload);
};
com.wt.WTWorkOrderType.create = function(valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering  com.wt.WTWorkOrderType.create->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	
	if(!kony.sync.validateInput(arguments, "com.wt.WTWorkOrderType.create" , "create", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTWorkOrderType.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);

	if(kony.sync.attributeValidation(valuestable,"WTWorkOrderType",errorcallback,true)===false){
		return;
	}
	
	function executeSuccess(){
		sync.log.trace("Entering  com.wt.WTWorkOrderType.create->success callback");
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}

	var pks = [];
	var errMsg = "";
	
	function createSuccesscallback(res){
		if(res==null || res.length==0){
			var relationshipMap={};  
			relationshipMap = com.wt.WTWorkOrderType.getRelationshipMap(relationshipMap,valuestable);
			kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
		}
		else{
			errMsg = "[" + errMsg + "]";
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey,kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
		}
	}
	
	if(kony.sync.enableORMValidations){
		errMsg = "workOrderTypeCode=" + valuestable.workOrderTypeCode;
		pks["workOrderTypeCode"] = {key:"workOrderTypeCode",value:valuestable.workOrderTypeCode};
		com.wt.WTWorkOrderType.getAllDetailsByPK(pks,createSuccesscallback,errorcallback)
	}
	else{
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}
};

/************************************************************************************
* Creates number of new instances of WTWorkOrderType in the local Database. These new 
* records will be merged with the enterprise datasource in the next Sync. Based upon 
* kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var valuesArray = [];
*		valuesArray[0] = {};
*		valuesArray[0].isInstallation = true;
*		valuesArray[0].isFollowUp = true;
*		valuesArray[0].updateStateOfService = true;
*		valuesArray[0].isCallOut = true;
*		valuesArray[0].workOrderTypeDescription = "workOrderTypeDescription_0";
*		valuesArray[0].workOrderTypeCode = "workOrderTypeCode_0";
*		valuesArray[0].isNonServiceEvent = true;
*		valuesArray[0].isDelivery = true;
*		valuesArray[0].isShared = true;
*		valuesArray[0].isSendToPDA = true;
*		valuesArray[0].isRemoval = true;
*		valuesArray[0].isAbortive = true;
*		valuesArray[0].configId = "configId_0";
*		valuesArray[1] = {};
*		valuesArray[1].isInstallation = true;
*		valuesArray[1].isFollowUp = true;
*		valuesArray[1].updateStateOfService = true;
*		valuesArray[1].isCallOut = true;
*		valuesArray[1].workOrderTypeDescription = "workOrderTypeDescription_1";
*		valuesArray[1].workOrderTypeCode = "workOrderTypeCode_1";
*		valuesArray[1].isNonServiceEvent = true;
*		valuesArray[1].isDelivery = true;
*		valuesArray[1].isShared = true;
*		valuesArray[1].isSendToPDA = true;
*		valuesArray[1].isRemoval = true;
*		valuesArray[1].isAbortive = true;
*		valuesArray[1].configId = "configId_1";
*		valuesArray[2] = {};
*		valuesArray[2].isInstallation = true;
*		valuesArray[2].isFollowUp = true;
*		valuesArray[2].updateStateOfService = true;
*		valuesArray[2].isCallOut = true;
*		valuesArray[2].workOrderTypeDescription = "workOrderTypeDescription_2";
*		valuesArray[2].workOrderTypeCode = "workOrderTypeCode_2";
*		valuesArray[2].isNonServiceEvent = true;
*		valuesArray[2].isDelivery = true;
*		valuesArray[2].isShared = true;
*		valuesArray[2].isSendToPDA = true;
*		valuesArray[2].isRemoval = true;
*		valuesArray[2].isAbortive = true;
*		valuesArray[2].configId = "configId_2";
*		com.wt.WTWorkOrderType.createAll(valuesArray, successcallback, errorcallback, true);
*************************************************************************************/
com.wt.WTWorkOrderType.createAll = function(valuesArray, successcallback, errorcallback, markForUpload){
	sync.log.trace("Entering com.wt.WTWorkOrderType.createAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTWorkOrderType.createAll" , "createAll", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTWorkOrderType.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var isProperData = true;
	var arrayLen = 0;
	var errorInfo = [];
	var arrayLength = valuesArray.length;
	var errObject = null;
	var isReferentialIntegrityFailure = false;
	var errMsg = null;
	if(kony.sync.enableORMValidations){
		var newValuesArray = [];

		//column level validations
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
			if(kony.sync.attributeValidation(valuestable,"WTWorkOrderType",errorcallback,true)===false){
				return;
			}

			newValuesArray[i] = valuestable;
		}
		valuesArray = newValuesArray;
		var isDuplicateKey = false;
		//checking for duplicate records
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkDuplicatePkCallback, transactionSuccessCallback, transactionErrorCallback);
		var isError = false;
		function checkDuplicatePkCallback(tx){
			arrayLength = valuesArray.length;
			for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
				var pks = [];
				errMsg = "workOrderTypeCode=" + valuestable.workOrderTypeCode;
				pks["workOrderTypeCode"] = {key:"workOrderTypeCode",value:valuestable.workOrderTypeCode};
				var wcs = [];
				if(com.wt.WTWorkOrderType.pkCheck(pks,wcs,errorcallback,"searching")===false){
					isError = true;
					return;
				}
				var query = kony.sync.qb_createQuery();
							kony.sync.qb_select(query, null);
							kony.sync.qb_from(query, tbname);
							kony.sync.qb_where(query, wcs);
				var query_compile = kony.sync.qb_compile(query);
				var sql = query_compile[0];
				var params = query_compile[1];
				var resultset = kony.sync.executeSql(tx, sql, params);
				if(resultset===false){
					isError = true;
					return;
				}
				if(resultset.rows.length!=0){
					isError = true;
					errMsg = "[" + errMsg + "]";
					isDuplicateKey = true;
					return;
				}
			}
			if(!isError){
				checkIntegrity(tx);
			}
		}
	}
	else{
		//copying by value
		var newValuesArray = [];
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			newValuesArray[i] = kony.sync.CreateCopy(valuesArray[i]);
		}
		valuesArray = newValuesArray;
		kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
	}

	function transactionErrorCallback(){
		if(isError==true){
			//Statement error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
		}
		else{
			//Transaction error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}

	function transactionSuccessCallback(){
		sync.log.trace("Entering  com.wt.WTWorkOrderType.createAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
		}
		else{
			if(isReferentialIntegrityFailure){
				kony.sync.verifyAndCallClosure(errorcallback, errObject);
			}
			if(isDuplicateKey){
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey,kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
			}
		}
	}
	
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  com.wt.WTWorkOrderType.createAll->checkIntegrity");
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var relationshipMap={};  
			relationshipMap = com.wt.WTWorkOrderType.getRelationshipMap(relationshipMap,valuesArray[i]);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				isReferentialIntegrityFailure = true;
				return;
			}
		}
	}
};
/************************************************************************************
* Updates WTWorkOrderType using primary key in the local Database. The update will be
* merged with the enterprise datasource in the next Sync.
*************************************************************************************/
com.wt.WTWorkOrderType.prototype.updateByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.wt.WTWorkOrderType.prototype.updateByPK function");
	var pks = this.getPKTable();
	var valuestable = this.getValuesTable(false);
	com.wt.WTWorkOrderType.updateByPK(pks,valuestable, successcallback,errorcallback,this.markForUpload);
};
com.wt.WTWorkOrderType.updateByPK = function(pks,valuestable, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering  com.wt.WTWorkOrderType.updateByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTWorkOrderType.updateByPK",  "updateByPk", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTWorkOrderType.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [ ];

	if(com.wt.WTWorkOrderType.pkCheck(pks,wcs,errorcallback,"updating")===false){
		return;
	}

	if(kony.sync.attributeValidation(valuestable,"WTWorkOrderType",errorcallback,false)===false){
		return;
	}

	var relationshipMap={};  
	relationshipMap = com.wt.WTWorkOrderType.getRelationshipMap(relationshipMap,valuestable);

	kony.sync.updateByPK(tbname, dbname, relationshipMap, pks,valuestable, successcallback,errorcallback, markForUpload, wcs);
};

/************************************************************************************
* Updates WTWorkOrderType(s) using where clause in the local Database. The update(s)
* will be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
com.wt.WTWorkOrderType.update = function(wcs, valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering com.wt.WTWorkOrderType.update function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTWorkOrderType.update",  "update", errorcallback)){
		return;
	}

	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTWorkOrderType.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);

	if(kony.sync.attributeValidation(valuestable,"WTWorkOrderType",errorcallback,false)===false){
		return;
	}
	function executeSuccess(){
		sync.log.trace("Entering  com.wt.WTWorkOrderType.update-> success callback of Integrity Check");
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, com.wt.WTWorkOrderType.getPKTable());
	}

	if(kony.sync.enableORMValidations){
		var relationshipMap={};  
		relationshipMap = com.wt.WTWorkOrderType.getRelationshipMap(relationshipMap,valuestable);
		kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
	}
	else{
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, com.wt.WTWorkOrderType.getPKTable());
	}
};

/************************************************************************************
* Updates WTWorkOrderType(s) satisfying one or more where clauses in the local Database. 
* The update(s) will be merged with the enterprise datasource in the next Sync.
* Based upon kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var inputArray = [];
*		inputArray[0] = {};
*		inputArray[0].changeSet = {};
*		inputArray[0].changeSet.isInstallation = true;
*		inputArray[0].changeSet.isFollowUp = true;
*		inputArray[0].changeSet.updateStateOfService = true;
*		inputArray[0].changeSet.isCallOut = true;
*		inputArray[0].whereClause = "where workOrderTypeCode = '0'";
*		inputArray[1] = {};
*		inputArray[1].changeSet = {};
*		inputArray[1].changeSet.isInstallation = true;
*		inputArray[1].changeSet.isFollowUp = true;
*		inputArray[1].changeSet.updateStateOfService = true;
*		inputArray[1].changeSet.isCallOut = true;
*		inputArray[1].whereClause = "where workOrderTypeCode = '1'";
*		inputArray[2] = {};
*		inputArray[2].changeSet = {};
*		inputArray[2].changeSet.isInstallation = true;
*		inputArray[2].changeSet.isFollowUp = true;
*		inputArray[2].changeSet.updateStateOfService = true;
*		inputArray[2].changeSet.isCallOut = true;
*		inputArray[2].whereClause = "where workOrderTypeCode = '2'";
*		com.wt.WTWorkOrderType.updateAll(inputArray,successcallback,errorcallback);
*************************************************************************************/
com.wt.WTWorkOrderType.updateAll = function(inputArray, successcallback, errorcallback, markForUpload) {
	sync.log.trace("Entering com.wt.WTWorkOrderType.updateAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTWorkOrderType.updateAll",  "updateAll", errorcallback)){
		return;
	}
	var dbname = "WorkTime_EMEA_SIT_2_1";
	var tbname = "WTWorkOrderType";
	var isError = false;
	var errObject = null;
	if(markForUpload == false || markForUpload == "false"){
		markForUpload="false"
	}
	else{
		markForUpload="true"
	}
	if((kony.sync.enableORMValidations)){

		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var v = inputArray[i];
			var valuestable = v.changeSet;
			var isEmpty = true;
			for(var key in valuestable){
				isEmpty = false;
				break;
			}
			if(isEmpty){
				errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeNullValue,kony.sync.getErrorMessage(kony.sync.errorCodeNullValue)));
				return;
			}
			var wcs = v.whereClause;
			var twcs = wcs;
			if(kony.sync.attributeValidation(valuestable,"WTWorkOrderType",errorcallback,false)===false){
				return;
			}

			newInputArray[i] = [];
			newInputArray[i].changeSet = valuestable;
			newInputArray[i].whereClause = wcs;
		}
		inputArray = newInputArray;
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);

	}
	else{
		//copying by value
		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
		    var v = inputArray[i];
		    newInputArray[i] = kony.sync.CreateCopy(v);
		}
		inputArray = newInputArray;
		kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,errorcallback,markForUpload, com.wt.WTWorkOrderType.getPKTable());
	}
	
		function transactionSuccessCallback(){
		sync.log.trace("Entering  com.wt.WTWorkOrderType.updateAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,transactionErrorCallback,markForUpload, com.wt.WTWorkOrderType.getPKTable());
		}
	}

	function transactionErrorCallback(){
		if(errObject===false){
			//Sql statement error has occcurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
			
		}
		else if(errObject!==null){
			// Referential integrity error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, errObject);
		}
		else{
			//Transaction error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  com.wt.WTWorkOrderType.updateAll->checkIntegrity");
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var relationshipMap={}; 
			relationshipMap = com.wt.WTWorkOrderType.getRelationshipMap(relationshipMap,inputArray[i].changeSet);
			sync.log.debug("Relationship Map for Integrity check created:", relationshipMap);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				kony.sync.rollbackTransaction(tx);
				return;
			}
		}
	}


}
/************************************************************************************
* Deletes WTWorkOrderType using primary key from the local Database. The record will be
* deleted from the enterprise datasource in the next Sync.
*************************************************************************************/
com.wt.WTWorkOrderType.prototype.deleteByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WTWorkOrderType.prototype.deleteByPK function");
	var pks = this.getPKTable();
	com.wt.WTWorkOrderType.deleteByPK(pks,successcallback,errorcallback,this.markForUpload);
};
com.wt.WTWorkOrderType.deleteByPK = function(pks, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering com.wt.WTWorkOrderType.deleteByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTWorkOrderType.deleteByPK",  "deleteByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTWorkOrderType.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var twcs = [];
	var deletedRows;
	var record = "";
	if(com.wt.WTWorkOrderType.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}	
	twcs = kony.sync.CreateCopy(wcs);
	function WTWorkOrderTypeTransactionCallback(tx){
		sync.log.trace("Entering com.wt.WTWorkOrderType.deleteByPK->WTWorkOrderType_PKPresent successcallback");
		record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(record===false){
			isError = true;
			return;
		}
		if (null !== record) {
		}else{
			pkNotFound = true;
		}
		var deletedRows = kony.sync.remove(tx, tbname, wcs, false, markForUpload, null);
			if(deletedRows === false){
				isError = true;
			}
	}
	
	function WTWorkOrderTypeErrorCallback(){
		sync.log.error("Entering com.wt.WTWorkOrderType.deleteByPK->relationship failure callback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}

	}
	function WTWorkOrderTypeSuccessCallback(){
		sync.log.trace("Entering com.wt.WTWorkOrderType.deleteByPK->relationship success callback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering com.wt.WTWorkOrderType.deleteByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
		
		var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(dbconnection != null){
			kony.sync.startTransaction(dbconnection, WTWorkOrderTypeTransactionCallback, WTWorkOrderTypeSuccessCallback, WTWorkOrderTypeErrorCallback, "Single Execute");
		}

};

/************************************************************************************
* Deletes WTWorkOrderType(s) using where clause from the local Database. The record(s)
* will be deleted from the enterprise datasource in the next Sync.
* e.g. com.wt.WTWorkOrderType.remove("where workOrderTypeDescription like 'A%'", successcallback,errorcallback, true);
*************************************************************************************/
com.wt.WTWorkOrderType.remove = function(wcs, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering com.wt.WTWorkOrderType.remove->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTWorkOrderType.remove",  "remove", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTWorkOrderType.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;
	var record = "";

	function WTWorkOrderType_removeTransactioncallback(tx){
			wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, false, markForUpload, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function WTWorkOrderType_removeSuccess(){
		sync.log.trace("Entering com.wt.WTWorkOrderType.remove->WTWorkOrderType_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering com.wt.WTWorkOrderType.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering com.wt.WTWorkOrderType.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, WTWorkOrderType_removeTransactioncallback, WTWorkOrderType_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Deletes WTWorkOrderType using primary key from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
com.wt.WTWorkOrderType.prototype.removeDeviceInstanceByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WTWorkOrderType.prototype.removeDeviceInstanceByPK function");
	var pks = this.getPKTable();
	com.wt.WTWorkOrderType.removeDeviceInstanceByPK(pks,successcallback,errorcallback);
};
com.wt.WTWorkOrderType.removeDeviceInstanceByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WTWorkOrderType.removeDeviceInstanceByPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTWorkOrderType.removeDeviceInstanceByPK",  "removeDeviceInstanceByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTWorkOrderType.getTableName();
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var deletedRows;
	if(com.wt.WTWorkOrderType.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}
	
	function WTWorkOrderTypeTransactionCallback(tx){
		sync.log.trace("Entering com.wt.WTWorkOrderType.removeDeviceInstanceByPK -> WTWorkOrderTypeTransactionCallback");
		var record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(null !== record && false !=record) {
			deletedRows = kony.sync.remove(tx, tbname, wcs, true, null, null);
			if(deletedRows === false){
				isError = true;
			}
		}else{
			pkNotFound = true;
		}
	}
	
	function WTWorkOrderTypeErrorCallback(){
		sync.log.error("Entering com.wt.WTWorkOrderType.removeDeviceInstanceByPK -> WTWorkOrderTypeErrorCallback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	function WTWorkOrderTypeSuccessCallback(){
		sync.log.trace("Entering com.wt.WTWorkOrderType.removeDeviceInstanceByPK -> WTWorkOrderTypeSuccessCallback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering com.wt.WTWorkOrderType.removeDeviceInstanceByPK -> PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
	
	var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(dbconnection != null){
		kony.sync.startTransaction(dbconnection, WTWorkOrderTypeTransactionCallback, WTWorkOrderTypeSuccessCallback, WTWorkOrderTypeErrorCallback, "Single Execute");
	}

};

/************************************************************************************
* Deletes WTWorkOrderType(s) using where clause from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
com.wt.WTWorkOrderType.removeDeviceInstance = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WTWorkOrderType.removeDeviceInstance->main function");
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTWorkOrderType.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;

	function WTWorkOrderType_removeTransactioncallback(tx){
		wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, true, null, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function WTWorkOrderType_removeSuccess(){
		sync.log.trace("Entering com.wt.WTWorkOrderType.remove->WTWorkOrderType_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering com.wt.WTWorkOrderType.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering com.wt.WTWorkOrderType.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, WTWorkOrderType_removeTransactioncallback, WTWorkOrderType_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Retrieves WTWorkOrderType using primary key from the local Database. 
*************************************************************************************/
com.wt.WTWorkOrderType.prototype.getAllDetailsByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WTWorkOrderType.prototype.getAllDetailsByPK function");
	var pks = this.getPKTable();
	com.wt.WTWorkOrderType.getAllDetailsByPK(pks,successcallback,errorcallback);
};
com.wt.WTWorkOrderType.getAllDetailsByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WTWorkOrderType.getAllDetailsByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTWorkOrderType.getAllDetailsByPK",  "getAllDetailsByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTWorkOrderType.getTableName();
	var wcs = [];
	if(com.wt.WTWorkOrderType.pkCheck(pks,wcs,errorcallback,"searching")===false){
		return;
	}
	twcs = kony.sync.CreateCopy(wcs);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, wcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	function mySuccCallback(res){
		sync.log.trace("Entering com.wt.WTWorkOrderType.getAllDetailsByPK-> success callback function");
		successcallback(com.wt.WTWorkOrderType.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};






/************************************************************************************
* Retrieves WTWorkOrderType(s) using where clause from the local Database. 
* e.g. com.wt.WTWorkOrderType.find("where workOrderTypeDescription like 'A%'", successcallback,errorcallback);
*************************************************************************************/
com.wt.WTWorkOrderType.find = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WTWorkOrderType.find function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTWorkOrderType.find",  "find", errorcallback)){
		return;
	}
	//wcs will be a string formed by the user.
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTWorkOrderType.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select * from \"" + tbname + "\" " + wcs;
	function mySuccCallback(res){
		kony.sync.verifyAndCallClosure(successcallback, com.wt.WTWorkOrderType.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccCallback, errorcallback);
};

/************************************************************************************
* Marks instance of WTWorkOrderType with given primary key for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
com.wt.WTWorkOrderType.prototype.markForUploadbyPK = function(successcallback, errorcallback){
	sync.log.trace("Entering com.wt.WTWorkOrderType.prototype.markForUploadbyPK function");
	var pks = this.getPKTable();
	com.wt.WTWorkOrderType.markForUploadbyPK(pks, successcallback, errorcallback);
};
com.wt.WTWorkOrderType.markForUploadbyPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.wt.WTWorkOrderType.markForUploadbyPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTWorkOrderType.markForUploadbyPK",  "markForUploadbyPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTWorkOrderType.getTableName();
	var isError = false;
	var recordsFound = false;
	var recordsMarkedForUpload = 0;
	var wcs = [];
	if(com.wt.WTWorkOrderType.pkCheck(pks, wcs, errorcallback, "marking for upload by PK")===false){
		return;
	}

	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);		
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = [];
		twcs = wcs;
		kony.table.insert(twcs,{key : kony.sync.historyTableChangeTypeColumn, value : record[kony.sync.historyTableChangeTypeColumn], optype : "EQ",comptype : "AND"});
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
					kony.sync.qb_where(query, twcs);
		kony.table.remove(twcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);
	}
	
	function single_transaction_callback (tx){
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_select(query, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query, tbname);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		var resultSet = kony.sync.executeSql(tx, sql, params);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		if(num_records > 0){
			recordsFound = true;
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, 0);
			var changeType = record[kony.sync.mainTableChangeTypeColumn];
			if(!kony.sync.isNullOrUndefined(changeType) && kony.string.startsWith(""+changeType,"9")){
				recordsMarkedForUpload = 1;
				if(markRecordForUpload(tx, record) === false){
					isError = true;
					return;
				}
			}
		}
					
				
		var query1 =kony.sync.qb_createQuery();
					kony.sync.qb_select(query1, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query1, tbname + kony.sync.historyTableName);
					kony.sync.qb_where(query1, wcs);
		var query1_compile = kony.sync.qb_compile(query1);
		var sql1 = query1_compile[0];
		var params1 = query1_compile[1];
		var resultSet1 = kony.sync.executeSql (tx, sql1, params1);
		if(resultSet1!==false){
			var num_records = resultSet1.rows.length;
			for(var i = 0; i <= num_records - 1; i++ ){
				var record = kony.db.sqlResultsetRowItem(tx, resultSet1, i);
				if(markRecordForUploadHistory(tx, record) === false){
					isError = true;
					return;
				}
				recordsFound = true;
			}
		}
		else{
			isError = true;
		}
	}
	function single_transaction_success_callback(){
		if(recordsFound === true){
			kony.sync.verifyAndCallClosure(successcallback , {count:recordsMarkedForUpload});
		}
		else{
			kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
		}
	}
	
	function single_transaction_error_callback(res){
		if (!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Marks instance(s) of WTWorkOrderType matching given where clause for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
com.wt.WTWorkOrderType.markForUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WTWorkOrderType.markForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTWorkOrderType.markForUpload",  "markForUpload", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTWorkOrderType.getTableName();
	var isError = false;
	var num_records_main = 0;
	wcs = kony.sync.validateWhereClause(wcs);
	if(!kony.sync.isNull(wcs) && !kony.sync.isEmptyString(wcs)) {
		wcs = wcs + " and " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}else{	
		wcs = "where " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}
	
	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0] + " " + wcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = "";
		twcs = wcs;
		twcs = twcs + " AND " + kony.sync.historyTableChangeTypeColumn + " = " + record[kony.sync.historyTableChangeTypeColumn];
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0]  + " " + twcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function single_transaction_callback (tx){
		sync.log.trace("Entering com.wt.WTWorkOrderType.markForUpload->single_transaction_callback");
		//updating main table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from \"" + tbname + "\" " + wcs ;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}
		
		num_records_main = resultSet.rows.length;
		for(var i = 0; i < num_records_main; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUpload(tx, record) === false){
				isError = true;
				return;
			}
		}
		
		//updating history table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from " + tbname + kony.sync.historyTableName + " " + wcs;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		for ( var i = 0; i <= num_records - 1; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUploadHistory(tx, record)=== false){
				isError = true;
				return;
			}
		}
	}
	
	function single_transaction_success_callback(){
		sync.log.trace("Entering com.wt.WTWorkOrderType.markForUpload->single_transaction_success_callback");
		kony.sync.verifyAndCallClosure(successcallback, {count:num_records_main});
	}
	
	function single_transaction_error_callback(){
		sync.log.error("Entering com.wt.WTWorkOrderType.markForUpload->single_transaction_error_callback");
		if(!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Retrieves instance(s) of WTWorkOrderType pending for upload. Records are marked for
* pending upload if they have been updated or created locally and the changes have
* not been merged with enterprise datasource.
*************************************************************************************/
com.wt.WTWorkOrderType.getPendingUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WTWorkOrderType.getPendingUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTWorkOrderType.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var sql;
	if(typeof(wcs) === "string" && wcs != null){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname + "\" "+ wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.wt.WTWorkOrderType.getPendingUpload->successcallback function");
		kony.sync.verifyAndCallClosure(successcallback, com.wt.WTWorkOrderType.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of WTWorkOrderType pending for acknowledgement. This is relevant
* when the SyncObject is part of the SyncScope whose SyncStrategy is PersistentSync.
* In persistent Sync the  records in the local database are put into a pending 
* acknowledgement state after an upload.
*************************************************************************************/
com.wt.WTWorkOrderType.getPendingAcknowledgement = function(successcallback, errorcallback){
	sync.log.trace("Entering com.wt.WTWorkOrderType.getPendingAcknowledgement->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTWorkOrderType.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var mysql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" <> "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	kony.sync.single_select_execute(dbname, mysql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.wt.WTWorkOrderType.getPendingAcknowledgement success callback function");
		kony.sync.verifyAndCallClosure(successcallback, com.wt.WTWorkOrderType.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of WTWorkOrderType deferred for upload.
*************************************************************************************/
com.wt.WTWorkOrderType.getDeferredUpload = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WTWorkOrderType.getDeferredUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTWorkOrderType.getTableName();
	var sql;
	if(typeof(wcs) === "string" && wcs != null ){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname +  "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'";
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'"; 
	}
	
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.wt.WTWorkOrderType.getDeferredUpload->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, com.wt.WTWorkOrderType.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
};

/************************************************************************************
* Rollbacks all changes to WTWorkOrderType in local database to last synced state
*************************************************************************************/
com.wt.WTWorkOrderType.rollbackPendingLocalChanges = function(successcallback, errorcallback){
	sync.log.trace("Entering com.wt.WTWorkOrderType.rollbackPendingLocalChanges->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTWorkOrderType.getTableName();
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.wt.WTWorkOrderType.rollbackPendingLocalChanges->main function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}		
};

/************************************************************************************
* Rollbacks changes to WTWorkOrderType's record with given primary key in local 
* database to last synced state
*************************************************************************************/
com.wt.WTWorkOrderType.prototype.rollbackPendingLocalChangesByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering com.wt.WTWorkOrderType.prototype.rollbackPendingLocalChangesByPK function");
	var pks = this.getPKTable();
	com.wt.WTWorkOrderType.rollbackPendingLocalChangesByPK(pks,successcallback,errorcallback);
};
com.wt.WTWorkOrderType.rollbackPendingLocalChangesByPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.wt.WTWorkOrderType.rollbackPendingLocalChangesByPK->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTWorkOrderType.rollbackPendingLocalChangesByPK",  "rollbackPendingLocalChangesByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTWorkOrderType.getTableName();
	var wcs = [];
	if(com.wt.WTWorkOrderType.pkCheck(pks,wcs,errorcallback,"rollbacking")===false){
		return;
	}	
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, wcs, mySuccesscallback, pkNotFoundErrCallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.wt.WTWorkOrderType.rollbackPendingLocalChangesByPK->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering com.wt.WTWorkOrderType.rollbackPendingLocalChangesByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
};

/************************************************************************************
* isRecordDeferredForUpload returns true or false depending on whether WTWorkOrderType's record  
* with given primary key got deferred in last sync
*************************************************************************************/
com.wt.WTWorkOrderType.prototype.isRecordDeferredForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.wt.WTWorkOrderType.prototype.isRecordDeferredForUpload function");
	var pks = this.getPKTable();
	com.wt.WTWorkOrderType.isRecordDeferredForUpload(pks,successcallback,errorcallback);
};
com.wt.WTWorkOrderType.isRecordDeferredForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.wt.WTWorkOrderType.isRecordDeferredForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTWorkOrderType.isRecordDeferredForUpload",  "isRecordDeferredForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTWorkOrderType.getTableName();
	var wcs = [] ;
	var flag;
	if(com.wt.WTWorkOrderType.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.wt.WTWorkOrderType.isRecordDeferredForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {deferred:flag});
	}
};

/************************************************************************************
* isRecordPendingForUpload returns true or false depending on whether WTWorkOrderType's record  
* with given primary key is pending for upload
*************************************************************************************/
com.wt.WTWorkOrderType.prototype.isRecordPendingForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  com.wt.WTWorkOrderType.prototype.isRecordPendingForUpload function");
	var pks = this.getPKTable();
	com.wt.WTWorkOrderType.isRecordPendingForUpload(pks,successcallback,errorcallback);
};
com.wt.WTWorkOrderType.isRecordPendingForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering com.wt.WTWorkOrderType.isRecordPendingForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "com.wt.WTWorkOrderType.isRecordPendingForUpload",  "isRecordPendingForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = com.wt.WTWorkOrderType.getTableName();
	var wcs = [] ;
	var flag;
	if(com.wt.WTWorkOrderType.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "NOT LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering com.wt.WTWorkOrderType.isRecordPendingForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {pending:flag});
	}
};




/************************************************************************************
* Start of helper functions used internally, not to be used as ORMs
*************************************************************************************/

//Deletes all the dependant tables in the relationship tables.Need to pass transaction handler as input
com.wt.WTWorkOrderType.removeCascade = function(tx, wcs, errorcallback, markForUpload, isCascade, parentTable, isLocal){
	sync.log.trace("Entering com.wt.WTWorkOrderType.removeCascade function");
	var tbname = com.wt.WTWorkOrderType.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	function removeCascadeChildren(){
	}
	if(isCascade){
		if(removeCascadeChildren()===false){
			return false;
		}
		if(kony.sync.deleteBatch(tx, tbname, wcs, isLocal,markForUpload, null)===false){
			return false;
		}
		return true;
	}else{
		var sql = "select * from \"" + tbname + "\" " + wcs;
		var resultSet = kony.sync.executeSql(tx, sql, null);
		if(resultSet===false){
			return false;
		}	
		var num_records = resultSet.rows.length;
		if(num_records === 0){
			return true;
		}else{
			sync.log.error(kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable));
			errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeReferentialIntegrity,kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable)));
			return false;
		}
	}
};


com.wt.WTWorkOrderType.convertTableToObject = function(res){
	sync.log.trace("Entering com.wt.WTWorkOrderType.convertTableToObject function");
	objMap = [];
	if(res!==null){
		for(var i in res){
			var obj = new com.wt.WTWorkOrderType();
			obj.isInstallation = res[i].isInstallation;
			obj.isFollowUp = res[i].isFollowUp;
			obj.updateStateOfService = res[i].updateStateOfService;
			obj.isCallOut = res[i].isCallOut;
			obj.workOrderTypeDescription = res[i].workOrderTypeDescription;
			obj.workOrderTypeCode = res[i].workOrderTypeCode;
			obj.updateDateTime = res[i].updateDateTime;
			obj.isNonServiceEvent = res[i].isNonServiceEvent;
			obj.isDelivery = res[i].isDelivery;
			obj.isShared = res[i].isShared;
			obj.isSendToPDA = res[i].isSendToPDA;
			obj.workOrderTypeDeleted = res[i].workOrderTypeDeleted;
			obj.isRemoval = res[i].isRemoval;
			obj.isAbortive = res[i].isAbortive;
			obj.configId = res[i].configId;
			obj.markForUpload = (Math.floor(res[i].konysyncchangetype/10)==9)? false:true;
			objMap[i] = obj;
		}
	}
	return objMap;
};

com.wt.WTWorkOrderType.filterAttributes = function(valuestable, insert){
	sync.log.trace("Entering com.wt.WTWorkOrderType.filterAttributes function");
	var attributeTable = {};
	attributeTable.isInstallation = "isInstallation";
	attributeTable.isFollowUp = "isFollowUp";
	attributeTable.updateStateOfService = "updateStateOfService";
	attributeTable.isCallOut = "isCallOut";
	attributeTable.workOrderTypeDescription = "workOrderTypeDescription";
	attributeTable.workOrderTypeCode = "workOrderTypeCode";
	attributeTable.isNonServiceEvent = "isNonServiceEvent";
	attributeTable.isDelivery = "isDelivery";
	attributeTable.isShared = "isShared";
	attributeTable.isSendToPDA = "isSendToPDA";
	attributeTable.isRemoval = "isRemoval";
	attributeTable.isAbortive = "isAbortive";
	attributeTable.configId = "configId";

	var PKTable = {};
	PKTable.workOrderTypeCode = {}
	PKTable.workOrderTypeCode.name = "workOrderTypeCode";
	PKTable.workOrderTypeCode.isAutoGen = false;
	var newvaluestable = {};
	for (var k in valuestable){
		var v = valuestable[k];
		if(kony.sync.isNull(attributeTable[k])) { 
			sync.log.warn("Ignoring the attribute " + k + " for the SyncObject WTWorkOrderType. "  + k + " not defined as an attribute in SyncConfiguration.");
		}else if(!kony.sync.isNull(PKTable[k])) {
			if(insert===false){
				sync.log.warn("Ignoring the primary key " + k + " for the SyncObject WTWorkOrderType. Primary Key should not be the part of the attributes to be updated in the local device database.");
			}else if(PKTable[k].isAutoGen){
				sync.log.warn("Ignoring the auto-generated primary key " + k + " for the SyncObject WTWorkOrderType. Auto-generated Primary Key should not be the part of the attributes to be inserted in the local device database.");
			}else{
				newvaluestable[k] = v;
			}
		}
		else{
			newvaluestable[k] = v;
		}
	}
	return newvaluestable;
};

com.wt.WTWorkOrderType.formOrderByClause = function(orderByMap){
	sync.log.trace("Entering com.wt.WTWorkOrderType.formOrderByClause function");
	if(!kony.sync.isNull(orderByMap)){
		var valuestable = kony.sync.convertOrderByMapToValuesTable(orderByMap);
		//var filteredValuestable = com.wt.WTWorkOrderType.filterAttributes(valuestable, true);
		return kony.sync.convertToValuesTableOrderByMap(orderByMap,valuestable);
	}
	return null;
};

com.wt.WTWorkOrderType.prototype.getValuesTable = function(isInsert){
	sync.log.trace("Entering com.wt.WTWorkOrderType.prototype.getValuesTable function");
	var valuesTable = {};
	valuesTable.isInstallation = this.isInstallation;
	valuesTable.isFollowUp = this.isFollowUp;
	valuesTable.updateStateOfService = this.updateStateOfService;
	valuesTable.isCallOut = this.isCallOut;
	valuesTable.workOrderTypeDescription = this.workOrderTypeDescription;
	if(isInsert===true){
		valuesTable.workOrderTypeCode = this.workOrderTypeCode;
	}
	valuesTable.isNonServiceEvent = this.isNonServiceEvent;
	valuesTable.isDelivery = this.isDelivery;
	valuesTable.isShared = this.isShared;
	valuesTable.isSendToPDA = this.isSendToPDA;
	valuesTable.isRemoval = this.isRemoval;
	valuesTable.isAbortive = this.isAbortive;
	valuesTable.configId = this.configId;
	return valuesTable;
};

com.wt.WTWorkOrderType.prototype.getPKTable = function(){
	sync.log.trace("Entering com.wt.WTWorkOrderType.prototype.getPKTable function");
	var pkTable = {};
	pkTable.workOrderTypeCode = {key:"workOrderTypeCode",value:this.workOrderTypeCode};
	return pkTable;
};

com.wt.WTWorkOrderType.getPKTable = function(){
	sync.log.trace("Entering com.wt.WTWorkOrderType.getPKTable function");
	var pkTable = [];
	pkTable.push("workOrderTypeCode");
	return pkTable;
};

com.wt.WTWorkOrderType.pkCheck = function(pks,wcs,errorcallback,opName){
	sync.log.trace("Entering com.wt.WTWorkOrderType.pkCheck function");
	var wc = [];
	if(kony.sync.isNull(pks)){
		sync.log.error("Primary Key workOrderTypeCode not specified in  " + opName + "  an item in WTWorkOrderType");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("workOrderTypeCode",opName,"WTWorkOrderType")));
		return false;	
	}
	else if(kony.sync.isValidJSTable(pks)){
		if(!kony.sync.isNull(pks.workOrderTypeCode)){
			if(!kony.sync.isNull(pks.workOrderTypeCode.value)){
				wc.key = "workOrderTypeCode";
				wc.value = pks.workOrderTypeCode.value;
			}
			else{
				wc.key = "workOrderTypeCode";
				wc.value = pks.workOrderTypeCode;
			}
		}else{
			sync.log.error("Primary Key workOrderTypeCode not specified in  " + opName + "  an item in WTWorkOrderType");
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("workOrderTypeCode",opName,"WTWorkOrderType")));
			return false;
		}
	}
	else{
		wc.key = "workOrderTypeCode";
		wc.value = pks;
	}	
	kony.table.insert(wcs,wc);
	return true;
};

com.wt.WTWorkOrderType.validateNull = function (valuestable,errorcallback){
	sync.log.trace("Entering com.wt.WTWorkOrderType.validateNull function");
	return true;
};

com.wt.WTWorkOrderType.validateNullInsert = function (valuestable,errorcallback){
	sync.log.trace("Entering com.wt.WTWorkOrderType.validateNullInsert function");
	return true;
};

com.wt.WTWorkOrderType.getRelationshipMap = function(relationshipMap,valuestable){
	sync.log.trace("Entering com.wt.WTWorkOrderType.getRelationshipMap function");
	var r1 = {};
	r1 = {};
	r1.sourceAttribute = [];
	r1.foreignKeyAttribute = [];
	r1.targetAttributeValue  = [];
		
	if (!kony.sync.isNullOrUndefined(valuestable.configId)){
		r1.sourceAttribute.push("configId");
		r1.foreignKeyAttribute.push("configId");
		r1.targetAttributeValue.push("'" + valuestable.configId + "'");
	}
	if(r1.targetAttributeValue.length > 0){
		if(relationshipMap.WTConfigTable===undefined){
			relationshipMap.WTConfigTable = [];
		}
		relationshipMap.WTConfigTable.push(r1);
	}
	

	return relationshipMap;
};


com.wt.WTWorkOrderType.checkPKValueTables = function (valuetables)	{
	var checkPksNotNullFlag = true;
	for(var i = 0; i < valuetables.length; i++)	{
		if(kony.sync.isNull(valuetables[i])){
			checkPksNotNullFlag = false;
			break;
		}
	}
	return checkPksNotNullFlag;
};

com.wt.WTWorkOrderType.getTableName = function(){
	return "WTWorkOrderType";
};




// **********************************End WTWorkOrderType's helper methods************************